﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace PartnerDeclarationPortal.App_Code
{
    public class LOG
    {
        public static void Exception(Exception ex)
        {
            lock (typeof(LOG))
            {
                StreamWriter oStreamWriter = null;
                try
                {
                    var LOG_FilePath = HttpContext.Current.Server.MapPath("~/Logs/") + "Exception_" + DateTime.Now.ToString("dd_MM_yyyy").ToString() + ".txt";
                    oStreamWriter = new StreamWriter(LOG_FilePath, true);
                    oStreamWriter.WriteLine(DateTime.Now.ToString());
                    oStreamWriter.WriteLine(ex.Message + System.Environment.NewLine + ex.InnerException);
                    oStreamWriter.WriteLine(ex.StackTrace);
                    oStreamWriter.WriteLine("-----------------------------------------------------------------------------------------------------");
                }
                catch
                {
                }
                finally
                {
                    if (oStreamWriter != null)
                    {
                        oStreamWriter.Close();
                    }
                }
            }
        }

        public static void Info(string message)
        {
            lock (typeof(LOG))
            {
                StreamWriter oStreamWriter = null;
                try
                {
                    var LOG_FilePath = HttpContext.Current.Server.MapPath("~/Logs/") + DateTime.Now.ToString("dd_MM_yyyy").ToString() + ".txt";
                    oStreamWriter = new StreamWriter(LOG_FilePath, true);
                    oStreamWriter.WriteLine(DateTime.Now.ToString());
                    oStreamWriter.WriteLine(message);
                    oStreamWriter.WriteLine("-----------------------------------------------------------------------------------------------------");
                }
                catch
                {
                }
                finally
                {
                    if (oStreamWriter != null)
                    {
                        oStreamWriter.Close();
                    }
                }
            }
        }

        public static void ExceptionLog(Exception ex, string ExceptionId)
        {
            lock (typeof(LOG))
            {
                StreamWriter oStreamWriter = null;
                try
                {
                    var LOG_FilePath = HttpContext.Current.Server.MapPath("~/Logs/") + "Exception/" + ExceptionId + ".txt";
                    oStreamWriter = new StreamWriter(LOG_FilePath, true);
                    oStreamWriter.WriteLine(DateTime.Now.ToString());
                    oStreamWriter.WriteLine(ex.Message + System.Environment.NewLine + ex.InnerException);
                    oStreamWriter.WriteLine("-----------------------------------------------------------------------------------------------------");
                }
                catch
                {
                }
                finally
                {
                    if (oStreamWriter != null)
                    {
                        oStreamWriter.Close();
                    }
                }
            }
        }
    }
}
﻿using Newtonsoft.Json;
using PartnerDeclarationPortal.Areas.api.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace PartnerDeclarationPortal.App_Code
{
    public class DBContext
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        string AES_KEY = "RedAppleKey@2020";

        public User GetUser()

        {
            var _UserAccount = new User();

            using (SqlConnection con = new SqlConnection(this.ConnectionString))
            {
                con.Open();
                var _SQL = "SELECT * FROM DPP_Partners";
                using (SqlCommand cmd = new SqlCommand(_SQL, con))
                {

                    using (SqlDataAdapter dpt = new SqlDataAdapter(cmd))
                    {
                        var dtUsers = new DataTable();

                        dpt.Fill(dtUsers);

                        if (dtUsers.Rows.Count > 0)
                        {
                            // _User.Id = dtUsers.Rows[0]["Id"] == 0 ? 0 : dtUsers.Rows[0]["Id"];
                            _UserAccount.PartnerName = dtUsers.Rows[0]["PartnerName"] == DBNull.Value ? "" : dtUsers.Rows[0]["PartnerName"].ToString();
                            _UserAccount.PartnerCode = dtUsers.Rows[0]["PartnerCode"] == DBNull.Value ? "" : dtUsers.Rows[0]["PartnerCode"].ToString();
                            _UserAccount.Password = dtUsers.Rows[0]["Password"] == DBNull.Value ? "" : dtUsers.Rows[0]["Password"].ToString();
                            //_User.IsActive = dtUsers.Rows[0]["IsActive"] == DBNull.Value ? false : dtUsers.Rows[0]["IsActive"];
                            _UserAccount.CreatedOn = dtUsers.Rows[0]["CreatedOn"] == DBNull.Value ? "" : dtUsers.Rows[0]["CreatedOn"].ToString();

                        }
                    }
                }
            }

            return _UserAccount;
        }

        public User GetLoginUser(string Username)

        {
            var _UserAccount = new User();

            using (SqlConnection con = new SqlConnection(this.ConnectionString))
            {
                con.Open();
                var _SQL = "SELECT * FROM DPP_Partners where PartnerCode=@Username";
                using (SqlCommand cmd = new SqlCommand(_SQL, con))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Parameters.AddWithValue("@Username", Username);

                    using (SqlDataAdapter dpt = new SqlDataAdapter(cmd))
                    {
                        var dtUsers = new DataTable();

                        dpt.Fill(dtUsers);

                        if (dtUsers.Rows.Count > 0)
                        {
                            // _User.Id = dtUsers.Rows[0]["Id"] == 0 ? 0 : dtUsers.Rows[0]["Id"];
                            _UserAccount.PartnerName = dtUsers.Rows[0]["PartnerName"] == DBNull.Value ? "" : dtUsers.Rows[0]["PartnerName"].ToString();
                            _UserAccount.PartnerCode = dtUsers.Rows[0]["PartnerCode"] == DBNull.Value ? "" : dtUsers.Rows[0]["PartnerCode"].ToString();
                            _UserAccount.Password = dtUsers.Rows[0]["Password"] == DBNull.Value ? "" : dtUsers.Rows[0]["Password"].ToString();
                            _UserAccount.IsActive = Convert.ToBoolean(dtUsers.Rows[0]["IsActive"]) == true ? true : Convert.ToBoolean(dtUsers.Rows[0]["IsActive"]);
                            _UserAccount.CreatedOn = dtUsers.Rows[0]["CreatedOn"] == DBNull.Value ? "" : dtUsers.Rows[0]["CreatedOn"].ToString();

                        }
                    }
                }
            }

            return _UserAccount;
        }

        public DataTable CheckEmailSAP(string email)
        {
            var dt = new DataTable();

            try
            {

                string query = "select MAILID_TO, KUNNR from SAPSR3.ZFI_MAIL WHERE TYPE = 'SOA' AND UPPER(TRIM('' from MAILID_TO)) = '"+email+"'";
                query += "UNION select Email1 AS MAILID_TO,KUNNR_SP AS KUNNR from SAPSR3.ZRCPARTNER_DET where UPPER(TRIM('' from Email1)) = '" + email + "'";
                query += "UNION select MAILID_TO,KUNNR from SAPSR3.ZFI_mail where TYPE = 'DIC' AND UPPER(TRIM('' from MAILID_TO)) = '" + email + "'";
                 //dt = HanaServiceLive("select * from SAPSR3.ZFI_MAIL WHERE TYPE = 'SOA' AND UPPER(TRIM('' from MAILID_TO)) =  '" + email + "'");
               dt = HanaServiceLive(query);
                LOG.Info("CheckEmailSAP - Success -" + email);

            }
            catch (Exception ee)
            {
                //res = ee.Message.ToString();
                LOG.Exception(ee);
            }

            return dt;


        }
        public User InsertOTP(string email, string otp)
        {
            var _UserAccount = new User();
            LOG.Info("InsertOTP - START -");
            var res = "";
            SqlConnection sCon = new SqlConnection(this.ConnectionString);
            SqlCommand sCmd;
            DataSet ds = new DataSet();
            SqlDataAdapter sDA = new SqlDataAdapter();
            sCmd = new SqlCommand("SP_InsertOtp", sCon);
            sCmd.CommandType = CommandType.StoredProcedure;
            sCmd.Parameters.Add(new SqlParameter("@Email", email));
            sCmd.Parameters.Add(new SqlParameter("@Otp", otp));
            sCmd.Parameters.Add(new SqlParameter("@IP", GetIPAddress()));
            try
            {
                sCon.Open();
                sCmd.ExecuteNonQuery();
                LOG.Info("InsertOTP - Success -" + email);
            }
            catch (Exception ee)
            {
                res = ee.Message.ToString();
                LOG.Info("InsertOTP - Error -" + email);
            }
            finally
            {
                sCon.Close();
            }
            return _UserAccount;
        }

        public DataTable GetOTPData(string email)
        {
            LOG.Info("GetOTPData - START -" + email);
            var res = "";

            SqlConnection sCon = new SqlConnection(this.ConnectionString);
            SqlCommand sCmd;
            DataTable ds = new DataTable();
            SqlDataAdapter sDA = new SqlDataAdapter();
            sCmd = new SqlCommand("SP_GetOTPData", sCon);
            sCmd.CommandType = CommandType.StoredProcedure;
            sCmd.Parameters.Add(new SqlParameter("@Email", email));
            try
            {
                sCon.Open();
                sDA = new SqlDataAdapter(sCmd);
                sDA.Fill(ds);
                LOG.Info("GetOTPData - Success -" + email);
            }
            catch (Exception ee)
            {
                res = ee.Message.ToString();
                LOG.Info("GetOTPData - Error -" + email);
            }
            finally
            {
                sCon.Close();
            }
            return ds;
        }
        protected static string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        public DataTable HanaServiceLive(string MethodName)
        {
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072 | (SecurityProtocolType)768 | (SecurityProtocolType)192;
            DataTable dt = new DataTable();
            try
            {
                Query qr = new Query();
                qr.query = MethodName;
                dynamic reqData = JsonConvert.SerializeObject(qr);
                WebRequest request = WebRequest.Create("https://ril-edi.redington.co.in/DBConnectorProd/api/DB/getdataPost");
                // Set the Method property of the request to POST.  
                request.Method = "POST";
                // Create POST data and convert it to a byte array.  
                string postData = reqData;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.  
                request.ContentType = "application/json";// "application/x-www-form-urlencoded";

                // Set the ContentLength property of the WebRequest.  
                request.ContentLength = byteArray.Length;
                // Get the request stream.  
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.  
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.  
                dataStream.Close();


                var myWebResponse = request.GetResponse();
                var responseStream = myWebResponse.GetResponseStream();
                if (responseStream == null) return null;
                var myStreamReader = new StreamReader(responseStream, Encoding.Default);
                var json = myStreamReader.ReadToEnd();
                responseStream.Close();
                myWebResponse.Close();

                var jsonser = JsonConvert.DeserializeObject<RootObject>(json);
                var format = "";

                if (jsonser != null)
                {
                    if (jsonser.Data != null)
                    {
                        if (jsonser.Data.ToString().Contains("["))
                        {
                            format = jsonser.Data.ToString();
                        }
                        else
                        {
                            format = "[" + jsonser.Data.ToString() + "]";
                        }
                        dt = (DataTable)JsonConvert.DeserializeObject(format, (typeof(DataTable)));
                    }
                }
            }
            catch (Exception ex)
            {
                //SAPLog(ex.Message + ex.StackTrace, "", MethodName);
            }
            return dt;
        }

        public bool Contactus(string name,string emailid,string phone,string country,string city,int pobox,string website,string type_of_query,string remarks)
        {
            try
            {
                SqlConnection con = new SqlConnection(this.ConnectionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("sp_contactusinsert", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@emailid", emailid);
                cmd.Parameters.AddWithValue("@phone", phone);
                cmd.Parameters.AddWithValue("@country", country);
                cmd.Parameters.AddWithValue("@city", city);
                cmd.Parameters.AddWithValue("@pobox", pobox);
                cmd.Parameters.AddWithValue("@website", website);
                cmd.Parameters.AddWithValue("@type_of_query", type_of_query);
                cmd.Parameters.AddWithValue("@remarks", remarks);
                cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
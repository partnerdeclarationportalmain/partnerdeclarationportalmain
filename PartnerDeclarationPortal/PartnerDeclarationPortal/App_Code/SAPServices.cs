﻿using Newtonsoft.Json;
using PartnerDeclarationPortal.Areas.api.Models.SAP;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace PartnerDeclarationPortal.App_Code
{
    public class SAPServices
    {
        #region Connection String

        //DRIVER={HDBODBC};UID=INTJBANET;PWD=Red@Sap2021;SERVERNODE=90.90.90.216:33015;DATABASENAME=SAPSR3
        //string _ConnectionString = "DRIVER={HDBODBC};UID=INTJBANET;PWD=Red@Sap2021;SERVERNODE=90.90.90.216:33015;"; //Production
        string _ConnectionString = "DRIVER={HDBODBC};UID=INTJBANET;PWD=Red@2020;SERVERNODE=172.17.129.147:31047;"; //Quality

        string _ConnectionString_ADO = string.Format("server={0};userid={1};password={2};", ConfigurationManager.AppSettings["SAPServerIPAndPort"],
            ConfigurationManager.AppSettings["SAPServerUsername"], ConfigurationManager.AppSettings["SAPServerPassword"]);

        #endregion

        #region Command Type Enumeration

        public enum SAPQueryType { PlainSQL = 1, StoredProcedure = 2, CDSView = 3 }

        #endregion

        public SAPQueryType CommandType { get; set; }
        public string CommandText { get; set; }
        public List<string> CommandList { get; set; }

        //public SAPServices()
        //{
        //    this.CommandList = new List<string>();
        //}

        //public System.Data.DataTable ExecuteDataTable()
        //{
        //    System.Data.DataTable dtResult = new System.Data.DataTable();

        //    if (this.CommandType == SAPQueryType.PlainSQL)
        //    {
        //        try
        //        {
        //            using (HanaConnection con = new HanaConnection(_ConnectionString_ADO))
        //            {
        //                con.Open();
        //                using (HanaCommand cmd = new HanaCommand(this.CommandText, con))
        //                {
        //                    cmd.CommandType = System.Data.CommandType.Text;
        //                    using (HanaDataAdapter dpt = new HanaDataAdapter(cmd))
        //                    {
        //                        dpt.Fill(dtResult);
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            LOG.Info("Error while ExecuteDataTable in SAPService:" + ex.Message);
        //            LOG.Exception(ex);
        //        }
        //    }

        //    return dtResult;
        //}

        //public System.Data.DataSet ExecuteDataSet()
        //{
        //    var dsResult = new System.Data.DataSet();
        //    System.Data.DataTable dtResult = new System.Data.DataTable();

        //    if (this.CommandType == SAPQueryType.PlainSQL)
        //    {
        //        try
        //        {
        //            using (HanaConnection con = new HanaConnection(_ConnectionString_ADO))
        //            {
        //                con.Open();

        //                foreach (string _CommandText in this.CommandList)
        //                {
        //                    using (HanaCommand cmd = new HanaCommand(_CommandText, con))
        //                    {
        //                        cmd.CommandType = System.Data.CommandType.Text;
        //                        using (HanaDataAdapter dpt = new HanaDataAdapter(cmd))
        //                        {
        //                            dtResult = new System.Data.DataTable();
        //                            dpt.Fill(dtResult);
        //                            dsResult.Tables.Add(dtResult);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            LOG.Info("Error while ExecuteDataTable in SAPService:" + ex.Message);
        //            LOG.Exception(ex);
        //        }
        //    }

        //    return dsResult;
        //}

        public static dynamic POST_CFCAll(dynamic Request)
        {
            try
            {
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                SAPResponse dlist = new SAPResponse();
                //string AccessToken = ConfigurationManager.AppSettings["SAPCredentials"].ToString();
                WebRequest request = WebRequest.Create("https://redington-bizboard.com/payment/loanframe/invoice");
                // Set the Method property of the request to POST.  
                request.Method = "POST";
                // Create POST data and convert it to a byte array.  
                string postData = Request;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.  
                //request.ContentType = "application/x-www-form-urlencoded";
                request.ContentType = "application/json";

                var myHttpWebRequest = (HttpWebRequest)request;
                myHttpWebRequest.PreAuthenticate = true;
                //myHttpWebRequest.Headers.Add("Authorization", "Basic " + AccessToken);
                myHttpWebRequest.Accept = "application/json";


                // Set the ContentLength property of the WebRequest.  
                request.ContentLength = byteArray.Length;
                // Get the request stream.  
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.  
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.  
                dataStream.Close();
                // Get the response.  
                WebResponse response = request.GetResponse();
                // Display the status.  
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.  
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                string json = reader.ReadToEnd();
                // Display the content.  

                // Clean up the streams.  
                reader.Close();
                dataStream.Close();
                response.Close();

                var jsonResponse = JsonConvert.DeserializeObject<SAPResponse>(json);
                return json;
            }
            catch (Exception ex)
            {
                //SAPLog(ex.Message.ToString(), "", MethodName + "-Err");
            }

            return "";
        }

        public static DataTable POST(string MethodName, dynamic Request)
        {
            //SAPLog(Request, "", MethodName + "-Req");
            DataTable dt = new DataTable();
            try
            {
                SAPResponse dlist = new SAPResponse();
                string AccessToken = ConfigurationManager.AppSettings["SAPCredentials"].ToString();
                WebRequest request = WebRequest.Create(ConfigurationManager.AppSettings["SAPUrl"].ToString() + MethodName);
                // Set the Method property of the request to POST.  
                request.Method = "POST";
                // Create POST data and convert it to a byte array.  
                string postData = Request;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.  
                //request.ContentType = "application/x-www-form-urlencoded";
                request.ContentType = "application/json";

                var myHttpWebRequest = (HttpWebRequest)request;
                myHttpWebRequest.PreAuthenticate = true;
                myHttpWebRequest.Headers.Add("Authorization", "Basic " + AccessToken);
                myHttpWebRequest.Accept = "application/json";


                // Set the ContentLength property of the WebRequest.  
                request.ContentLength = byteArray.Length;
                // Get the request stream.  
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.  
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.  
                dataStream.Close();
                // Get the response.  
                WebResponse response = request.GetResponse();
                // Display the status.  
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.  
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                string json = reader.ReadToEnd();
                // Display the content.  

                // Clean up the streams.  
                reader.Close();
                dataStream.Close();
                response.Close();



                var jsonser = JsonConvert.DeserializeObject<SAPResponse>(json);
                //SAPLog(jsonser.Data.ToString(), "", MethodName + "-Resp");
                var format = "";

                if (jsonser != null)
                {
                    if ((jsonser.Data != null))
                    {
                        if (jsonser.Data.ToString().Contains("["))
                        {
                            format = jsonser.Data.ToString();
                        }
                        else
                        {
                            format = "[" + jsonser.Data.ToString() + "]";
                        }

                        dt = (DataTable)JsonConvert.DeserializeObject(format, (typeof(DataTable)));
                    }
                }
            }
            catch (Exception ex)
            {
                //SAPLog(ex.Message.ToString(), "", MethodName + "-Err");
            }

            return dt;
        }

        public static dynamic POST_JSON_RESPONSE(string MethodName, dynamic Request)
        {
            try
            {
                SAPResponse dlist = new SAPResponse();
                string AccessToken = ConfigurationManager.AppSettings["SAPCredentials"].ToString();
                WebRequest request = WebRequest.Create(ConfigurationManager.AppSettings["SAPUrl"].ToString() + MethodName);
                // Set the Method property of the request to POST.  
                request.Method = "POST";
                // Create POST data and convert it to a byte array.  
                string postData = Request;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.  
                //request.ContentType = "application/x-www-form-urlencoded";
                request.ContentType = "application/json";

                var myHttpWebRequest = (HttpWebRequest)request;
                myHttpWebRequest.PreAuthenticate = true;
                myHttpWebRequest.Headers.Add("Authorization", "Basic " + AccessToken);
                myHttpWebRequest.Accept = "application/json";


                // Set the ContentLength property of the WebRequest.  
                request.ContentLength = byteArray.Length;
                // Get the request stream.  
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.  
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.  
                dataStream.Close();
                // Get the response.  
                WebResponse response = request.GetResponse();
                // Display the status.  
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.  
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                string json = reader.ReadToEnd();
                // Display the content.  

                // Clean up the streams.  
                reader.Close();
                dataStream.Close();
                response.Close();

                var jsonResponse = JsonConvert.DeserializeObject<SAPResponse>(json);
                return jsonResponse.Data[0];
            }
            catch (Exception ex)
            {
                //SAPLog(ex.Message.ToString(), "", MethodName + "-Err");
            }

            return "";
        }

        public static dynamic POST_JSON(string MethodName, dynamic Request)
        {
            try
            {
                SAPResponse dlist = new SAPResponse();
                string AccessToken = ConfigurationManager.AppSettings["SAPCredentials"].ToString();
                WebRequest request = WebRequest.Create(ConfigurationManager.AppSettings["SAPUrl"].ToString() + MethodName);
                // Set the Method property of the request to POST.  
                request.Method = "POST";
                // Create POST data and convert it to a byte array.  
                string postData = Request;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.  
                //request.ContentType = "application/x-www-form-urlencoded";
                request.ContentType = "application/json";

                var myHttpWebRequest = (HttpWebRequest)request;
                myHttpWebRequest.PreAuthenticate = true;
                myHttpWebRequest.Headers.Add("Authorization", "Basic " + AccessToken);
                myHttpWebRequest.Accept = "application/json";


                // Set the ContentLength property of the WebRequest.  
                request.ContentLength = byteArray.Length;
                // Get the request stream.  
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.  
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.  
                dataStream.Close();
                // Get the response.  
                WebResponse response = request.GetResponse();
                // Display the status.  
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.  
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                string json = reader.ReadToEnd();
                // Display the content.  

                // Clean up the streams.  
                reader.Close();
                dataStream.Close();
                response.Close();

                var jsonResponse = JsonConvert.DeserializeObject<SAPResponse>(json);
                return json;
            }
            catch (Exception ex)
            {
                //SAPLog(ex.Message.ToString(), "", MethodName + "-Err");
            }

            return "";
        }

        public static dynamic POST_JSON(string MethodName, dynamic Request,string env)
        {
            try
            {
                SAPResponse dlist = new SAPResponse();
                string AccessToken = ConfigurationManager.AppSettings["SAPCredentials" + env].ToString();
                WebRequest request = WebRequest.Create(ConfigurationManager.AppSettings["SAPUrl" + env].ToString()  + MethodName);
                // Set the Method property of the request to POST.  
                request.Method = "POST";
                // Create POST data and convert it to a byte array.  
                string postData = Request;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.  
                //request.ContentType = "application/x-www-form-urlencoded";
                request.ContentType = "application/json";

                var myHttpWebRequest = (HttpWebRequest)request;
                myHttpWebRequest.PreAuthenticate = true;
                myHttpWebRequest.Headers.Add("Authorization", "Basic " + AccessToken);
                myHttpWebRequest.Accept = "application/json";


                // Set the ContentLength property of the WebRequest.  
                request.ContentLength = byteArray.Length;
                // Get the request stream.  
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.  
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.  
                dataStream.Close();
                // Get the response.  
                WebResponse response = request.GetResponse();
                // Display the status.  
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.  
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                string json = reader.ReadToEnd();
                // Display the content.  

                // Clean up the streams.  
                reader.Close();
                dataStream.Close();
                response.Close();

                var jsonResponse = JsonConvert.DeserializeObject<SAPResponse>(json);
                return json;
            }
            catch (Exception ex)
            {
                //SAPLog(ex.Message.ToString(), "", MethodName + "-Err");
            }

            return "";
        }

        public static DataTable POST_SALE_ORDER(string MethodName, dynamic Request)
        {
            //SAPLog(Request, "", MethodName + "-Req");
            DataTable dt = new DataTable();
            try
            {
                SAPResponse dlist = new SAPResponse();
                string AccessToken = ConfigurationManager.AppSettings["SAPCredentials"].ToString();
                WebRequest request = WebRequest.Create(ConfigurationManager.AppSettings["SAPUrlSAleOrder"].ToString() + MethodName);
                // Set the Method property of the request to POST.  
                request.Method = "POST";
                // Create POST data and convert it to a byte array.  
                string postData = Request;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.  
                //request.ContentType = "application/x-www-form-urlencoded";
                request.ContentType = "application/json";

                var myHttpWebRequest = (HttpWebRequest)request;
                myHttpWebRequest.PreAuthenticate = true;
                myHttpWebRequest.Headers.Add("Authorization", "Basic " + AccessToken);
                myHttpWebRequest.Accept = "application/json";


                // Set the ContentLength property of the WebRequest.  
                request.ContentLength = byteArray.Length;
                // Get the request stream.  
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.  
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.  
                dataStream.Close();
                // Get the response.  
                WebResponse response = request.GetResponse();
                // Display the status.  
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.  
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                string json = reader.ReadToEnd();
                // Display the content.  

                // Clean up the streams.  
                reader.Close();
                dataStream.Close();
                response.Close();



                var jsonser = JsonConvert.DeserializeObject<SAPResponse>(json);
                //SAPLog(jsonser.Data.ToString(), "", MethodName + "-Resp");
                var format = "";

                if (jsonser != null)
                {
                    if ((jsonser.Data != null))
                    {
                        if (jsonser.Data.ToString().Contains("["))
                        {
                            format = jsonser.Data.ToString();
                        }
                        else
                        {
                            format = "[" + jsonser.Data.ToString() + "]";
                        }

                        dt = (DataTable)JsonConvert.DeserializeObject(format, (typeof(DataTable)));
                    }
                }
            }
            catch (Exception ex)
            {
                //SAPLog(ex.Message.ToString(), "", MethodName + "-Err");
            }

            return dt;
        }

        public static DataTable GET(string MethodAndRequest)
        {
            SAPResponse dlist = new SAPResponse();

            var myUri = new Uri(ConfigurationManager.AppSettings["SAPUrl"].ToString() + MethodAndRequest);
            string AccessToken = ConfigurationManager.AppSettings["SAPCredentials"].ToString();

            var myWebRequest = WebRequest.Create(myUri);
            var myHttpWebRequest = (HttpWebRequest)myWebRequest;
            myHttpWebRequest.PreAuthenticate = true;
            myHttpWebRequest.Headers.Add("Authorization", "Basic " + AccessToken);
            myHttpWebRequest.Accept = "application/json";

            var myWebResponse = myWebRequest.GetResponse();
            var responseStream = myWebResponse.GetResponseStream();
            if (responseStream == null) return null;

            var myStreamReader = new StreamReader(responseStream, Encoding.Default);
            var json = myStreamReader.ReadToEnd();

            responseStream.Close();
            myWebResponse.Close();

            DataTable dt = new DataTable();
            var jsonser = JsonConvert.DeserializeObject<SAPResponse>(json);
            dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));

            return dt;
        }

        public static SAPResponse GET_JSON_RESPONSE(string MethodAndRequest)
        {
            SAPResponse dlist = new SAPResponse();

            var myUri = new Uri(ConfigurationManager.AppSettings["SAPUrl"].ToString() + MethodAndRequest);
            string AccessToken = ConfigurationManager.AppSettings["SAPCredentials"].ToString();

            var myWebRequest = WebRequest.Create(myUri);
            var myHttpWebRequest = (HttpWebRequest)myWebRequest;
            myHttpWebRequest.PreAuthenticate = true;
            myHttpWebRequest.Headers.Add("Authorization", "Basic " + AccessToken);
            myHttpWebRequest.Accept = "application/json";

            var myWebResponse = myWebRequest.GetResponse();
            var responseStream = myWebResponse.GetResponseStream();
            if (responseStream == null) return null;

            var myStreamReader = new StreamReader(responseStream, Encoding.Default);
            var json = myStreamReader.ReadToEnd();

            responseStream.Close();
            myWebResponse.Close();

            DataTable dt = new DataTable();
            var _SAPResponse = JsonConvert.DeserializeObject<SAPResponse>(json);
            return _SAPResponse;
        }

        public static dynamic GET_JSON(string MethodAndRequest)
        {
            SAPResponse dlist = new SAPResponse();

            var myUri = new Uri(ConfigurationManager.AppSettings["SAPUrl"].ToString() + MethodAndRequest);
            string AccessToken = ConfigurationManager.AppSettings["SAPCredentials"].ToString();

            var myWebRequest = WebRequest.Create(myUri);
            var myHttpWebRequest = (HttpWebRequest)myWebRequest;
            myHttpWebRequest.PreAuthenticate = true;
            myHttpWebRequest.Headers.Add("Authorization", "Basic " + AccessToken);
            myHttpWebRequest.Accept = "application/json";

            var myWebResponse = myWebRequest.GetResponse();
            var responseStream = myWebResponse.GetResponseStream();
            if (responseStream == null) return null;

            var myStreamReader = new StreamReader(responseStream, Encoding.Default);
            var json = myStreamReader.ReadToEnd();

            responseStream.Close();
            myWebResponse.Close();

            DataTable dt = new DataTable();
            var _SAPResponse = JsonConvert.DeserializeObject<SAPResponse>(json);
            return json;
        }

        public static DataTable GET_ADDRESS(string MethodAndRequest)
        {
            SAPResponse dlist = new SAPResponse();

            var myUri = new Uri(ConfigurationManager.AppSettings["SAPUrl"].ToString() + MethodAndRequest);
            string AccessToken = ConfigurationManager.AppSettings["SAPCredentials"].ToString();

            var myWebRequest = WebRequest.Create(myUri);
            var myHttpWebRequest = (HttpWebRequest)myWebRequest;
            myHttpWebRequest.PreAuthenticate = true;
            myHttpWebRequest.Headers.Add("Authorization", "Basic " + AccessToken);
            myHttpWebRequest.Accept = "application/json";

            var myWebResponse = myWebRequest.GetResponse();
            var responseStream = myWebResponse.GetResponseStream();
            if (responseStream == null) return null;

            var myStreamReader = new StreamReader(responseStream, Encoding.Default);
            var json = myStreamReader.ReadToEnd();

            responseStream.Close();
            myWebResponse.Close();

            DataTable dt = new DataTable();
            var jsonser = JsonConvert.DeserializeObject<SAPResponse>(json);
            dt = (DataTable)JsonConvert.DeserializeObject(jsonser.Data.ToString(), (typeof(DataTable)));

            return dt;
        }


        public static dynamic PostWhatsUP_JSON(string MethodName, dynamic Request)
        {
            DataTable dt = new DataTable();
            string json = "";
            try
            {
                SAPResponse dlist = new SAPResponse();
             string AccessToken = ConfigurationManager.AppSettings["SAPCredentials"].ToString();
                //string AccessToken = "dGVzdDpSZWRpbmd0b25AMjAxOA==";
                WebRequest request = WebRequest.Create(ConfigurationManager.AppSettings["SAPWhatsup"].ToString() + MethodName);
                // Set the Method property of the request to POST.  
                request.Method = "POST";
                // Create POST data and convert it to a byte array.  
                string postData = Request;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.  
                //request.ContentType = "application/x-www-form-urlencoded";
                request.ContentType = "application/json";

                var myHttpWebRequest = (HttpWebRequest)request;
                myHttpWebRequest.PreAuthenticate = true; 
                myHttpWebRequest.Headers.Add("Authorization", "Basic " + AccessToken);
                myHttpWebRequest.Accept = "application/json";


                // Set the ContentLength property of the WebRequest.  
                request.ContentLength = byteArray.Length;
                // Get the request stream.  
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.  
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.  
                dataStream.Close();
                // Get the response.  
                WebResponse response = request.GetResponse();
                // Display the status.  
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.  
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                 json = reader.ReadToEnd();
                // Display the content.  

                // Clean up the streams.  
                reader.Close();
                dataStream.Close();
                response.Close();
                var _SAPResponse = JsonConvert.DeserializeObject<SAPResponse>(json);
                return json;


                /*    var jsonser = JsonConvert.DeserializeObject<SAPResponse>(json);
                    if (jsonser != null)
                    {
                        if ((jsonser.Data != null))
                        {
                            if (jsonser.Data.ToString().Contains("["))
                            {
                                format = jsonser.Data.ToString();
                            }
                            else
                            {
                                format = "[" + jsonser.Data.ToString() + "]";
                            }

                            dt = (DataTable)JsonConvert.DeserializeObject(format, (typeof(DataTable)));
                        }
                    }*/


            }
            catch (Exception ex)
            {
               
                //SAPLog(ex.Message.ToString(), "", MethodName + "-Err");
            }

            return "";
        }


        #region HANA call with external API POST
        public static DataTable HanaService(string MethodName)
        {
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072 | (SecurityProtocolType)768 | (SecurityProtocolType)192;
            DataTable dt = new DataTable();
            try
            {
                Query qr = new Query();
                qr.query = MethodName;
                dynamic reqData = JsonConvert.SerializeObject(qr);
                WebRequest request = WebRequest.Create("https://ril-edi.redington.co.in/DBConnectorProd/api/DB/getdataPost");
                // Set the Method property of the request to POST.  
                request.Method = "POST";
                // Create POST data and convert it to a byte array.  
                string postData = reqData;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.  
                request.ContentType = "application/json";// "application/x-www-form-urlencoded";

                // Set the ContentLength property of the WebRequest.  
                request.ContentLength = byteArray.Length;
                // Get the request stream.  
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.  
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.  
                dataStream.Close();


                var myWebResponse = request.GetResponse();
                var responseStream = myWebResponse.GetResponseStream();
                if (responseStream == null) return null;
                var myStreamReader = new StreamReader(responseStream, Encoding.Default);
                var json = myStreamReader.ReadToEnd();
                responseStream.Close();
                myWebResponse.Close();

                var jsonser = JsonConvert.DeserializeObject<RootObject>(json);
                var format = "";

                if (jsonser != null)
                {
                    if (jsonser.Data != null)
                    {
                        if (jsonser.Data.ToString().Contains("["))
                        {
                            format = jsonser.Data.ToString();
                        }
                        else
                        {
                            format = "[" + jsonser.Data.ToString() + "]";
                        }
                        dt = (DataTable)JsonConvert.DeserializeObject(format, (typeof(DataTable)));
                    }
                }
            }
            catch (Exception ex)
            {
                //SAPLog(ex.Message + ex.StackTrace, "", MethodName);
            }
            return dt;
        }
        public static DataTable HanaServiceDev(string MethodName)
        {
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072 | (SecurityProtocolType)768 | (SecurityProtocolType)192;
            DataTable dt = new DataTable();
            try
            {
                Query qr = new Query();
                qr.query = MethodName;
                dynamic reqData = JsonConvert.SerializeObject(qr);
                WebRequest request = WebRequest.Create("https://ril-edi.redington.co.in/DBConnector_Dev/api/DB/getdataPost");
                // Set the Method property of the request to POST.  
                request.Method = "POST";
                // Create POST data and convert it to a byte array.  
                string postData = reqData;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.  
                request.ContentType = "application/json";// "application/x-www-form-urlencoded";

                // Set the ContentLength property of the WebRequest.  
                request.ContentLength = byteArray.Length;
                // Get the request stream.  
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.  
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.  
                dataStream.Close();


                var myWebResponse = request.GetResponse();
                var responseStream = myWebResponse.GetResponseStream();
                if (responseStream == null) return null;
                var myStreamReader = new StreamReader(responseStream, Encoding.Default);
                var json = myStreamReader.ReadToEnd();
                responseStream.Close();
                myWebResponse.Close();

                var jsonser = JsonConvert.DeserializeObject<RootObject>(json);
                var format = "";

                if (jsonser != null)
                {
                    if (jsonser.Data != null)
                    {
                        if (jsonser.Data.ToString().Contains("["))
                        {
                            format = jsonser.Data.ToString();
                        }
                        else
                        {
                            format = "[" + jsonser.Data.ToString() + "]";
                        }
                        dt = (DataTable)JsonConvert.DeserializeObject(format, (typeof(DataTable)));
                    }
                }
            }
            catch (Exception ex)
            {
                //SAPLog(ex.Message + ex.StackTrace, "", MethodName);
            }
            return dt;
        }
        #endregion
    }
}

public class Query
{
    public string query { get; set; }
}

public class RootObject
{
    public string Message { get; set; }
    public string StatusCode { get; set; }
    public dynamic Data { get; set; }
}
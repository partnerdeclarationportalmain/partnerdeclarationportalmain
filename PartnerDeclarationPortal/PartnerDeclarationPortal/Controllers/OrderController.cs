﻿using Newtonsoft.Json;
using PartnerDeclarationPortal.App_Code;
using PartnerDeclarationPortal.Areas.api.Models.SAP;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using PartnerDeclarationPortal.Models;
using System.Drawing;

using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Security.Cryptography;
using System.Text;
using PartnerDeclarationPortal.Areas.api.Models;

namespace PartnerDeclarationPortal.Controllers
{
    public class OrderController : Controller
    {
        //
        // GET: /Order/
        public ActionResult Index()
        {
         
                      return View();

        }
        
        [HttpPost]
        public ActionResult praise()
        {
            GetYears();
            var model = new MyListModel();
            // assign other SelectListItem list values here
            ViewBag.DrbValue = model;
            return View(model);
          
        }
       
        public ActionResult WentWrong()
        {
            if (Session["status_code"] == "500")
            {
                return RedirectToAction("Waiting", "Order");
            }
            return View();
        }


        public ActionResult Profile()
        {
            var auth_users = ConfigurationManager.AppSettings["LOCAL_USERS"].ToString();
            int pos = Array.IndexOf(auth_users.Split(','), System.Web.HttpContext.Current.Session["Email"].ToString().Trim().ToUpper());
            if (pos > -1)
            {
                this.Session["CustomerCode"] = "S10003";
            }
            else
            {
                this.Session["CustomerCode"] = System.Web.HttpContext.Current.Session["CustomerCode"];
            }

            var dt = SAPServices.HanaService("SELECT Name1 from  SAPSR3.KNA1 where LENGTH(KUNNR) = 6  AND KUNNR ='" + Session["CustomerCode"].ToString() + "'");
            if (dt.Rows.Count > 0)
            {
                Session["CustomerName"] = dt.Rows[0]["Name1"].ToString();
            }

            return View();
        }

        public ActionResult setsession(string _session)
        {
            Session["CustomerCode"] = _session;
            return RedirectToAction("Profile", "Order");
        }


        public ActionResult AddressList()
        {
            this.Session["CustomerCode"] = System.Web.HttpContext.Current.Session["CustomerCode"];
            return View();
        }

        public ActionResult PendingCheques()
        {
            this.Session["CustomerCode"] = System.Web.HttpContext.Current.Session["CustomerCode"];
            return View();
        }

        public ActionResult PaymentDues()
        {
            this.Session["CustomerCode"] = System.Web.HttpContext.Current.Session["CustomerCode"];
            return View();
        }

        public ActionResult OrderHistory()
        {

            this.Session["CustomerCode"] = System.Web.HttpContext.Current.Session["CustomerCode"];
            var dt = SAPServices.HanaService("SELECT Name1 from  SAPSR3.KNA1 where LENGTH(KUNNR) = 6  AND KUNNR ='" + Session["CustomerCode"].ToString() + "'");
            if (dt.Rows.Count > 0)
            {
                Session["CustomerName"] = dt.Rows[0]["Name1"].ToString();
            }



            return View();
        }

        public ActionResult CDSView()
        {
            this.Session["CustomerCode"] = System.Web.HttpContext.Current.Session["CustomerCode"];
            var dt = SAPServices.HanaService("select * from SAPSR3.ZCDS_ZFI_TDS('S10003','20190101','20220101')");
            if (dt.Rows.Count > 0)
            {
                Session["CustomerName"] = dt.Rows[0]["CUSTOMER_NAME"].ToString();
            }
            List<CDSDetails> CDSList = new List<CDSDetails>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CDSDetails CDSList1 = new CDSDetails();
                CDSList1.CUSTOMER_CODE = dt.Rows[i]["CUSTOMER_CODE"].ToString();
                CDSList1.CUSTOMER_NAME = dt.Rows[i]["CUSTOMER_NAME"].ToString();
                CDSList1.DOCUMENT_NO = dt.Rows[i]["DOCUMENT_NO"].ToString();
                CDSList1.DOCUMENT_TYPE = dt.Rows[i]["DOCUMENT_TYPE"].ToString();
                CDSList1.DOCUMENT_DATE = dt.Rows[i]["DOCUMENT_DATE"].ToString();
                CDSList1.OS_AMOUNT = dt.Rows[i]["OS_AMOUNT"].ToString();
                CDSList1.CUSTOMER_PROFILE = dt.Rows[i]["CUSTOMER_PROFILE"].ToString();
                CDSList.Add(CDSList1);
            }

            ViewBag.DownloadedDetails = CDSList.ToList();
            return View();

        }
        public ActionResult OrderDetails()
        {
            return View();
        }

        public ActionResult PaymentDuesByWeek()
        {
            return View();
        }

        public ActionResult PaymentDuesByWeekNew()
        {
            return View();
        }

        public ActionResult PaymentDuesByWeekOutstanding()
        {
            return View();
        }

        public ActionResult Contact()
        {
            string data = System.Web.HttpContext.Current.Session["CustomerCode"].ToString();
            string key="MAKV2SPBNI992121";
            string str;
            byte[] iv = new byte[16];
            byte[] array;

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(data);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            str= Convert.ToBase64String (array);
            byte[] convertedByte = Encoding.Unicode.GetBytes(str);

            byte[] bytes = Convert.FromBase64String(str);
            string hex = BitConverter.ToString(bytes);
            string hexString = hex.Replace("-", "");

            string enccoed = "https://edi.redingtonb2b.in/PartnerContact/Update/ByPartner?PartnerCode=" + hexString;
            return Redirect(enccoed);
        }


        public ActionResult PartnerEnquiry()
        {
                       return View();
        }
        public ActionResult Profile1()
        {
           

            return View();
        }

        public ActionResult FinancialUpload()
        {
                        return View();
        }
        [HttpGet]
        public ActionResult UploadFile()
        {
            GetYears();
            return View();
        }
        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase[] file)
        {
            string filename = "";
               SqlConnection ConnectionString = new SqlConnection(ConfigurationManager.ConnectionStrings["PartnerPortalConnection"].ConnectionString);
            int i = 1;
            string cuscode = System.Web.HttpContext.Current.Session["CustomerCode"].ToString();
            string FynType = Request.Form["FynType"].ToString();
            string FynYear = Request.Form["Year"].ToString();
            //string FynQtr = Request.Form["FynQtr"].ToString();
            try
            {                        
                        
                        foreach (HttpPostedFileBase files in file)
                    {

                    string FileExtension = Path.GetExtension(files.FileName);
                    string _FileName = Path.GetFileName(files.FileName);
                    if (FileExtension == ".jpg"||FileExtension== ".png")
                    {
                     

                      
                         filename = cuscode + FynType + FynYear+"_"+i + FileExtension;
                        string _path = Path.Combine(Server.MapPath("~/DOCS"), filename);
                        files.SaveAs(_path);
                        ImagesToPdf(_path, Server.MapPath("~/DOCS/" + cuscode + FynType + FynYear + "_" + i + ".pdf"));
                        System.IO.File.Delete(_path);
                    }
                    else {

                         filename = cuscode + FynType + FynYear + FileExtension;
                        string _path = Path.Combine(Server.MapPath("~/DOCS"), filename);
                        files.SaveAs(_path);

                    }

                    SqlCommand sCmd = new SqlCommand("PartnerPortalInsertDOC", ConnectionString);
                    sCmd.CommandType = CommandType.StoredProcedure;
                    sCmd.Parameters.Add(new SqlParameter("@Customer_Code", cuscode));
                    sCmd.Parameters.Add(new SqlParameter("@DOC_name", filename));
             
                    try
                    {
                        ConnectionString.Open();
                        sCmd.ExecuteNonQuery();
                        // LOG.Info("InsertOTP - Success -");
                    }
                    catch (Exception e)
                    { }

                                       i++;
                    
                }
                GetYears();
                ViewBag.Message = "File Uploaded Successfully!!";
                    return View();

                
            }
            catch
            {
                ViewBag.Message = "File upload failed!!";
                return View();
            }
        }

        private void GetYears()
        {
            string datee = DateTime.Now.AddYears(-1).ToString("yy");
            List<SelectListItem> Years = new List<SelectListItem>();
            DateTime startYear = DateTime.Now;
            while (startYear.Year <= DateTime.Now.AddYears(1).Year)
            {
                Years.Add(new SelectListItem
                {
                    Value = "FY" + startYear.Year + "_" + datee,
                    Text = "FY" + startYear.Year + "_" + datee
                });


                startYear = startYear.AddYears(1);
                datee = DateTime.Now.ToString("yy");
            }
            
            ViewBag.Years = Years;
        }

        public void ImagesToPdf(string imagepaths, string pdfpath)
        {
            using (var doc = new iTextSharp.text.Document())
            {
                iTextSharp.text.pdf.PdfWriter.GetInstance(doc, new System.IO.FileStream(pdfpath, System.IO.FileMode.Create));
                doc.Open();
               
                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagepaths);
                image.ScaleAbsolute(500f, 200f); // Set image size.
                                doc.Add(image);
                }            }


       
    }
}
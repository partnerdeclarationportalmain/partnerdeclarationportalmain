﻿var app = angular.module("Redington", []);

app.factory('ShoppingCart', function () {

    return {
        cartItemsCount: 0,
        cart: {}
    };

});

app.controller('Type_Details', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope.Typechanged = function (Type) {
        //window.location.href.indexOf("MVC") > -1 ? window.location.href : Type + window.location.href.replace("\MVC", "");//Type + window.location.pathname;

        debugger;
        if (Type == "MVC") {
            window.location.href = "MVC/Dashboard/CustomDates";
        }
        else {
            window.location.href = "Dashboard/CustomDates";
        }
    }

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();

    $scope.Portal = {};
    $scope.Portal.TypeName = 'Select';
    $scope.PortalTypeDetails = [];

    $scope.getAddressDetails = function () {

        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/GetPortalTypes?CustomerCode=' + $scope.CustomerCode,
            headers: { 'Content-Type': 'application/json' },
        }).then(function (data, status, header, config) {
            $scope.PortalTypeDetails = JSON.parse(JSON.stringify(data.data.Data));

            if (window.location.href.indexOf("MVC") > -1)
                $scope.Portal.TypeName = "MVC";
            else
                $scope.Portal.TypeName = "GeM";

            $scope.showLoader = false;
        }, function (data, status, header, config) {
            $scope.showLoader = false;
        });
    }

    $scope.getAddressDetails();
});

app.controller('Cart_Details', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();

    $scope.ShoppingCart = ShoppingCart;

    $scope.updateCart = function () {
        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/GetShoppingCart',
            headers: { 'Content-Type': 'application/json' },
        }).then(function (data, status, header, config) {

            console.log("Cart Result");
            console.log(data.data.Data);

            $scope.ShoppingCart.ItemsCount = data.data.Data.CartItemCount;
            $scope.ShoppingCart.Cart = data.data.Data.Items;

        }, function (data, status, header, config) {

        });
    }

    $scope.updateCart();

});

app.controller('Dashboard', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope.initShoppingCart = function (_ShoppingCart) {
        ShoppingCart.ItemsCount = _ShoppingCart.ItemsCount;
        ShoppingCart.Items = _ShoppingCart.Items;
    }

   
    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();

    $scope.CartItem = {};
    $scope.showLoader = false;
    $scope.ItemCategory = (new URL(location.href)).searchParams.get('Category');

    console.log($scope.ItemCategory);

    $scope.ProductList = [];
    $scope.showLoader = true;

    $scope.addItemToCart = function (item) {

        if (item.quantity <= 0) {
            $.toast({
                heading: 'Opps!',
                text: 'Please select quantity atleast 1.',
                position: {
                    right: 1,
                    top: 70
                },
                stack: true,
                hideAfter: 2000,
                icon: 'error'
            })
        } else {

            $scope.showLoader = false;
            //alert("Success " + item.quantity);

            $scope.request = {};

            $scope.request.Item_Code = item.Item_Code;
            $scope.request.Quantity = item.quantity;
            $scope.request.Product_Name = item.Item_Description;
            $scope.request.Item_Description = item.Item_Description;
            $scope.request.Item_Category = item.Item_Category;
            $scope.request.Vendor_Code = item.Vendor_Code;
            $scope.request.Brand = item.Brand;
            $scope.request.Actual_Price = item.Actual_Price;
            $scope.request.Discount_Percent = item.Discount_Percent;
            $scope.request.Discount_Price = item.Discount_Price;


            $http({
                method: 'POST',
                url: $scope._baseUrl + 'api/Order/AddCartItem',
                headers: { 'Content-Type': 'application/json' },
                data: $scope.request
            }).then(function (data, status, header, config) {

                console.log(data.data.Data);

                if (data.data.Status == "Success") {

                    $scope.initShoppingCart(data.data.Data);

                    $.toast({
                        heading: 'Cart Item',
                        text: 'Item added to cart successfully!',
                        position: {
                            right: 1,
                            top: 70
                        },
                        stack: true,
                        hideAfter: 1500,
                        icon: 'success'
                    })

                }
                //$scope.initShoppingCart(data.data.Data);

                NProgress.done();
                $scope.showLoader = false;

            }, function (data, status, header, config) {

                NProgress.done();
                $scope.showLoader = false;
            });

        }

    }

    NProgress.configure({ parent: '.col-full' });

    $scope.DashboardStats = {};
    $scope.DashboardStats.DropshipmentOrderHistroy = 0;
    $scope.DashboardStats.T2DocsUpload = 0;
    $scope.DashboardStats.PODCopyandCRACUpload = 0;
    $scope.DashboardStats.ExpiredDemands = 0;

    $scope.loadDashboardStats = function () {

        NProgress.start();
        debugger;
        $http({
            
            method: 'POST',
            //url: $scope._baseUrl + 'api/Order/GetProfile?CustomerCode=' + $scope.CustomerCode,
            url: $scope._baseUrl + 'api/Order/GET_Dashboard?CustomerCode=' + $scope.CustomerCode +
                '&_FromDate=' + $scope.dateFrom + '&_TillDate=' + $scope.dateTill,
            headers: { 'Content-Type': 'application/json' },
            data: $scope.request
        }).then(function (data, status, header, config) {

            console.log("Dashboard Response");
            console.log(data.data.Data);

            $scope.DashboardStats = JSON.parse(JSON.stringify(data.data.Data));

            NProgress.done();
        }, function (data, status, header, config) {
            NProgress.done();
        });

    }

    $scope.loadDashboardStats();


    $scope.formatNumber = function (x) {
        if (x != null || x != undefined) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        } else {
            return "0";
        }
    }

    $scope.loadDeliveredChart = function (chartData) {


        var jsonArray = new Array();
        $(chartData).each(function (ind, val) {
            jsonArray.push({ label: val.label, y: parseInt(val.y), color: "white" });
        });

        console.log(jsonArray);

        var chart4 = new CanvasJS.Chart("chartContainer4", {

            title: {
                text: ""
            },
            data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    dataPoints: jsonArray,
                    indexLabel: "{y}",
                    indexLabelFontSize: 14,
                    indexLabelFontColor: "white",
                    indexLabelPlacement: "outside"
                }
            ],
            showInLegend: false,
            backgroundColor: "rgba(0,0,0,0)",
            axisY: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function () {
                    return " ";
                },
                valueFormatString: ""
            },
            axisX: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function (e) {
                    return e.label.substring(0, 5);
                },
                labelAngle: -45,
                labelFontColor: "white",
                labelFontSize: 10
            },
            toolTip: {
                backgroundColor: "#606060"
            }
        });
        chart4.render();
    }

    $scope.loadDispatchedChart = function (chartData) {


        var jsonArray = new Array();
        $(chartData).each(function (ind, val) {
            jsonArray.push({ label: val.label, y: parseInt(val.y), color: "white" });
        });

        console.log(jsonArray);

        var chart3 = new CanvasJS.Chart("chartContainer3", {

            title: {
                text: ""
            },
            data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "line",
                    dataPoints: jsonArray,
                    indexLabel: "{y}",
                    indexLabelFontSize: 14,
                    indexLabelFontColor: "white",
                    indexLabelPlacement: "outside"
                }
            ],
            showInLegend: false,
            backgroundColor: "rgba(0,0,0,0)",
            axisY: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function () {
                    return " ";
                },
                valueFormatString: ""
            },
            axisX: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function (e) {
                    return e.label.substring(0, 5);
                },
                labelAngle: -45,
                labelFontColor: "white",
                labelFontSize: 10
            },
            toolTip: {
                backgroundColor: "#F34235"
            }
        });
        chart3.render();
    }

    $scope.loadInvoicedChart = function (chartData) {


        var jsonArray = new Array();
        $(chartData).each(function (ind, val) {
            jsonArray.push({ label: val.label, y: parseInt(val.y), color: "white" });
        });

        console.log(jsonArray);

        var chart2 = new CanvasJS.Chart("chartContainer2", {

            title: {
                text: ""
            },
            data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "line",
                    dataPoints: jsonArray,
                    indexLabel: "{y}",
                    indexLabelFontSize: 14,
                    indexLabelFontColor: "white",
                    indexLabelPlacement: "outside"
                }
            ],
            showInLegend: false,
            backgroundColor: "rgba(0,0,0,0)",
            axisY: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function () {
                    return " ";
                },
                valueFormatString: ""
            },
            axisX: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function (e) {
                    return e.label.substring(0, 5);
                },
                labelAngle: -45,
                labelFontColor: "white",
                labelFontSize: 10
            },
            toolTip: {
                backgroundColor: "#FE9700",
            },
        });
        chart2.render();
    }

    $scope.loadSOGeneratedChart = function (chartData) {

        var jsonArray = new Array();
        $(chartData).each(function (ind, val) {
            jsonArray.push({ label: val.label, y: parseInt(val.y), color: "white" });
        });

        console.log(jsonArray);

        var chart1 = new CanvasJS.Chart("chartContainer1", {

            title: {
                text: ""
            },
            data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    dataPoints: jsonArray,
                    indexLabel: "{y}",
                    indexLabelFontSize: 14,
                    indexLabelFontColor: "white",
                    indexLabelPlacement: "outside"
                }
            ],
            showInLegend: false,
            backgroundColor: "rgba(0,0,0,0)",
            axisY: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function () {
                    return " ";
                },
                valueFormatString: ""
            },
            axisX: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function (e) {
                    return e.label.substring(0, 5);
                },
                labelAngle: -45,
                labelFontColor: "white",
                labelFontSize: 10
            },
            toolTip: {
                backgroundColor: "#00BBD3",
            },
        });
        chart1.render();
    }

    $scope.loadPIEChart = function (Fulfilled, Pending, Expired) {

        var _Fulfilled = 0;
        var _Pending = 0;
        var _Expired = 0;

        ////if (Fulfilled == null || Fulfilled == undefined)
        ////    Fulfilled = 0;
        ////if (Pending == null || Pending == undefined)
        ////    Pending = 0;
        ////if (Expired == null || Expired == undefined)
        ////    Expired = 0;

        //if (parseInt(Fulfilled) < 100)
        //    _Fulfilled = parseInt(Fulfilled) + 100;
        //if (parseInt(Pending) < 100)
        //    _Pending = parseInt(Pending) + 100;
        //if (parseInt(Expired) < 100)
        //    _Expired = parseInt(Expired) + 100;

        //console.log(Fulfilled);
        //console.log(Pending);
        //console.log(Expired);

        var pieChart = new CanvasJS.Chart("pieChart", {

            title: {
                text: "All Demands"
            },
            data: [//array of dataSeries
                { //dataSeries object

                    /*** Change type "column" to "bar", "area", "line" or "pie"***/
                    type: "pie",
                    dataPoints: [
                        { x: "Fulfilled Demands", label: "Fulfilled Demands (" + Fulfilled + ")", y: Fulfilled, color: "#D22E2E" },
                        { x: "Pending Demands", label: "Pending Demands (" + Pending + ")", y: Pending, color: "#378D3B" },
                        { x: "Expired Demands", label: "Expired Demands (" + Expired + ")", y: Expired, color: "#0096A6" }
                    ],
                    click: function (e) {
                        if (e.dataPoint.x == "Pending Demands") {
                            $scope.loadCategoryChart($scope.DashboardStats.CategoryPending, "Category wise Pending Demands");
                        }
                        else if (e.dataPoint.x == "Fulfilled Demands") {
                            $scope.loadCategoryChart($scope.DashboardStats.CategoryFulfilled, "Category wise Fulfilled Demands");
                        }
                        else if (e.dataPoint.x == "Expired Demands") {
                            $scope.loadCategoryChart($scope.DashboardStats.CategoryExpired, "Category wise Expired Demands");
                        }
                        //alert(e.dataSeries.type + ", dataPoint { x:" + e.dataPoint.x + ", y: " + e.dataPoint.y + " }");
                    },

                    toolTipContent: "{label}",
                }
            ]
        });

        pieChart.render();


        NProgress.done();
    }

    $scope.loadCategoryChart = function (chartData, _title) {

        console.log(chartData);
        var jsonArray = new Array();
        $(chartData).each(function (ind, val) {
            jsonArray.push({ label: val.label, y: parseInt(val.y) });
        });

        console.log(jsonArray);

        var categoryColumnChart = new CanvasJS.Chart("categoryColumnChart", {

            title: {
                text: _title
            },
            data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "bar",
                    dataPoints: jsonArray,
                    indexLabel: "{y}",
                    indexLabelPlacement: "outside",
                    indexLabelOrientation: "horizontal",
                }
            ],

            showInLegend: true,
            backgroundColor: "rgba(0,0,0,0)",
            axisY: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,

            },
            axisX: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,

            },
        });

        categoryColumnChart.render();
    }

});

app.controller('DashboardCustomDates', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope.initShoppingCart = function (_ShoppingCart) {
        ShoppingCart.ItemsCount = _ShoppingCart.ItemsCount;
        ShoppingCart.Items = _ShoppingCart.Items;
    }

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();

    $scope.CartItem = {};
    $scope.showLoader = false;
    $scope.ItemCategory = (new URL(location.href)).searchParams.get('Category');

    console.log($scope.ItemCategory);


    $scope.Type = "Select";


    $scope.ProductList = [];
    $scope.showLoader = true;
    NProgress.configure({ parent: '.col-full' });

    $scope.DashboardStats = {};
    $scope.DashboardStats.DropshipmentOrderHistroy = 0;
    $scope.DashboardStats.T2DocsUpload = 0;
    $scope.DashboardStats.PODCopyandCRACUpload = 0;
    $scope.DashboardStats.ExpiredDemands = 0;

    var DateObj = new Date();
    var DateObj2 = new Date();
    DateObj.setDate(DateObj.getDate() - 5365);
    $scope.dateFrom = '01-01-2021';//('0' + (DateObj.getDate() + 0)).slice(-2) + '-' + ('0' + (DateObj.getMonth() + 1)).slice(-2) + '-' + DateObj.getFullYear();
    $scope.dateTill = ('0' + (DateObj2.getDate() + 0)).slice(-2) + '-' + ('0' + (DateObj2.getMonth() + 1)).slice(-2) + '-' + DateObj2.getFullYear();

    $scope.loadDashboardStats = function () {

        NProgress.start();
        debugger;
        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Account/GetDashboardStatsByDates?CustomerCode=' + $scope.CustomerCode +
                '&_FromDate=' + $scope.dateFrom + '&_TillDate=' + $scope.dateTill,
            headers: { 'Content-Type': 'application/json' },
        }).then(function (data, status, header, config) {

            console.log(data.data.Data);

            debugger;
            $scope.DashboardStats = data.data.Data;

            var Processing = '0';
            var InvoicedbyRILAwaitingforScanning = '0';
            var InvoicedbyRILAwaiting = '0';
            var ShipmentInTransit = '0';
            var ShipmentDelivered = '0';
            var PODAvailable = '0';
            var Completed = '0';

            Processing = data.data.Data.Processing;
            InvoicedbyRILAwaitingforScanning = data.data.Data.InvoicedbyRILAwaitingforScanning;
            InvoicedbyRILAwaiting = data.data.Data.InvoicedbyRILAwaitingT2Docs;
            ShipmentInTransit = data.data.Data.ShipmentInTransit;
            ShipmentDelivered = data.data.Data.ShipmentDelivered;
            PODAvailable = data.data.Data.PODAvailable;
            Completed = data.data.Data.Completed;

            NProgress.done();


            google.charts.load('current', { 'packages': ['corechart'] });
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {

                var data = google.visualization.arrayToDataTable([

                    ['Task', 'Hours per Day'],
                    ['Processing', Processing],
                    ['Invoiced by RIL / Awaiting for Scanning', InvoicedbyRILAwaitingforScanning],
                    ['Invoiced by RIL / Awaiting for T2 Docs', InvoicedbyRILAwaiting],
                    ['Shipment In Transit', ShipmentInTransit],
                    ['Shipment Delivered', ShipmentDelivered],
                    ['POD Available', PODAvailable],
                    ['CRAC Uploaded', Completed]
                ]);

                var options = {
                    title: 'Drop Shipment Overall Orders'
                };

                var chart = new google.visualization.PieChart(document.getElementById('pieChart'));

                chart.draw(data, options);
            }



            //var pieChart = new CanvasJS.Chart("pieChart",
            //    {
            //        title: {
            //            text: "Drop Shipment Overall Orders"
            //        },
            //        data: [
            //            {
            //                type: "pie",

            //                dataPoints: [

            //                    //{ x: "Fulfilled Demands", label: "Fulfilled Demands (" + 0 + ")", y: 0 },
            //                    //{ x: "Pending Demands", label: "Pending Demands (" + 100 + ")", y: 200 },
            //                    //{ x: "Expired Demands", label: "Expired Demands (" + 300 + ")", y: 0 },


            //                    { x: "Processing", label: "Processing", y: 10 },
            //                    { x: "Invoiced by RIL / Awaiting for Scanning", label: "Invoiced by RIL / Awaiting for Scanning", y: 0 },
            //                    { x: "Invoiced by RIL / Awaiting for T2 Docs", label: "Invoiced by RIL / Awaiting for T2 Docs", y: 0 },
            //                    { x: "Shipment In-Transit", label: "Shipment In-Transit", y: 35 },
            //                    { x: "Shipment Delivered", label: "Shipment Delivered", y: 76 },
            //                    { x: "POD Available", label: "POD Available", y: 31 },
            //                    { x: "Completed", label: "Completed", y: 0 }



            //                    //{ x: "Processing", label: "Processing", y: Processing },
            //                    //{ x: "Invoiced by RIL / Awaiting for Scanning", label: "Invoiced by RIL / Awaiting for Scanning", y: InvoicedbyRILAwaitingforScanning },
            //                    //{ x: "Invoiced by RIL / Awaiting for T2 Docs", label: "Invoiced by RIL / Awaiting for T2 Docs", y: InvoicedbyRILAwaiting },
            //                    //{ x: "Shipment In-Transit", label: "Shipment In-Transit", y: ShipmentInTransit },
            //                    //{ x: "Shipment Delivered", label: "Shipment Delivered", y: ShipmentDelivered },
            //                    //{ x: "POD Available", label: "POD Available", y: PODAvailable },
            //                    //{ x: "Completed", label: "Completed", y: Completed }

            //                ]

            //            }
            //        ]
            //    });
            //pieChart.render();


            //if ($scope.DashboardStats.ChartDelivered.length > 0) {
            //    $("#chartContainer4").show();
            //    $scope.loadDeliveredChart($scope.DashboardStats.ChartDelivered);
            //} else {
            //    $("#chartContainer4").hide();
            //}


            //if ($scope.DashboardStats.ChartDispatched != null && $scope.DashboardStats.ChartDispatched.length > 0) {
            //    $("#chartContainer3").show();
            //    $scope.loadDispatchedChart($scope.DashboardStats.ChartDispatched);
            //} else {
            //    $("#chartContainer3").hide();
            //}


            //if ($scope.DashboardStats.ChartInvoiced.length > 0) {
            //    $("#chartContainer2").show();
            //    $scope.loadInvoicedChart($scope.DashboardStats.ChartInvoiced);
            //} else {
            //    $("#chartContainer2").hide();
            //}


            //if ($scope.DashboardStats.ChartSOGenerated != null && $scope.DashboardStats.ChartSOGenerated.length > 0) {
            //    $("#chatSOGenerated").show();
            //    $scope.loadSOGeneratedChart($scope.DashboardStats.ChartSOGenerated);
            //} else {
            //    $("#chatSOGenerated").hide();
            //}

            //if ($scope.DashboardStats.T2DocsUpload != null && $scope.DashboardStats.PODCopyandCRACUpload != null) {
            //    $scope.loadPIEChart($scope.DashboardStats.T2DocsUpload, $scope.DashboardStats.PODCopyandCRACUpload, 0);
            //    //$scope.loadPIEChart(1, 7, 1);
            //}
            ////$scope.loadPIEChart(290, 130, 100);

            ////$scope.loadCategoryChart($scope.DashboardStats.CategoryExpired, "Category wise Fulfilled Demands");
            //debugger;
            ////if ($scope.DashboardStats.T2DocsUpload > 0) {
            ////    $scope.loadCategoryChart($scope.DashboardStats.CategoryFulfilled, "Category wise Fulfilled Demands");
            ////}

            //if (($scope.DashboardStats.PODCopyandCRACUpload > 0) || ($scope.DashboardStats.T2DocsUpload > 0)) {
            //    $scope.loadCategoryChart($scope.DashboardStats.CategoryPending, "Category wise Dropshipment Orders");
            //}
            //else {
            //    $scope.loadCategoryChart("", "Category wise Dropshipment Orders");
            //}

        }, function (data, status, header, config) {
            NProgress.done();
        });

    }

    $scope.loadDashboardStats();

    $scope.formatNumber = function (x) {
        if (x != null || x != undefined) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        } else {
            return "0";
        }
    }

    $scope.loadDeliveredChart = function (chartData) {

        var jsonArray = new Array();
        $(chartData).each(function (ind, val) {
            jsonArray.push({ label: val.label, y: parseInt(val.y), color: "white" });
        });

        console.log(jsonArray);

        var chart4 = new CanvasJS.Chart("chartContainer4", {

            title: {
                text: ""
            },
            data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    dataPoints: jsonArray,
                    indexLabel: "{y}",
                    indexLabelFontSize: 14,
                    indexLabelFontColor: "white",
                    indexLabelPlacement: "outside"
                }
            ],
            showInLegend: false,
            backgroundColor: "rgba(0,0,0,0)",
            axisY: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function () {
                    return " ";
                },
                valueFormatString: ""
            },
            axisX: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function (e) {
                    try {
                        return e.label.substring(0, 5);
                    } catch (e) {
                        return "";
                    }
                },
                labelAngle: -45,
                labelFontColor: "white",
                labelFontSize: 10
            },
            toolTip: {
                backgroundColor: "#606060"
            }
        });
        chart4.render();
    }

    $scope.loadDispatchedChart = function (chartData) {

        var jsonArray = new Array();
        $(chartData).each(function (ind, val) {
            jsonArray.push({ label: val.label, y: parseInt(val.y), color: "white" });
        });

        console.log(jsonArray);

        var chart3 = new CanvasJS.Chart("chartContainer3", {

            title: {
                text: ""
            },
            data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "line",
                    dataPoints: jsonArray,
                    indexLabel: "{y}",
                    indexLabelFontSize: 14,
                    indexLabelFontColor: "white",
                    indexLabelPlacement: "outside"
                }
            ],
            showInLegend: false,
            backgroundColor: "rgba(0,0,0,0)",
            axisY: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function () {
                    return " ";
                },
                valueFormatString: ""
            },
            axisX: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function (e) {
                    try {
                        return e.label.substring(0, 5);
                    } catch (e) {
                        return "";
                    }
                },
                labelAngle: -45,
                labelFontColor: "white",
                labelFontSize: 10
            },
            toolTip: {
                backgroundColor: "#F34235"
            }
        });
        chart3.render();
    }

    $scope.loadInvoicedChart = function (chartData) {

        var jsonArray = new Array();
        $(chartData).each(function (ind, val) {
            jsonArray.push({ label: val.label, y: parseInt(val.y), color: "white" });
        });

        console.log(jsonArray);

        var chart2 = new CanvasJS.Chart("chartContainer2", {

            title: {
                text: ""
            },
            data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "line",
                    dataPoints: jsonArray,
                    indexLabel: "{y}",
                    indexLabelFontSize: 14,
                    indexLabelFontColor: "white",
                    indexLabelPlacement: "outside"
                }
            ],
            showInLegend: false,
            backgroundColor: "rgba(0,0,0,0)",
            axisY: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function () {
                    return " ";
                },
                valueFormatString: ""
            },
            axisX: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function (e) {
                    try {
                        return e.label.substring(0, 5);
                    } catch (e) {
                        return "";
                    }
                },
                labelAngle: -45,
                labelFontColor: "white",
                labelFontSize: 10
            },
            toolTip: {
                backgroundColor: "#FE9700",
            },
        });
        chart2.render();
    }

    $scope.loadSOGeneratedChart = function (chartData) {

        //chatSOGenerated

        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ["Element", "Density", { role: "style" }],
                ["Copper", 8.94, "#fff"],
                ["Silver", 10.49, "silver"],
                ["Gold", 19.30, "gold"],
                ["Platinum", 21.45, "color: #e5e4e2"]
            ]);

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                {
                    calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation"
                },
                2]);

            var options = {
                title: "",
                indexLabel: "{y}",
                height: 140,
                bar: { groupWidth: "95%" },
                legend: { position: "none" },
                backgroundColor: '#00BBD3',
            };
            var chart = new google.visualization.ColumnChart(document.getElementById("chatSOGenerated"));
            //chart.draw(view, options);
        }

        var jsonArray = new Array();
        $(chartData).each(function (ind, val) {
            jsonArray.push({ label: val.label, y: parseInt(val.y), color: "white" });
        });

        console.log(jsonArray);

        var chart1 = new CanvasJS.Chart("chatSOGenerated", {

            title: {
                text: ""
            },
            data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    dataPoints: jsonArray,
                    indexLabel: "{y}",
                    indexLabelFontSize: 14,
                    indexLabelFontColor: "white",
                    indexLabelPlacement: "outside"
                }
            ],
            showInLegend: false,
            backgroundColor: "rgba(0,0,0,0)",
            axisY: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function () {
                    return " ";
                },
                valueFormatString: ""
            },
            axisX: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function (e) {
                    try {
                        return e.label.substring(0, 5);
                    } catch (e) {
                        return "";
                    }
                },
                labelAngle: -45,
                labelFontColor: "white",
                labelFontSize: 10
            },
            toolTip: {
                backgroundColor: "#00BBD3",
            },
        });
        chart1.render();
    }

    $scope.loadPIEChart = function (Fulfilled, Pending, Expired) {

        google.charts.load("current", { packages: ["corechart"] });
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                ['PENDING T2 DOCS UPLOAD', Fulfilled],
                ['POD COPY & CRAC UPLOAD', Pending],
                ['Expired', Expired]
            ]);

            var options = {
                title: 'Dropshipment Orders',
                titleTextStyle: {
                    fontSize: '22',
                    fontName: 'Roboto',
                    color: '#3A3A3A'
                },
                is3D: true,
                colors: ['#378D3B', '#0096A6', '#D22E2E'],
                width: 530
            };

            function selectHandler() {
                //alert("sdsdfsd");
                var selectedItem = chart.getSelection()[0];
                if (selectedItem) {
                    //var value = data.getValue(selectedItem.row, selectedItem.column);
                    //alert('The user selected ' + value);
                    alert(selectedItem);
                    console.log(selectedItem);
                }
            }

            var chart = new google.visualization.PieChart(document.getElementById('pieChart'));
            google.visualization.events.addListener(chart, 'select', function (e) {

                //try {
                //    var _SelectedLable = data.getValue(chart.getSelection()[0].row, 0)
                //    console.log(_SelectedLable);
                //    if (_SelectedLable == "Pending") {
                //        $scope.loadCategoryChart($scope.DashboardStats.CategoryPending, "Category wise Pending Demands");
                //    }
                //    else if (_SelectedLable == "Fullfilled") {
                //        $scope.loadCategoryChart($scope.DashboardStats.CategoryFulfilled, "Category wise Fulfilled Demands11");
                //    }
                //    else if (_SelectedLable == "Expired") {
                //        $scope.loadCategoryChart($scope.DashboardStats.CategoryExpired, "Category wise Expired Demands");
                //    }
                //} catch (e) {

                //}


            });
            chart.draw(data, options);


        }

        var _Fulfilled = 0;
        var _Pending = 0;
        var _Expired = 0;

        var pieChart = new CanvasJS.Chart("pieChart", {

            title: {
                text: "All Demands"
            },
            data: [//array of dataSeries
                { //dataSeries object

                    /*** Change type "column" to "bar", "area", "line" or "pie"***/
                    type: "pie",
                    dataPoints: [
                        { x: "Fulfilled Demands", label: "Fulfilled Demands (" + Fulfilled + ")", y: Fulfilled, color: "#D22E2E" },
                        { x: "Pending Demands", label: "Pending Demands (" + Pending + ")", y: Pending, color: "#378D3B" },
                        { x: "Expired Demands", label: "Expired Demands (" + Expired + ")", y: Expired, color: "#0096A6" }
                    ],
                    click: function (e) {
                        if (e.dataPoint.x == "Pending Demands") {
                            $scope.loadCategoryChart($scope.DashboardStats.CategoryPending, "Category wise Pending Demands");
                        }
                        else if (e.dataPoint.x == "Fulfilled Demands") {
                            $scope.loadCategoryChart($scope.DashboardStats.CategoryFulfilled, "Category wise Fulfilled Demands");
                        }
                        else if (e.dataPoint.x == "Expired Demands") {
                            $scope.loadCategoryChart($scope.DashboardStats.CategoryExpired, "Category wise Expired Demands");
                        }
                        //alert(e.dataSeries.type + ", dataPoint { x:" + e.dataPoint.x + ", y: " + e.dataPoint.y + " }");
                    },

                    toolTipContent: "{label}",
                }
            ]
        });

        //pieChart.render();


        NProgress.done();
    }

    $scope.loadCategoryChart = function (chartData, _title) {

        console.log(chartData);
        var jsonArray = new Array();
        $(chartData).each(function (ind, val) {
            jsonArray.push({ label: val.label, y: parseInt(val.y) });
        });

        console.log(jsonArray);

        var categoryColumnChart = new CanvasJS.Chart("categoryColumnChart", {

            title: {
                text: _title
            },
            data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    dataPoints: jsonArray,
                    indexLabel: "{y}",
                    indexLabelPlacement: "outside",
                    indexLabelOrientation: "horizontal",
                }
            ],


        });

        categoryColumnChart.render();
    }

});

app.controller('DashboardCustomDatesMVC', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope.initShoppingCart = function (_ShoppingCart) {
        ShoppingCart.ItemsCount = _ShoppingCart.ItemsCount;
        ShoppingCart.Items = _ShoppingCart.Items;
    }

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();

    $scope.CartItem = {};
    $scope.showLoader = false;
    $scope.ItemCategory = (new URL(location.href)).searchParams.get('Category');

    console.log($scope.ItemCategory);


    $scope.Type = "Select";


    $scope.ProductList = [];
    $scope.showLoader = true;
    NProgress.configure({ parent: '.col-full' });

    $scope.DashboardStats = {};
    $scope.DashboardStats.DropshipmentOrderHistroy = 0;
    $scope.DashboardStats.T2DocsUpload = 0;
    $scope.DashboardStats.PODCopyandCRACUpload = 0;
    $scope.DashboardStats.ExpiredDemands = 0;

    var DateObj = new Date();
    var DateObj2 = new Date();
    DateObj.setDate(DateObj.getDate() - 5365);
    $scope.dateFrom = '01-01-2021';//('0' + (DateObj.getDate() + 0)).slice(-2) + '-' + ('0' + (DateObj.getMonth() + 1)).slice(-2) + '-' + DateObj.getFullYear();
    $scope.dateTill = ('0' + (DateObj2.getDate() + 0)).slice(-2) + '-' + ('0' + (DateObj2.getMonth() + 1)).slice(-2) + '-' + DateObj2.getFullYear();

    $scope.loadDashboardStats = function () {

        NProgress.start();
        debugger;
        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Account/GetDashboardStatsByDatesMVC?CustomerCode=' + $scope.CustomerCode +
                '&_FromDate=' + $scope.dateFrom + '&_TillDate=' + $scope.dateTill,
            headers: { 'Content-Type': 'application/json' },
        }).then(function (data, status, header, config) {

            console.log(data.data.Data);

            debugger;
            $scope.DashboardStats = data.data.Data;

            var Processing = '0';
            var InvoicedbyRILAwaitingforScanning = '0';
            var InvoicedbyRILAwaiting = '0';
            var ShipmentInTransit = '0';
            var ShipmentDelivered = '0';
            var PODAvailable = '0';
            var Completed = '0';
            var InvoiceDispatched = '0';

            Processing = data.data.Data.Processing;
            InvoicedbyRILAwaitingforScanning = data.data.Data.InvoicedbyRILAwaitingforScanning;
            InvoicedbyRILAwaiting = data.data.Data.InvoicedbyRILAwaitingT2Docs;
            ShipmentInTransit = data.data.Data.ShipmentInTransit;
            ShipmentDelivered = data.data.Data.ShipmentDelivered;
            PODAvailable = data.data.Data.PODAvailable;
            Completed = data.data.Data.Completed;
            InvoiceDispatched = data.data.Data.InvoiceunderDispatch

            NProgress.done();


            google.charts.load('current', { 'packages': ['corechart'] });
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {

                var data = google.visualization.arrayToDataTable([

                    ['Task', 'Hours per Day'],
                    ['Processing', Processing],
                    ['Invoiced by RIL / Awaiting for Scanning', InvoicedbyRILAwaitingforScanning],
                    ['Invoiced by RIL / Awaiting for T2 Docs', InvoicedbyRILAwaiting],
                    ['Invoice UnderDispatch', InvoiceDispatched],
                    ['Shipment In Transit', ShipmentInTransit],
                    ['Shipment Delivered', ShipmentDelivered],
                    ['POD Available', PODAvailable],
                    ['CRAC Uploaded', Completed]
                ]);

                var options = {
                    title: 'Drop Shipment Overall Orders'
                };

                var chart = new google.visualization.PieChart(document.getElementById('pieChart'));

                chart.draw(data, options);
            }



            //var pieChart = new CanvasJS.Chart("pieChart",
            //    {
            //        title: {
            //            text: "Drop Shipment Overall Orders"
            //        },
            //        data: [
            //            {
            //                type: "pie",

            //                dataPoints: [

            //                    //{ x: "Fulfilled Demands", label: "Fulfilled Demands (" + 0 + ")", y: 0 },
            //                    //{ x: "Pending Demands", label: "Pending Demands (" + 100 + ")", y: 200 },
            //                    //{ x: "Expired Demands", label: "Expired Demands (" + 300 + ")", y: 0 },


            //                    { x: "Processing", label: "Processing", y: 10 },
            //                    { x: "Invoiced by RIL / Awaiting for Scanning", label: "Invoiced by RIL / Awaiting for Scanning", y: 0 },
            //                    { x: "Invoiced by RIL / Awaiting for T2 Docs", label: "Invoiced by RIL / Awaiting for T2 Docs", y: 0 },
            //                    { x: "Shipment In-Transit", label: "Shipment In-Transit", y: 35 },
            //                    { x: "Shipment Delivered", label: "Shipment Delivered", y: 76 },
            //                    { x: "POD Available", label: "POD Available", y: 31 },
            //                    { x: "Completed", label: "Completed", y: 0 }



            //                    //{ x: "Processing", label: "Processing", y: Processing },
            //                    //{ x: "Invoiced by RIL / Awaiting for Scanning", label: "Invoiced by RIL / Awaiting for Scanning", y: InvoicedbyRILAwaitingforScanning },
            //                    //{ x: "Invoiced by RIL / Awaiting for T2 Docs", label: "Invoiced by RIL / Awaiting for T2 Docs", y: InvoicedbyRILAwaiting },
            //                    //{ x: "Shipment In-Transit", label: "Shipment In-Transit", y: ShipmentInTransit },
            //                    //{ x: "Shipment Delivered", label: "Shipment Delivered", y: ShipmentDelivered },
            //                    //{ x: "POD Available", label: "POD Available", y: PODAvailable },
            //                    //{ x: "Completed", label: "Completed", y: Completed }

            //                ]

            //            }
            //        ]
            //    });
            //pieChart.render();


            //if ($scope.DashboardStats.ChartDelivered.length > 0) {
            //    $("#chartContainer4").show();
            //    $scope.loadDeliveredChart($scope.DashboardStats.ChartDelivered);
            //} else {
            //    $("#chartContainer4").hide();
            //}


            //if ($scope.DashboardStats.ChartDispatched != null && $scope.DashboardStats.ChartDispatched.length > 0) {
            //    $("#chartContainer3").show();
            //    $scope.loadDispatchedChart($scope.DashboardStats.ChartDispatched);
            //} else {
            //    $("#chartContainer3").hide();
            //}


            //if ($scope.DashboardStats.ChartInvoiced.length > 0) {
            //    $("#chartContainer2").show();
            //    $scope.loadInvoicedChart($scope.DashboardStats.ChartInvoiced);
            //} else {
            //    $("#chartContainer2").hide();
            //}


            //if ($scope.DashboardStats.ChartSOGenerated != null && $scope.DashboardStats.ChartSOGenerated.length > 0) {
            //    $("#chatSOGenerated").show();
            //    $scope.loadSOGeneratedChart($scope.DashboardStats.ChartSOGenerated);
            //} else {
            //    $("#chatSOGenerated").hide();
            //}

            //if ($scope.DashboardStats.T2DocsUpload != null && $scope.DashboardStats.PODCopyandCRACUpload != null) {
            //    $scope.loadPIEChart($scope.DashboardStats.T2DocsUpload, $scope.DashboardStats.PODCopyandCRACUpload, 0);
            //    //$scope.loadPIEChart(1, 7, 1);
            //}
            ////$scope.loadPIEChart(290, 130, 100);

            ////$scope.loadCategoryChart($scope.DashboardStats.CategoryExpired, "Category wise Fulfilled Demands");
            //debugger;
            ////if ($scope.DashboardStats.T2DocsUpload > 0) {
            ////    $scope.loadCategoryChart($scope.DashboardStats.CategoryFulfilled, "Category wise Fulfilled Demands");
            ////}

            //if (($scope.DashboardStats.PODCopyandCRACUpload > 0) || ($scope.DashboardStats.T2DocsUpload > 0)) {
            //    $scope.loadCategoryChart($scope.DashboardStats.CategoryPending, "Category wise Dropshipment Orders");
            //}
            //else {
            //    $scope.loadCategoryChart("", "Category wise Dropshipment Orders");
            //}

        }, function (data, status, header, config) {
            NProgress.done();
        });

    }

    $scope.loadDashboardStats();

    $scope.formatNumber = function (x) {
        if (x != null || x != undefined) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        } else {
            return "0";
        }
    }

    $scope.loadDeliveredChart = function (chartData) {

        var jsonArray = new Array();
        $(chartData).each(function (ind, val) {
            jsonArray.push({ label: val.label, y: parseInt(val.y), color: "white" });
        });

        console.log(jsonArray);

        var chart4 = new CanvasJS.Chart("chartContainer4", {

            title: {
                text: ""
            },
            data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    dataPoints: jsonArray,
                    indexLabel: "{y}",
                    indexLabelFontSize: 14,
                    indexLabelFontColor: "white",
                    indexLabelPlacement: "outside"
                }
            ],
            showInLegend: false,
            backgroundColor: "rgba(0,0,0,0)",
            axisY: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function () {
                    return " ";
                },
                valueFormatString: ""
            },
            axisX: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function (e) {
                    try {
                        return e.label.substring(0, 5);
                    } catch (e) {
                        return "";
                    }
                },
                labelAngle: -45,
                labelFontColor: "white",
                labelFontSize: 10
            },
            toolTip: {
                backgroundColor: "#606060"
            }
        });
        chart4.render();
    }

    $scope.loadDispatchedChart = function (chartData) {

        var jsonArray = new Array();
        $(chartData).each(function (ind, val) {
            jsonArray.push({ label: val.label, y: parseInt(val.y), color: "white" });
        });

        console.log(jsonArray);

        var chart3 = new CanvasJS.Chart("chartContainer3", {

            title: {
                text: ""
            },
            data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "line",
                    dataPoints: jsonArray,
                    indexLabel: "{y}",
                    indexLabelFontSize: 14,
                    indexLabelFontColor: "white",
                    indexLabelPlacement: "outside"
                }
            ],
            showInLegend: false,
            backgroundColor: "rgba(0,0,0,0)",
            axisY: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function () {
                    return " ";
                },
                valueFormatString: ""
            },
            axisX: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function (e) {
                    try {
                        return e.label.substring(0, 5);
                    } catch (e) {
                        return "";
                    }
                },
                labelAngle: -45,
                labelFontColor: "white",
                labelFontSize: 10
            },
            toolTip: {
                backgroundColor: "#F34235"
            }
        });
        chart3.render();
    }

    $scope.loadInvoicedChart = function (chartData) {

        var jsonArray = new Array();
        $(chartData).each(function (ind, val) {
            jsonArray.push({ label: val.label, y: parseInt(val.y), color: "white" });
        });

        console.log(jsonArray);

        var chart2 = new CanvasJS.Chart("chartContainer2", {

            title: {
                text: ""
            },
            data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "line",
                    dataPoints: jsonArray,
                    indexLabel: "{y}",
                    indexLabelFontSize: 14,
                    indexLabelFontColor: "white",
                    indexLabelPlacement: "outside"
                }
            ],
            showInLegend: false,
            backgroundColor: "rgba(0,0,0,0)",
            axisY: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function () {
                    return " ";
                },
                valueFormatString: ""
            },
            axisX: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function (e) {
                    try {
                        return e.label.substring(0, 5);
                    } catch (e) {
                        return "";
                    }
                },
                labelAngle: -45,
                labelFontColor: "white",
                labelFontSize: 10
            },
            toolTip: {
                backgroundColor: "#FE9700",
            },
        });
        chart2.render();
    }

    $scope.loadSOGeneratedChart = function (chartData) {

        //chatSOGenerated

        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ["Element", "Density", { role: "style" }],
                ["Copper", 8.94, "#fff"],
                ["Silver", 10.49, "silver"],
                ["Gold", 19.30, "gold"],
                ["Platinum", 21.45, "color: #e5e4e2"]
            ]);

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                {
                    calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation"
                },
                2]);

            var options = {
                title: "",
                indexLabel: "{y}",
                height: 140,
                bar: { groupWidth: "95%" },
                legend: { position: "none" },
                backgroundColor: '#00BBD3',
            };
            var chart = new google.visualization.ColumnChart(document.getElementById("chatSOGenerated"));
            //chart.draw(view, options);
        }

        var jsonArray = new Array();
        $(chartData).each(function (ind, val) {
            jsonArray.push({ label: val.label, y: parseInt(val.y), color: "white" });
        });

        console.log(jsonArray);

        var chart1 = new CanvasJS.Chart("chatSOGenerated", {

            title: {
                text: ""
            },
            data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    dataPoints: jsonArray,
                    indexLabel: "{y}",
                    indexLabelFontSize: 14,
                    indexLabelFontColor: "white",
                    indexLabelPlacement: "outside"
                }
            ],
            showInLegend: false,
            backgroundColor: "rgba(0,0,0,0)",
            axisY: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function () {
                    return " ";
                },
                valueFormatString: ""
            },
            axisX: {
                gridThickness: 0,
                tickLength: 0,
                lineThickness: 0,
                labelFormatter: function (e) {
                    try {
                        return e.label.substring(0, 5);
                    } catch (e) {
                        return "";
                    }
                },
                labelAngle: -45,
                labelFontColor: "white",
                labelFontSize: 10
            },
            toolTip: {
                backgroundColor: "#00BBD3",
            },
        });
        chart1.render();
    }

    $scope.loadPIEChart = function (Fulfilled, Pending, Expired) {

        google.charts.load("current", { packages: ["corechart"] });
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                ['PENDING T2 DOCS UPLOAD', Fulfilled],
                ['POD COPY & CRAC UPLOAD', Pending],
                ['Expired', Expired]
            ]);

            var options = {
                title: 'Dropshipment Orders',
                titleTextStyle: {
                    fontSize: '22',
                    fontName: 'Roboto',
                    color: '#3A3A3A'
                },
                is3D: true,
                colors: ['#378D3B', '#0096A6', '#D22E2E'],
                width: 530
            };

            function selectHandler() {
                //alert("sdsdfsd");
                var selectedItem = chart.getSelection()[0];
                if (selectedItem) {
                    //var value = data.getValue(selectedItem.row, selectedItem.column);
                    //alert('The user selected ' + value);
                    alert(selectedItem);
                    console.log(selectedItem);
                }
            }

            var chart = new google.visualization.PieChart(document.getElementById('pieChart'));
            google.visualization.events.addListener(chart, 'select', function (e) {

                //try {
                //    var _SelectedLable = data.getValue(chart.getSelection()[0].row, 0)
                //    console.log(_SelectedLable);
                //    if (_SelectedLable == "Pending") {
                //        $scope.loadCategoryChart($scope.DashboardStats.CategoryPending, "Category wise Pending Demands");
                //    }
                //    else if (_SelectedLable == "Fullfilled") {
                //        $scope.loadCategoryChart($scope.DashboardStats.CategoryFulfilled, "Category wise Fulfilled Demands11");
                //    }
                //    else if (_SelectedLable == "Expired") {
                //        $scope.loadCategoryChart($scope.DashboardStats.CategoryExpired, "Category wise Expired Demands");
                //    }
                //} catch (e) {

                //}


            });
            chart.draw(data, options);


        }

        var _Fulfilled = 0;
        var _Pending = 0;
        var _Expired = 0;

        var pieChart = new CanvasJS.Chart("pieChart", {

            title: {
                text: "All Demands"
            },
            data: [//array of dataSeries
                { //dataSeries object

                    /*** Change type "column" to "bar", "area", "line" or "pie"***/
                    type: "pie",
                    dataPoints: [
                        { x: "Fulfilled Demands", label: "Fulfilled Demands (" + Fulfilled + ")", y: Fulfilled, color: "#D22E2E" },
                        { x: "Pending Demands", label: "Pending Demands (" + Pending + ")", y: Pending, color: "#378D3B" },
                        { x: "Expired Demands", label: "Expired Demands (" + Expired + ")", y: Expired, color: "#0096A6" }
                    ],
                    click: function (e) {
                        if (e.dataPoint.x == "Pending Demands") {
                            $scope.loadCategoryChart($scope.DashboardStats.CategoryPending, "Category wise Pending Demands");
                        }
                        else if (e.dataPoint.x == "Fulfilled Demands") {
                            $scope.loadCategoryChart($scope.DashboardStats.CategoryFulfilled, "Category wise Fulfilled Demands");
                        }
                        else if (e.dataPoint.x == "Expired Demands") {
                            $scope.loadCategoryChart($scope.DashboardStats.CategoryExpired, "Category wise Expired Demands");
                        }
                        //alert(e.dataSeries.type + ", dataPoint { x:" + e.dataPoint.x + ", y: " + e.dataPoint.y + " }");
                    },

                    toolTipContent: "{label}",
                }
            ]
        });

        //pieChart.render();


        NProgress.done();
    }

    $scope.loadCategoryChart = function (chartData, _title) {

        console.log(chartData);
        var jsonArray = new Array();
        $(chartData).each(function (ind, val) {
            jsonArray.push({ label: val.label, y: parseInt(val.y) });
        });

        console.log(jsonArray);

        var categoryColumnChart = new CanvasJS.Chart("categoryColumnChart", {

            title: {
                text: _title
            },
            data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    dataPoints: jsonArray,
                    indexLabel: "{y}",
                    indexLabelPlacement: "outside",
                    indexLabelOrientation: "horizontal",
                }
            ],


        });

        categoryColumnChart.render();
    }

});

app.controller('AdminDashboardCustomDates', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope.initShoppingCart = function (_ShoppingCart) {
        ShoppingCart.ItemsCount = _ShoppingCart.ItemsCount;
        ShoppingCart.Items = _ShoppingCart.Items;
    }

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();

    $scope.CartItem = {};
    $scope.showLoader = false;
    $scope.ItemCategory = (new URL(location.href)).searchParams.get('Category');

    console.log($scope.ItemCategory);

    $scope.ProductList = [];
    $scope.showLoader = true;
    NProgress.configure({ parent: '.col-full' });

    $scope.DashboardStats = {};
    $scope.DashboardStats.DropshipmentOrderHistroy = 0;
    $scope.DashboardStats.T2DocsUpload = 0;
    $scope.DashboardStats.PODCopyandCRACUpload = 0;
    $scope.DashboardStats.ExpiredDemands = 0;

    var DateObj = new Date();
    var DateObj2 = new Date();
    DateObj.setDate(DateObj.getDate() - 7);
    $scope.dateFrom = ('0' + (DateObj.getDate() + 0)).slice(-2) + '-' + ('0' + (DateObj.getMonth() + 1)).slice(-2) + '-' + DateObj.getFullYear();
    $scope.dateTill = ('0' + (DateObj2.getDate() + 0)).slice(-2) + '-' + ('0' + (DateObj2.getMonth() + 1)).slice(-2) + '-' + DateObj2.getFullYear();

    $scope.Request = {};
    $scope.Request.Branch = "All";
    $scope.Request.Partner = "All";

    $scope.loadDashboardStats = function () {

        NProgress.start();


        //$scope.Partner = $scope.Partner == undefined || $scope.Partner == null ? "All" : $scope.Partner;
        //$scope.Branch =$scope.Request.Branch;

        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Account/GetAdminDashboardStatsByDates?CustomerCode=' + $scope.CustomerCode +
                '&_FromDate=' + $scope.dateFrom + '&_TillDate=' + $scope.dateTill + "&Partner=" + $scope.Request.Partner +
                '&Branch=' + $scope.Request.Branch,
            headers: { 'Content-Type': 'application/json' },
        }).then(function (data, status, header, config) {

            console.log(data.data.Data);

            $scope.DashboardStats = data.data.Data;
            $scope.PartnerList = data.data.Data.Partners;
            $scope.BranchList = data.data.Data.Branches;
            $scope.Request.Partner = data.data.Data.Partner;
            $scope.Request.Branch = data.data.Data.Branch;

            NProgress.done();

            $scope.chartDataDelivered = [];
            if ($scope.DashboardStats.ChartDelivered.length > 0) {
                $("#chartContainer4").show();
                $.each($scope.DashboardStats.ChartDelivered, function (index, val) {
                    $scope.chartDataDelivered.push([val.label, parseInt(val.y), 'color:' + val.color]);
                });
                $scope.loadDeliveredChart($scope.chartDataDelivered);
            } else {
                $("#chartContainer4").hide();
            }

            $scope.chartDataDispatched = [];
            if ($scope.DashboardStats.ChartDispatched != null && $scope.DashboardStats.ChartDispatched.length > 0) {
                $("#chartDispatched").show();
                $.each($scope.DashboardStats.ChartDispatched, function (index, val) {
                    $scope.chartDataDispatched.push([val.label, parseInt(val.y), 'color:' + val.color]);
                });
                $scope.loadDispatchedChart($scope.chartDataDispatched);
            } else {
                $("#chartContainer3").hide();
            }

            $scope.chartDataInvoiced = [];
            if ($scope.DashboardStats.ChartInvoiced.length > 0) {
                $("#chartInvoiced").show();
                $.each($scope.DashboardStats.ChartInvoiced, function (index, val) {
                    $scope.chartDataInvoiced.push([val.label, parseInt(val.y), 'color:' + val.color]);
                });
                $scope.loadInvoicedChart($scope.chartDataInvoiced);
            } else {
                $("#chartContainer2").hide();
            }

            chartDataSOGenerated = [];


            if ($scope.DashboardStats.ChartSOGenerated != null && $scope.DashboardStats.ChartSOGenerated.length > 0) {
                $("#chatSOGenerated").show();

                $.each($scope.DashboardStats.ChartSOGenerated, function (index, val) {
                    chartDataSOGenerated.push([val.label, parseInt(val.y), 'color:' + val.color]);
                });
                $scope.loadSOGeneratedChart(chartDataSOGenerated);

            } else {
                $("#chatSOGenerated").hide();
            }

            if ($scope.DashboardStats.T2DocsUpload != null && $scope.DashboardStats.PODCopyandCRACUpload != null &&
                $scope.DashboardStats.ExpiredDemands != null) {
                $scope.loadPIEChart($scope.DashboardStats.T2DocsUpload, $scope.DashboardStats.PODCopyandCRACUpload, $scope.DashboardStats.ExpiredDemands);
            }


            branchWiseTotalDemands = [];
            $.each($scope.DashboardStats.ChartBranchPie, function (index, val) {
                branchWiseTotalDemands.push([val.label, parseInt(val.x), 'color:' + val.color]);
            });
            $scope.loadBranchWiseChart(branchWiseTotalDemands);


            customerWiseTotalDemands = [];
            $.each($scope.DashboardStats.ChartCustomerWise, function (index, val) {
                customerWiseTotalDemands.push([val.label, parseInt(val.x), 'color:' + val.color]);
            });
            $scope.loadCustomerWiseChart(customerWiseTotalDemands);

            $scope.fullfilledCategory = [];
            $scope.pendingCategory = [];
            $scope.expiredCategory = [];

            $.each($scope.DashboardStats.CategoryFulfilled, function (index, val) {
                $scope.fullfilledCategory.push([val.label, parseInt(val.y), 'color:' + val.color]);
            });

            $.each($scope.DashboardStats.CategoryPending, function (index, val) {
                $scope.pendingCategory.push([val.label, parseInt(val.y), 'color:' + val.color]);
            });

            $.each($scope.DashboardStats.CategoryExpired, function (index, val) {
                $scope.expiredCategory.push([val.label, parseInt(val.y), 'color:' + val.color]);
            });

            if ($scope.DashboardStats.T2DocsUpload > 0) {
                $scope.loadCategoryChart($scope.fullfilledCategory, "Category wise Fulfilled Demands");
            } else if ($scope.DashboardStats.PODCopyandCRACUpload > 0) {
                $scope.loadCategoryChart($scope.pendingCategory, "Category wise Pending Demands");
            } else {
                $scope.loadCategoryChart($scope.expiredCategory, "Category wise Expired Demands");
            }

        }, function (data, status, header, config) {
            NProgress.done();
        });

    }

    $scope.loadDashboardStats();

    $scope.formatNumber = function (x) {
        if (x != null || x != undefined) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        } else {
            return "0";
        }
    }

    $scope.loadDeliveredChart = function (array) {

        google.charts.load("current", { packages: ["corechart"] });
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Branch');
            data.addColumn('number', 'Quantity');
            data.addColumn({ type: 'string', role: 'style' });
            data.addRows(array);
            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1, { calc: "stringify", sourceColumn: 1, type: "string", role: "annotation" }, 2]);
            var options = {
                title: '',
                titleTextStyle: {
                    fontSize: '22',
                    fontName: 'Roboto',
                    color: '#3A3A3A'
                },
                width: '100%',
                legend: { position: 'none' },
                backgroundColor: '#5F5F5F',
                hAxis: {
                    textStyle: { color: '#FFF' }
                },
                vAxis: {
                    textStyle: { color: '#FFF' }
                },
                theme: 'material',
                chartArea: { left: 70, top: 10, width: '65%' }
            };

            var chart = new google.visualization.BarChart(document.getElementById('chartDelivered'));
            google.visualization.events.addListener(chart, 'select', function (e) {
                try {
                    var _SelectedLable = data.getValue(chart.getSelection()[0].row, 0)
                    //alert(_SelectedLable);                   
                } catch (e) {

                }
            });
            chart.draw(view, options);
        }
    }

    $scope.loadDispatchedChart = function (array) {

        google.charts.load("current", { packages: ["corechart"] });
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Branch');
            data.addColumn('number', 'Quantity');
            data.addColumn({ type: 'string', role: 'style' });
            data.addRows(array);
            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1, { calc: "stringify", sourceColumn: 1, type: "string", role: "annotation" }, 2]);
            var options = {
                title: '',
                titleTextStyle: {
                    fontSize: '22',
                    fontName: 'Roboto',
                    color: '#3A3A3A'
                },
                width: '95%',
                legend: { position: 'none' },
                backgroundColor: '#F14134',
                hAxis: {
                    textStyle: { color: '#FFF' }
                },
                vAxis: {
                    textStyle: { color: '#FFF' }
                },
                theme: 'material',
                chartArea: { left: 70, top: 10, width: '54%' }
            };

            var chart = new google.visualization.BarChart(document.getElementById('chartDispatched'));
            google.visualization.events.addListener(chart, 'select', function (e) {
                try {
                    var _SelectedLable = data.getValue(chart.getSelection()[0].row, 0)
                    //alert(_SelectedLable);                   
                } catch (e) {

                }
            });
            chart.draw(view, options);
        }
    }

    $scope.loadInvoicedChart = function (array) {

        google.charts.load("current", { packages: ["corechart"] });
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Branch');
            data.addColumn('number', 'Quantity');
            data.addColumn({ type: 'string', role: 'style' });
            data.addRows(array);
            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1, { calc: "stringify", sourceColumn: 1, type: "string", role: "annotation" }, 2]);
            var options = {
                title: '',
                titleTextStyle: {
                    fontSize: '22',
                    fontName: 'Roboto',
                    color: '#3A3A3A'
                },
                width: '100%',
                legend: { position: 'none' },
                backgroundColor: '#FD9700',
                hAxis: {
                    textStyle: { color: '#333' }
                },
                vAxis: {
                    textStyle: { color: '#333' }
                },
                theme: 'material',
                chartArea: { left: 70, top: 10, width: '65%' }
            };

            var chart = new google.visualization.BarChart(document.getElementById('chartInvoiced'));
            google.visualization.events.addListener(chart, 'select', function (e) {
                try {
                    var _SelectedLable = data.getValue(chart.getSelection()[0].row, 0)
                    //alert(_SelectedLable);                   
                } catch (e) {

                }
            });
            chart.draw(view, options);
        }

    }

    $scope.loadSOGeneratedChart = function (array) {

        google.charts.load("current", { packages: ["corechart"] });
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Branch');
            data.addColumn('number', 'Quantity');
            data.addColumn({ type: 'string', role: 'style' });
            data.addRows(array);
            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1, { calc: "stringify", sourceColumn: 1, type: "string", role: "annotation" }, 2]);
            var options = {
                title: '',
                titleTextStyle: {
                    fontSize: '22',
                    fontName: 'Roboto',
                    color: '#3A3A3A'
                },
                width: '100%',
                legend: { position: 'none' },
                backgroundColor: '#45DBC9',
                hAxis: {
                    textStyle: { color: '#333' }
                },
                vAxis: {
                    textStyle: { color: '#333' }
                },
                theme: 'material',
                chartArea: { left: 70, top: 10, width: '65%' }
            };

            var chart = new google.visualization.BarChart(document.getElementById('chatSOGenerated'));
            google.visualization.events.addListener(chart, 'select', function (e) {
                try {
                    var _SelectedLable = data.getValue(chart.getSelection()[0].row, 0)
                    //alert(_SelectedLable);                   
                } catch (e) {

                }
            });
            chart.draw(view, options);
        }

    }

    $scope.loadPIEChart = function (Fulfilled, Pending, Expired) {

        google.charts.load("current", { packages: ["corechart"] });
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                ['Fullfilled', Fulfilled],
                ['Pending', Pending],
                ['Expired', Expired]
            ]);

            var options = {
                title: 'All Demands',
                titleTextStyle: {
                    fontSize: '22',
                    fontName: 'Roboto',
                    color: '#3A3A3A'
                },
                is3D: true,
                colors: ['#378D3B', '#0096A6', '#D22E2E'],
                width: 530
            };

            function selectHandler() {
                //alert("sdsdfsd");
                var selectedItem = chart.getSelection()[0];
                if (selectedItem) {
                    //var value = data.getValue(selectedItem.row, selectedItem.column);
                    //alert('The user selected ' + value);
                    alert(selectedItem);
                    console.log(selectedItem);
                }
            }

            var chart = new google.visualization.PieChart(document.getElementById('pieChart'));
            google.visualization.events.addListener(chart, 'select', function (e) {

                try {
                    var _SelectedLable = data.getValue(chart.getSelection()[0].row, 0)
                    console.log(_SelectedLable);
                    if (_SelectedLable == "Pending") {
                        $scope.loadCategoryChart($scope.pendingCategory, "Category wise Pending Demands");
                    }
                    else if (_SelectedLable == "Fullfilled") {
                        $scope.loadCategoryChart($scope.fullfilledCategory, "Category wise Fulfilled Demands");
                    }
                    else if (_SelectedLable == "Expired") {
                        $scope.loadCategoryChart($scope.expiredCategory, "Category wise Expired Demands");
                    }
                } catch (e) {

                }


            });
            chart.draw(data, options);


        }

        var _Fulfilled = 0;
        var _Pending = 0;
        var _Expired = 0;




        NProgress.done();
    }

    $scope.loadBranchWiseChart = function (array) {

        google.charts.load("current", { packages: ["corechart"] });
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Branch');
            data.addColumn('number', 'Quantity');
            data.addColumn({ type: 'string', role: 'style' });
            data.addRows(array);
            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                {
                    calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation"
                },
                2]);
            var options = {
                title: 'All Demands by Branch',
                titleTextStyle: {
                    fontSize: '22',
                    fontName: 'Roboto',
                    color: '#3A3A3A'
                },
                width: 530,
                pieHole: 0.2,
                legend: { position: 'none' },
                backgroundColor: 'rgba(0,0,0,0)',
                theme: 'material'
            };

            function selectHandler() {
                //alert("sdsdfsd");
                var selectedItem = chart.getSelection()[0];
                if (selectedItem) {
                    //var value = data.getValue(selectedItem.row, selectedItem.column);
                    //alert('The user selected ' + value);
                    alert(selectedItem);
                    console.log(selectedItem);
                }
            }

            var chart = new google.visualization.BarChart(document.getElementById('branchWiseChart'));
            google.visualization.events.addListener(chart, 'select', function (e) {

                try {
                    var _SelectedLable = data.getValue(chart.getSelection()[0].row, 0)
                    alert(_SelectedLable);
                    console.log(_SelectedLable);
                    if (_SelectedLable == "Pending") {
                        $scope.loadCategoryChart($scope.DashboardStats.CategoryPending, "Category wise Pending Demands");
                    }
                    else if (_SelectedLable == "Fullfilled") {
                        $scope.loadCategoryChart($scope.DashboardStats.CategoryFulfilled, "Category wise Fulfilled Demands");
                    }
                    else if (_SelectedLable == "Expired") {
                        $scope.loadCategoryChart($scope.DashboardStats.CategoryExpired, "Category wise Expired Demands");
                    }
                } catch (e) {

                }


            });
            chart.draw(view, options);
        }
    }

    $scope.loadCustomerWiseChart = function (array) {

        google.charts.load("current", { packages: ["corechart"] });
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Customer');
            data.addColumn('number', 'Quantity');
            data.addColumn({ type: 'string', role: 'style' });
            data.addRows(array);

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                {
                    calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation"
                },
                2]);
            var options = {
                title: 'All Demands by Customer',
                titleTextStyle: {
                    fontSize: '22',
                    fontName: 'Roboto',
                    color: '#3A3A3A'
                },
                width: 530,
                pieHole: 0.2,
                legend: { position: 'none' }
            };
            var chart = new google.visualization.ColumnChart(document.getElementById('divCustomerWiseChart'));
            google.visualization.events.addListener(chart, 'select', function (e) {

                try {
                    var _SelectedLable = data.getValue(chart.getSelection()[0].row, 0)
                    alert(_SelectedLable);
                    console.log(_SelectedLable);
                    if (_SelectedLable == "Pending") {

                    }
                    else if (_SelectedLable == "Fullfilled") {

                    }
                    else if (_SelectedLable == "Expired") {

                    }
                } catch (e) {

                }
            });
            chart.draw(view, options);
        }
    }

    $scope.loadCategoryChart = function (array, _title) {

        google.charts.load("current", { packages: ["corechart"] });
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Category');
            data.addColumn('number', 'Quantity');
            data.addColumn({ type: 'string', role: 'style' });
            data.addRows(array);

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                {
                    calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation"
                },
                2]);

            var options = {
                title: _title,
                titleTextStyle: {
                    fontSize: '22',
                    fontName: 'Roboto',
                    color: '#3A3A3A'
                },
                width: 530,
                pieHole: 0.2,
                legend: { position: 'none' },
                chartArea: { left: 140, top: 10, width: '100%' },
                theme: 'material'
            };

            function selectHandler() {
                //alert("sdsdfsd");
                var selectedItem = chart.getSelection()[0];
                if (selectedItem) {
                    //var value = data.getValue(selectedItem.row, selectedItem.column);
                    //alert('The user selected ' + value);
                    alert(selectedItem);
                    console.log(selectedItem);
                }
            }

            var chart = new google.visualization.BarChart(document.getElementById('categoryWiseBarChart'));
            chart.draw(view, options);
        }


    }

});

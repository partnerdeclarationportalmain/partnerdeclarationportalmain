﻿var app = angular.module("Redington", []);


app.controller('Login', function ($scope, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');

    $scope.shoLoginLoader = false;

    //$scope.Name = "Admin";


    $scope.validateAndLogin = function () {
        debugger;
        NProgress.start();
        $scope.shoLoginLoader = true;

        $scope.request = {};
        $scope.request.Username = $scope.Username;

        //var hashObj = new jsSHA("SHA-256", "TEXT", { numRounds: 1 });
        //hashObj.update($scope.Password);
        //var hash = hashObj.getHash("HEX");

        //$scope.request.Password = hash;
        $scope.request.Password = $scope.Password;

        $scope.ErrorMessage = "";

        $http({
            method: 'POST',
            url: $scope._baseUrl + 'api/Account/SendOTP?arr=' + $scope.Username,
            headers: { 'Content-Type': 'application/json' },
            data: $scope.request,
        }).then(function (data, status, header, config) {

            console.log(data.data.Data)
            if (data.data.Status == "Success") {

                $("#divSEND").hide();
                $("#divOTP").show();
                $('#Email').prop('readonly', true);
                //alert("We sent verification code to your email.")
                alertNew("We sent verification code to your email.");
                NProgress.done();
                $scope.shoLoginLoader = false;

            }
            else {
                $scope.ErrorMessage = data.data.ErrorMessage;
                NProgress.done();
                $scope.shoLoginLoader = false;
            }

        }, function (data, status, header, config) {
            $scope.shoLoginLoader = false;
            NProgress.done();
        });
    }

    $scope.ValidateOTP = function () {
        //debugger;
        NProgress.start();
        $scope.shoLoginLoader = true;

        $scope.request = {};
        $scope.request.Username = $scope.Username;

        //var hashObj = new jsSHA("SHA-256", "TEXT", { numRounds: 1 });
        //hashObj.update($scope.Password);
        //var hash = hashObj.getHash("HEX");

        //$scope.request.Password = hash;
        $scope.request.Password = $scope.Password;

        $scope.ErrorMessage = "";

        $http({
            method: 'POST',
            url: $scope._baseUrl + 'api/Account/ValidateOTP?email=' + $scope.Username + '&OTP=' + $scope.Password,
            headers: { 'Content-Type': 'application/json' },
            data: $scope.request,
        }).then(function (data, status, header, config) {

            console.log(data);
            if (data.data.Status == "success") {

                window.location = $scope._baseUrl + "Order/Profile";


               
                NProgress.done();
                $scope.shoLoginLoader = false;

            }
            else {
                $scope.ErrorMessage = data.data.Status;

                NProgress.done();
                $scope.shoLoginLoader = false;
                alertNew(data.data.Status);
                alert(data.data.Status);
            }

        }, function (data, status, header, config) {
            $scope.shoLoginLoader = false;
            NProgress.done();
        });
    }

    $scope.sendOTP = function () {

        NProgress.start();
        $scope.shoLoginLoader = true;

        $http({
            method: 'POST',
            url: $scope._baseUrl + 'api/Account/SendOTP',
            headers: { 'Content-Type': 'application/json' },
            data: $scope.request,
        }).then(function (data, status, header, config) {

            if (data.data.Status == "Success") {
                $("#divSEND").hide();
                $("#divOTP").show();
                $('#Email').prop('readonly', true);
                //alert("We sent verification code to your email.")
                alertNew("We sent verification code to your email.");
                NProgress.done();
                $scope.shoLoginLoader = false;
            }
            else {
                $scope.ErrorResponse = data.data.ErrorMessage;
                $scope.SuccessResponse = "";
                NProgress.done();
                $scope.shoLoginLoader = false;
            }

        }, function (data, status, header, config) {
            $scope.shoLoginLoader = false;
            NProgress.done();
            $scope.ErrorResponse = "There was an unexpected error, please try after a while";
        });

    };

    function alertNew(arr) {
        $("#textAlert").text(arr);
        $("#divAlert").fadeIn();
        setTimeout(function () {
            $("#divAlert").fadeOut();
        }, 7000);
    }
    function enable() {
        $("#divSEND").show();
        $("#divOTP").hide();
        $('#Email').prop('readonly', false);
    }
    function getcheckemail(d, sts) {
        if (sts != false) {
            var emailExp = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i; // to validate email id
            if (d.val().match(emailExp)) {
                d.className = "textBox_bor";
                sts = true;
            }
            else {
                d.focus();
                d.className = "textBox_bor_red";
                sts = false;
            }
            return sts;
        }
        else {
            return sts;
        }
    }

    $scope.SuccessResponse = "";
    $scope.ErrorResponse = "";

    $scope.forgotPassword = function () {

        NProgress.start();
        $scope.shoLoginLoader = true;

        $scope.request = {};
        $scope.request.UserID = $scope.Username.toUpperCase();

        $scope.ErrorMessage = "";

        $http({
            method: 'POST',
            url: $scope._baseUrl + 'api/Partner/ForgotPassword',
            headers: { 'Content-Type': 'application/json' },
            data: $scope.request,
        }).then(function (data, status, header, config) {
            console.log(data.data.Data);
            if (data.data.Status == "Success") {
                $scope.SuccessResponse = "Success!, password has been sent to your registered Email id and Mobile Number, please check your inbox.";
                $scope.ErrorResponse = "";
                NProgress.done();
                $scope.shoLoginLoader = false;
            }
            else {
                $scope.ErrorResponse = data.data.ErrorMessage;
                $scope.SuccessResponse = "";
                NProgress.done();
                $scope.shoLoginLoader = false;
            }

        }, function (data, status, header, config) {
            $scope.shoLoginLoader = false;
            NProgress.done();
            $scope.ErrorResponse = "There was an unexpected error, please try after a while";
        });



    };

});

app.controller('AppleQuotation', function ($scope, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');

    $scope.shoLoginLoader = false;



    $scope.validateAndLogin = function () {

        NProgress.start();
        $scope.shoLoginLoader = true;

        $scope.request = {};
        $scope.request.Username = $scope.Username;

        var hashObj = new jsSHA("SHA-256", "TEXT", { numRounds: 1 });
        hashObj.update($scope.Password);
        var hash = hashObj.getHash("HEX");

        //$scope.request.Password = hash;
        $scope.request.Password = $scope.Password;

        //$scope.Password = hash;
        $scope.ErrorMessage = "";

        $http({
            method: 'POST',
            url: $scope._baseUrl + 'api/Account/Validate',
            headers: { 'Content-Type': 'application/json' },
            data: $scope.request,
        }).then(function (data, status, header, config) {

            console.log(data.data.Data);
            if (data.data.Status == "Success") {
                window.location = data.data.Data.toString(); //$scope._baseUrl + "Dashboard/CustomDates";
            }
            else {
                $scope.ErrorMessage = data.data.ErrorMessage;
                NProgress.done();
                $scope.shoLoginLoader = false;
            }

        }, function (data, status, header, config) {
            $scope.shoLoginLoader = false;
            NProgress.done();
        });

    }

    $scope.SuccessResponse = "";
    $scope.ErrorResponse = "";

    $scope.forgotPassword = function () {

        NProgress.start();
        $scope.shoLoginLoader = true;

        $scope.request = {};
        $scope.request.UserID = $scope.Username.toUpperCase();

        $scope.ErrorMessage = "";

        $http({
            method: 'POST',
            url: $scope._baseUrl + 'api/Partner/ForgotPassword',
            headers: { 'Content-Type': 'application/json' },
            data: $scope.request,
        }).then(function (data, status, header, config) {
            console.log(data.data.Data);
            if (data.data.Status == "Success") {
                $scope.SuccessResponse = "Success!, password has been sent to your registered Email id and Mobile Number, please check your inbox.";
                $scope.ErrorResponse = "";
                NProgress.done();
                $scope.shoLoginLoader = false;
            }
            else {
                $scope.ErrorResponse = data.data.ErrorMessage;
                $scope.SuccessResponse = "";
                NProgress.done();
                $scope.shoLoginLoader = false;
            }

        }, function (data, status, header, config) {
            $scope.shoLoginLoader = false;
            NProgress.done();
            $scope.ErrorResponse = "There was an unexpected error, please try after a while";
        });



    };

});


app.controller('ForcePasswordChange', function ($scope, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');

    $scope.shoLoginLoader = false;
    $scope.SuccessMessage = "";
    $scope.Name = "Ramkumar";


    $scope.validateAndChangePassword = function () {

        if ($scope.NewPassword === $scope.ConfirmPassword) {
            $scope.ConfirmPasswordError = "";
            $scope.SuccessMessage = "";
            $scope.request = {};
            $scope.request.userid = $scope.Username.toUpperCase();
            $scope.request.old_passkey = $scope.Password;
            $scope.request.new_passkey = $scope.NewPassword;

            NProgress.start();


            $http({
                method: 'POST',
                url: $scope._baseUrl + 'api/Partner/ChangePassword',
                headers: { 'Content-Type': 'application/json' },
                data: $scope.request
            }).then(function (data, status, header, config) {

                if (data.data.Status === "Success") {
                    $scope.CurrentPassword = "";
                    $scope.NewPassword = "";
                    $scope.ConfirmPassword = "";
                    $scope.submitted = false;
                    $scope.ErrorMessage = "";
                    $scope.SuccessMessage = "Your password has been changed successfully!";
                } else {
                    $scope.ErrorMessage = data.data.ErrorMessage;
                }

                NProgress.done();
            }, function (data, status, header, config) {
                NProgress.done();
                $scope.ErrorMessage = "Unable to connect server, please try after a while";
            });

        } else {
            $scope.ConfirmPasswordError = "Your New Password and Confirm password are not match. Please enter correct password";
        }

    }

    $scope.SuccessResponse = "";
    $scope.ErrorResponse = "";

    $scope.forgotPassword = function () {

        NProgress.start();
        $scope.shoLoginLoader = true;

        $scope.request = {};
        $scope.request.UserID = $scope.Username.toUpperCase();

        $scope.ErrorMessage = "";

        $http({
            method: 'POST',
            url: $scope._baseUrl + 'api/Partner/ForgotPassword',
            headers: { 'Content-Type': 'application/json' },
            data: $scope.request,
        }).then(function (data, status, header, config) {
            console.log(data.data.Data);
            if (data.data.Status == "Success") {
                $scope.SuccessResponse = "Success!, password has been sent to your registered Email id and Mobile Number, please check your inbox.";
                $scope.ErrorResponse = "";
                NProgress.done();
                $scope.shoLoginLoader = false;
            }
            else {
                $scope.ErrorResponse = data.data.ErrorMessage;
                $scope.SuccessResponse = "";
                NProgress.done();
                $scope.shoLoginLoader = false;
            }

        }, function (data, status, header, config) {
            $scope.shoLoginLoader = false;
            NProgress.done();
            $scope.ErrorResponse = "There was an unexpected error, please try after a while";
        });



    };

});


app.controller('OtherUserLogins', function ($scope, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');

    $scope.shoLoginLoader = false;
    $scope.SuccessMessage = "";
    $scope.Name = "Ramkumar";



    $scope.loginAsPartnerEvent = function (keyEvent) {
        if (keyEvent.which === 13) {
            $scope.loginAsPartner();
        }
    }

    $scope.loginAsPartner = function () {

        NProgress.start();
        $scope.shoLoginLoader = true;

        $http({
            method: 'POST',
            url: $scope._baseUrl + 'api/Account/LoginAsPartner?CustomerCode=' + $scope.CustomerCode,
            headers: { 'Content-Type': 'application/json' },
            data: $scope.request,
        }).then(function (data, status, header, config) {

            if (data.data.Status == "Success") {
                window.location = data.data.Data.toString();
            }
            else {
                $scope.ErrorResponse = data.data.ErrorMessage;
                $scope.SuccessResponse = "";
                NProgress.done();
                $scope.shoLoginLoader = false;
            }

        }, function (data, status, header, config) {
            $scope.shoLoginLoader = false;
            NProgress.done();
            $scope.ErrorResponse = "There was an unexpected error, please try after a while";
        });

    };


    $scope.loginAsBizUserKeyEvent = function (keyEvent) {
        if (keyEvent.which === 13) {
            $scope.loginAsBizUser();
        }
    }

    $scope.loginAsBizUser = function () {

        NProgress.start();
        $scope.shoLoginLoader = true;

        $http({
            method: 'POST',
            url: $scope._baseUrl + 'api/Account/LoginAsBizUser?Username=' + $scope.Username,
            headers: { 'Content-Type': 'application/json' },
            data: $scope.request,
        }).then(function (data, status, header, config) {

            if (data.data.Status == "Success") {
                window.location = data.data.Data.toString();
            }
            else {
                $scope.ErrorResponse = data.data.ErrorMessage;
                $scope.SuccessResponse = "";
                NProgress.done();
                $scope.shoLoginLoader = false;
            }

        }, function (data, status, header, config) {
            $scope.shoLoginLoader = false;
            NProgress.done();
            $scope.ErrorResponse = "There was an unexpected error, please try after a while";
        });

    };


    $scope.loginAsAppleUserKeyEvent = function (keyEvent) {
        if (keyEvent.which === 13) {
            $scope.loginAsAppleUser();
        }
    }

    $scope.loginAsAppleUser = function () {

        NProgress.start();
        $scope.shoLoginLoader = true;

        $http({
            method: 'POST',
            url: $scope._baseUrl + 'api/Account/LoginAsAppleUser?Username=' + $scope.AppleUsername,
            headers: { 'Content-Type': 'application/json' },
            data: $scope.request,
        }).then(function (data, status, header, config) {

            if (data.data.Status == "Success") {
                window.location = data.data.Data.toString();
            }
            else {
                $scope.ErrorResponse = data.data.ErrorMessage;
                $scope.SuccessResponse = "";
                NProgress.done();
                $scope.shoLoginLoader = false;
            }

        }, function (data, status, header, config) {
            $scope.shoLoginLoader = false;
            NProgress.done();
            $scope.ErrorResponse = "There was an unexpected error, please try after a while";
        });

    };
});


app.controller('OTPValidation', function ($scope, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');

    $scope.shoLoginLoader = false;
    $scope.SuccessMessage = "";
    $scope.Name = "Ramkumar";
    $scope.UesrMobile = "";

    $scope.UesrMobile = $("#txtMobileNumber").val();

    console.log($scope.UserMobile);

    $scope.resendOTP = function () {

        NProgress.start();
        $scope.shoLoginLoader = true;
        $("#txtmessage").html("");
        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Account/ResendOTP',
            headers: { 'Content-Type': 'application/json' },
            data: $scope.request,
        }).then(function (data, status, header, config) {

            if (data.data.Status == "Success") {
                //window.location = data.data.Data.toString();
                $scope.shoLoginLoader = false;
                NProgress.done();
                $("#txtmessage").html("OTP sent successfully.");

                setTimeout(function () { $("#txtmessage").html("") }, 2000);

            }
            else {
                $scope.ErrorMessage = data.data.ErrorMessage;
                $scope.SuccessResponse = "";
                NProgress.done();
                $scope.shoLoginLoader = false;
                //$("#txtmessage").html("OTP sent successfully.");
                //setTimeout(function () { $("#txtmessage").html("") }, 2000);
                //$scope.shoLoginLoader = false;
            }

        }, function (data, status, header, config) {
            $scope.shoLoginLoader = false;
            NProgress.done();
            $scope.ErrorResponse = "OTP Resend failed, kindly try again";
        });
        //$("#txtmessage").html("OTP sent successfully.");
        //setTimeout(function () { $("#txtmessage").html("") }, 2000);
    }

    $scope.validateOTP = function () {

        NProgress.start();
        $scope.shoLoginLoader = true;
        $scope.request = {};
        $scope.request.Username = $("#txtUsername").val();
        $scope.request.OTP = $scope.OTP;

        $http({
            method: 'POST',
            url: $scope._baseUrl + 'api/Account/ValidateOTP',
            headers: { 'Content-Type': 'application/json' },
            data: $scope.request,
        }).then(function (data, status, header, config) {

            if (data.data.Status == "Success") {
                window.location = data.data.Data.toString();
            }
            else {
                $scope.ErrorMessage = data.data.ErrorMessage;
                $scope.SuccessResponse = "";
                NProgress.done();
                $scope.shoLoginLoader = false;
            }

        }, function (data, status, header, config) {
            $scope.shoLoginLoader = false;
            NProgress.done();
            $scope.ErrorMessage = "There was an unexpected error, please try after a while";
        });

    };


});
﻿var app = angular.module("Redington",[]);

app.factory('ShoppingCart', function () {

    return {
        isBasicDetailsValid: false,
        isBranchDetailsValid: false,
        isAddressDetailsValid: false,
        isBankDetailsValid: false,
        isDocumentsValid: false
    };

});

app.directive('uploadFiles', function () {
    return {
        scope: true,        //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                console.log(event);
                var files = event.target.files;
                //iterate files since 'multiple' may be specified on the element

                scope.uploadCartFile(event);

                //emit event upward
                //scope.$emit("seletedFile", { file: files[0], fileFor: attrs.filefor });
            });
        }
    };
});

app.controller('Cart_Details', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();

    $scope.ShoppingCart = ShoppingCart;

    $http({
        method: 'GET',
        url: $scope._baseUrl + 'api/Order/GetShoppingCart',
        headers: { 'Content-Type': 'application/json' },
    }).then(function (data, status, header, config) {

        console.log("Cart Result");
        console.log(data.data.Data);

        $scope.ShoppingCart.ItemsCount = data.data.Data.CartItemCount;
        $scope.ShoppingCart.Cart = data.data.Data.Items;
        $scope.ShoppingCart.CartTotal = data.data.Data.CartTotal;
        $scope.ShoppingCart.CartSubTotal = data.data.Data.CartSubTotal;
        $scope.ShoppingCart.CartDiscountTotal = data.data.Data.CartDiscountTotal;
        $scope.ShoppingCart.TaxTotal = data.data.Data.TaxTotal;

    }, function (data, status, header, config) {

    });

});

app.controller('Order_ViewCart', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();
    $scope.IsCTOItemInCart = false;
    $scope.IsB2BPartner = false;

    $scope.ShoppingCart = ShoppingCart;

    $scope.initShoppingCart = function (ShoppingCart) {
        $scope.ShoppingCart.ItemsCount = ShoppingCart.CartItemCount;
        $scope.ShoppingCart.Cart = ShoppingCart.Items;
        $scope.ShoppingCart.CartTotal = ShoppingCart.CartTotal;
        $scope.ShoppingCart.CartSubTotal = ShoppingCart.CartSubTotal;
        $scope.ShoppingCart.CartDiscountTotal = ShoppingCart.CartDiscountTotal;
        $scope.ShoppingCart.TaxTotal = ShoppingCart.TaxTotal;
    }

    $scope.loadShoppingCart = function () {

        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/GetShoppingCart',
            headers: { 'Content-Type': 'application/json' },
        }).then(function (data, status, header, config) {

            console.log("Cart Result");
            console.log(data.data.Data);

            $scope.initShoppingCart(data.data.Data);
            $scope.ShoppingCart.Cart = data.data.Data.Items;
            $scope.IsB2BPartner = data.data.Data.IsB2BPartner;

            $scope.checkCTOItemInTheCart();
        }, function (data, status, header, config) {

        });
    }

    $scope.loadShoppingCart();

    $scope.checkCTOItemInTheCart = function () {
        var ctoItems = $scope.ShoppingCart.Cart.filter(function (cartItem) {
            return cartItem.IsCTOProduct === true;
        });

        if (ctoItems != null && ctoItems != undefined && ctoItems.length > 0) {
            $scope.IsCTOItemInCart = true;
        } else {
            $scope.IsCTOItemInCart = false;
        }
    }

    $scope.removeCartItem = function (item) {

        if (item.quantity <= 0) {
            $.toast({
                heading: 'Opps!',
                text: 'Please select quantity atleast 1.',
                position: {
                    right: 1,
                    top: 70
                },
                stack: true,
                hideAfter: 2000,
                icon: 'error'
            })
        } else {

            $scope.showLoader = false;
            //alert("Success " + item.quantity);

            $scope.request = {};

            $scope.request.CustomerCode = $scope.CustomerCode;
            $scope.request.ItemCode = item.ItemCode;
            $scope.request.ItemName = item.Item_Description;
            $scope.request.VendorPartCode = item.VendorPartCode;
            $scope.request.Quantity = item.quantity;
            $scope.request.UnitPrice = item.Actual_Price;
            $scope.request.DiscountPercent = item.Discount_Percent;
            $scope.request.DiscountPrice = item.Discount_Price;
            //$scope.request.Item_Description = item.Item_Description;
            //$scope.request.Item_Category = item.Item_Category;
            //$scope.request.Brand = item.Brand;


            $http({
                method: 'POST',
                url: $scope._baseUrl + 'api/Order/DeleteCartItem',
                headers: { 'Content-Type': 'application/json' },
                data: $scope.request
            }).then(function (data, status, header, config) {

                console.log(data.data.Data);

                if (data.data.Status == "Success") {

                    $scope.initShoppingCart(data.data.Data);
                    $scope.checkCTOItemInTheCart();
                    $.toast({
                        heading: 'Cart Deletion',
                        text: 'Item deleted from the cart successfully!',
                        position: {
                            right: 1,
                            top: 70
                        },
                        stack: true,
                        hideAfter: 1500,
                        icon: 'success'
                    })

                }

                NProgress.done();
                $scope.showLoader = false;

            }, function (data, status, header, config) {

                NProgress.done();
                $scope.showLoader = false;
            });
        }


    }

    $scope.Address = {};
    $scope.Address.BillingSequence = 'Select';
    $scope.Address.DeliverySequence = 'Select';
    $scope.Address.BillingLocation = "";
    $scope.Address.DeliveryLocation = "";
    $scope.AddressDetails = [];
    $scope.BillingAddressDetails = [];
    $scope.showLoader = true;
    $scope.ShowOrderConfirmationDialog = false;
    var DateObj = new Date();
    DateObj.setDate(DateObj.getDate() + 1);
    $scope.Address.ValidTill = ('0' + (DateObj.getDate())).slice(-2) + '-' + ('0' + (DateObj.getMonth() + 1)).slice(-2) + '-' + DateObj.getFullYear();

    $scope.getAddressDetails = function () {

        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/GetAddressSequences?CustomerCode=' + $scope.CustomerCode,
            headers: { 'Content-Type': 'application/json' },
        }).then(function (data, status, header, config) {

            console.log("Address Response");
            console.log(data.data.Data);

            $scope.AddressDetails = JSON.parse(JSON.stringify(data.data.Data));
            $scope.BillingAddressDetails = JSON.parse(JSON.stringify(data.data.Data));;
            var EXWAddress = $scope.BillingAddressDetails.splice(1, 1);

            //if (localStorage.getItem('DeliverySequence') != null || localStorage.getItem('DeliverySequence') != undefined) {
            //    $scope.Address.DeliverySequence = localStorage.getItem('DeliverySequence');
            //    $scope.showDeliveryAddress();
            //}

            //if (localStorage.getItem('BillingSequence') != null || localStorage.getItem('BillingSequence') != undefined) {
            //    $scope.Address.BillingSequence = localStorage.getItem('BillingSequence');
            //    $scope.showBillingAddress();
            //}

            $scope.showLoader = false;
        }, function (data, status, header, config) {
            $scope.showLoader = false;
        });

    }

    $scope.getAddressDetails();

    $scope.showBillingAddress = function () {

        console.log($scope.Address.BillingSequence);
        if ($scope.Address.BillingSequence != '' && $scope.Address.BillingSequence != null && $scope.Address.BillingSequence != 'Select') {
            $scope.isValidBillingAddress = true;
            $scope.BillingAddress = {};
            //$scope.BillingAddress = address;

            $scope.BillingAddress = $scope.AddressDetails.filter(function (addressSeq) {
                return (addressSeq.Delivery_Sequence == $scope.Address.BillingSequence);
            })[0];

            $scope.Address.BillingLocation = $scope.BillingAddress.Location_Code;

            console.log($scope.BillingAddress);

            localStorage.setItem('BillingSequence', $scope.Address.BillingSequence);

        } else {
            $scope.isValidBillingAddress = false;
        }
    }

    $scope.showDeliveryAddress = function () {
        debugger;
        if ($scope.Address.DeliverySequence != '' && $scope.Address.DeliverySequence != null && $scope.Address.DeliverySequence != 'Select') {
            $scope.isValidDeliveryAddress = true;
            $scope.DeliveryAddress = {};

            $scope.DeliveryAddress = $scope.AddressDetails.filter(function (addressSeq) {
                return (addressSeq.Delivery_Sequence == $scope.Address.DeliverySequence);
            })[0];

            $scope.Address.DeliveryLocation = $scope.DeliveryAddress.Location_Code;

            console.log($scope.DeliveryAddress);
            localStorage.setItem('DeliverySequence', $scope.Address.DeliverySequence);

        } else {
            $scope.isValidDeliveryAddress = false;
        }

    }

    $scope.ShowPOType = false;
    $scope.Address.ValidityDays = "15";
    $scope.Address.POType = "Demand";
    $scope.ValidityDaysChanged = function (days) {
        if (days == 35) {
            $scope.ShowPOType = true;
        } else {
            $scope.ShowPOType = false;
        }
        console.log(days);
    }

    $scope._POFile = {};
    $scope.OrderConfrimationMessage = "";



    $scope.placeOrderConfirmation = function () {

        if ($scope.Address.ValidityDays === "15") {

            $scope.OrderConfrimationMessage = "You are Placing a demand request to Redington(India) Ptv. Ltd. This will be taken as confirmed order. The material will be Dispatched within 2 weeks, Based on the availability of stocks.";
        }
        else if ($scope.Address.ValidityDays === "35") {

            if ($scope.Address.POType === "Demand") {
                $scope.OrderConfrimationMessage = "You are Placing a demand request to Redington(India) Ptv. Ltd. This will be taken as confirmed order. The material will be Dispatched within 5 weeks, Based on the availability of stocks.";
            }
            else if ($scope.Address.POType === "Forecast") {
                $scope.OrderConfrimationMessage = "You are Placing a Forecast request to Redington India Ltd, You can still convert this Forecast into demand within 2 weeks.";
            }
            else if ($scope.Address.POType === "Back-2-Back") {
                $scope.OrderConfrimationMessage = "You are placing a Back 2 Back confirmed order. The material will be dispatched to you within 5 weeks.";
            }
        }

        $.confirm({
            title: 'Order Confirmation',
            icon: 'fa fa-question-circle',
            columnClass: 'medium',
            type: 'dark',
            content: $scope.OrderConfrimationMessage,
            buttons: {
                Proceed: {
                    text: 'Proceed',
                    btnClass: 'btn btn-sm btn-success',
                    action: function () {
                        $scope.placePreOrder();
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn btn-sm btn-warning'
                }
            }
        });

    }

    $scope.closeOrderConfirmation = function () {
        $scope.ShowOrderConfirmationDialog = false;
    }

    $scope.placePreOrder = function () {
        $scope.ShowOrderConfirmationDialog = false;

        $scope.isValidPOReferenceNo = true;
        $scope.request = {};
        $scope.request.BillingAddress = $scope.Address.BillingSequence;
        $scope.request.DeliveryAddress = $scope.Address.DeliverySequence;
        $scope.request.DeliveryLocation = $scope.Address.DeliveryLocation;
        $scope.request.BillingLocation = $scope.Address.BillingLocation;
        $scope.request.POReferenceNo = $scope.Address.POReferenceNo;
        $scope.request.PartialDelivery = $scope.Address.PartialDelivery;
        $scope.request.ValidTill = $scope.Address.ValidTill;
        $scope.request.ValidityDays = $scope.Address.ValidityDays;
        $scope.request.POType = $scope.Address.POType;

        $scope.request.BillSeq1 = $scope.BillingAddress.Address1;
        $scope.request.BillSeq2 = $scope.BillingAddress.Address2;
        $scope.request.BillSeq3 = $scope.BillingAddress.Address3;
        $scope.request.BillSeq4 = $scope.BillingAddress.Address4;
        $scope.request.BillSeq5 = $scope.BillingAddress.Address5;
        $scope.request.BillSeq6 = $scope.BillingAddress.PIN_Code;

        $scope.request.DelSeq1 = $scope.DeliveryAddress.Address1;
        $scope.request.DelSeq2 = $scope.DeliveryAddress.Address2;
        $scope.request.DelSeq3 = $scope.DeliveryAddress.Address3;
        $scope.request.DelSeq4 = $scope.DeliveryAddress.Address4;
        $scope.request.DelSeq5 = $scope.DeliveryAddress.Address5;
        $scope.request.DelSeq6 = $scope.DeliveryAddress.PIN_Code;

        $scope.request.IGSTBilling = $scope.Address.IGSTBilling;
        $scope.request.PaymentOption = $scope.Address.PaymentOption;

        //$scope._POFile = {};
        //$scope._POFile = event.target.files[0];

        $scope.showOrderLoader = true;


        if ($scope.validatePONumber()) {
            $http({
                method: 'POST',
                url: $scope._baseUrl + 'api/Order/PlacePreOrderWithUpload',
                headers: { 'Content-Type': undefined },
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("request", JSON.stringify($scope.request));
                    formData.append("POFile", data.files);
                    return formData;
                },
                data: { model: $scope.request, files: $scope._POFile }
            }).then(function (data, status, header, config) {

                console.log("Order Response");
                console.log(data.data.Data);

                if (data.data.Status == "Success") {
                    window.location = $scope._baseUrl + "Order/OrderStatus?Status=Success";
                } else {
                    window.location = $scope._baseUrl + "Order/OrderStatus?Status=Failed";
                }

                $scope.showLoader = false;
                $scope.showOrderLoader = false;
            }, function (data, status, header, config) {
                $scope.showLoader = false;
                $scope.showOrderLoader = false;
            });


        } else {
            $scope.showOrderLoader = false;
            swal("Sorry!", $scope.Address.POReferenceNo + " is duplicate, please enter new P.O Reference number", "error", { button: "Ok", });
        }


    }

    $scope.formatNumber = function (x) {
        if (x != null || x != undefined) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        } else {
            return "0";
        }
    }

    $scope.showFileUploadLoader = false;

    $scope.uploadCartFile = function (event) {
        //$scope.showFileUploadLoader = true;
        $scope._POFile = {};
        $scope._POFile = event.target.files[0];
        $scope.Address.PODocument = event.target.files[0].name;
        console.log(event);
    }

    $scope.isValidPOReferenceNo = true;

    $scope.validatePONumber = function () {
        //$scope.isValidPOReferenceNo = true;
        //$scope.frmCart.$setValidity('POReferenceNo', false, $scope.frmCart);
        //swal("Sorry!", "Please enter atleast 1 Proprietor / Partner / Director Address Details", "error", { button: "Ok", });
        return true;
    }

    $scope.validatePOAttachment = function () {

        if ($scope.Address.POType === "Back-2-Back" && $scope._POFile === "") {
            return false;
        } else {
            return true;
        }
    }

    $scope.getProductImage = function (Item_Code) {
        return "http://redingtonb2b.in/Ril_Images/REDC_Products/" + Item_Code + "/" + Item_Code + "_1.png";
    }
});

app.controller('Order_AddressDetails', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();

    $scope.ShoppingCart = ShoppingCart;

    $scope.initShoppingCart = function (ShoppingCart) {
        $scope.ShoppingCart.ItemsCount = ShoppingCart.ItemsCount;
        $scope.ShoppingCart.Cart = ShoppingCart.Items;
    }

    $scope.Address = {};
    $scope.Address.BillingSequence = 'Select';
    $scope.Address.DeliverySequence = 'Select';
    $scope.Address.BillingLocation = "";
    $scope.Address.DeliveryLocation = "";
    $scope.AddressDetails = {};
    $scope.BillingAddressDetails = {};
    $scope.showLoader = true;

    $scope.getAddressDetails = function () {

        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/GetAddressSequences?CustomerCode=' + $scope.CustomerCode,
            headers: { 'Content-Type': 'application/json' },
        }).then(function (data, status, header, config) {

            console.log("Address Response");
            console.log(data.data.Data);

            $scope.AddressDetails = data.data.Data;
            $scope.BillingAddressDetails = JSON.parse(JSON.stringify(data.data.Data));;
            var EXWAddress = $scope.BillingAddressDetails.splice(1, 1);


            $scope.showLoader = false;
        }, function (data, status, header, config) {
            $scope.showLoader = false;
        });

    }

    $scope.getAddressDetails();

    $scope.showBillingAddress = function () {

        console.log($scope.Address.BillingSequence);
        if ($scope.Address.BillingSequence != '' && $scope.Address.BillingSequence != null && $scope.Address.BillingSequence != 'Select') {
            $scope.isValidBillingAddress = true;
            $scope.BillingAddress = {};
            //$scope.BillingAddress = address;

            $scope.BillingAddress = $scope.AddressDetails.filter(function (addressSeq) {
                return (addressSeq.Delivery_Sequence == $scope.Address.BillingSequence);
            })[0];

            $scope.Address.BillingLocation = $scope.BillingAddress.Location_Code;

            console.log($scope.BillingAddress);

        } else {
            $scope.isValidBillingAddress = false;
        }
    }

    $scope.showDeliveryAddress = function () {
        debugger;
        if ($scope.Address.DeliverySequence != '' && $scope.Address.DeliverySequence != null && $scope.Address.DeliverySequence != 'Select') {
            $scope.isValidDeliveryAddress = true;
            $scope.DeliveryAddress = {};

            $scope.DeliveryAddress = $scope.AddressDetails.filter(function (addressSeq) {
                return (addressSeq.Delivery_Sequence == $scope.Address.DeliverySequence);
            })[0];

            $scope.Address.DeliveryLocation = $scope.DeliveryAddress.Location_Code;

            console.log($scope.DeliveryAddress);

        } else {
            $scope.isValidDeliveryAddress = false;
        }

    }

    $scope.placePreOrder = function () {

        $scope.request = {};
        $scope.request.BillingAddress = $scope.Address.BillingSequence;
        $scope.request.DeliveryAddress = $scope.Address.DeliverySequence;
        $scope.request.DeliveryLocation = $scope.Address.DeliveryLocation;
        $scope.request.BillingLocation = $scope.Address.BillingLocation;

        $scope.showOrderLoader = true;
        $http({
            method: 'POST',
            url: $scope._baseUrl + 'api/Order/PlacePreOrder',
            headers: { 'Content-Type': 'application/json' },
            data: $scope.request
        }).then(function (data, status, header, config) {

            console.log("Order Response");
            console.log(data.data.Data);

            if (data.data.Status == "Success") {
                window.location = $scope._baseUrl + "Order/OrderStatus?Status=Success";
            } else {
                window.location = $scope._baseUrl + "Order/OrderStatus?Status=Failed";
            }

            $scope.showLoader = false;
            $scope.showOrderLoader = false;
        }, function (data, status, header, config) {
            $scope.showLoader = false;
            $scope.showOrderLoader = false;
        });

    }

    $scope.$on("seletedFile", function (event, args) {
        console.log(args);
        $scope.$apply(function () {
            ////add the file object to the scope's files collection
            //var _PartnerDocument = new Object();
            //_PartnerDocument.file = args.file;
            //_PartnerDocument.fileFor = args.fileFor;
            //$scope.files.push(_PartnerDocument);

            alert("File Selected");
        });

    });

});


app.controller('Order_Status', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();

});

app.controller('OrderDetails', function ($scope, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();
    $scope.Order = {};
    $scope.OrderNumber = getQueryStringParameter("OrderNumber");

    //NProgress.configure({ parent: '.box' });

    $scope.loadOrderDetails = function () {
        //$scope.OrderNumber = '3000013715';
        //NProgress.start();
        //$scope.showDialog = false;
        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/GetOrderDetails?OrderNumber=' + getQueryStringParameter("OrderNumber"),//+ '&CustomerCode=' + getQueryStringParameter("CustomerCode") + '&Gst=' + getQueryStringParameter("Gst"),
            headers: { 'Content-Type': 'application/json' },
        }).then(function (data, status, header, config) {
            if (data.data.Data != null) {
                if (data.data.Data.status_code == "500") {
                    window.location.href = "Order/Waiting";
                }
                else {
                    console.log("Address Response");
                    console.log(data.data.Data);
                    $scope.Order = data.data.Data;
                    $scope.showLoader = false;
                    //NProgress.done();
                }
            }
            else {
                window.location.href = "Order/WentWrong";
            }
        }, function (data, status, header, config) {
            $scope.showLoader = false;
            // NProgress.done();
        });

    }

    $scope.loadOrderDetails();

    $scope.closeDialog = function () {
        $scope.showDialog = false;
        $(".modal").hide();
    };


    $scope.formatNumber = function (x) {
        if (x != null || x != undefined) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        } else {
            return "0";
        }
    }
});

    
app.controller('PendingOrdersList', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();
    $scope.Order = {};

    NProgress.configure({ parent: '.box' });

    $scope.downloadOrders = function () {
        NProgress.start();

        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/DownloadPendingOrders?DateFrom=' + $(".dateFrom").val() + '&DateTill=' + $(".dateTill").val(),
            headers: { 'Content-Type': 'application/json' }
        }).then(function (data, status, header, config) {

            $scope.isDownloading = false;

            if (data.data.Status == "Success") {

                console.log(data.data.Data);
                window.location.href = data.data.Data;

                $.toast({
                    heading: 'File Download',
                    text: 'File Downloaded successfully!',
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 1100,
                    icon: 'success'
                });
            }
            else {
                $.toast({
                    heading: 'Sorry!',
                    text: data.data.ErrorMessage,
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 1100,
                    icon: 'success'
                });
            }

            NProgress.done();
        }, function (data, status, header, config) {
            NProgress.done();
            $scope.isDownloading = false;
            $.toast({
                heading: 'Opps!',
                text: 'Something went wrong, please try after a while.',
                position: {
                    right: 1,
                    top: 70
                },
                stack: true,
                hideAfter: 1100,
                icon: 'error'
            });
        });


    }

});

app.controller('PendingOrdersListBizUser', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();
    $scope.Order = {};

    NProgress.configure({ parent: '.box' });

    $scope.downloadOrders = function () {
        NProgress.start();

        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/DownloadPendingOrders?DateFrom=' + $(".dateFrom").val() + '&DateTill=' + $(".dateTill").val(),
            headers: { 'Content-Type': 'application/json' }
        }).then(function (data, status, header, config) {

            $scope.isDownloading = false;

            if (data.data.Status == "Success") {

                console.log(data.data.Data);
                window.location.href = data.data.Data;

                $.toast({
                    heading: 'File Download',
                    text: 'File Downloaded successfully!',
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 1100,
                    icon: 'success'
                });
            }
            else {
                $.toast({
                    heading: 'Sorry!',
                    text: data.data.ErrorMessage,
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 1100,
                    icon: 'success'
                });
            }

            NProgress.done();
        }, function (data, status, header, config) {
            NProgress.done();
            $scope.isDownloading = false;
            $.toast({
                heading: 'Opps!',
                text: 'Something went wrong, please try after a while.',
                position: {
                    right: 1,
                    top: 70
                },
                stack: true,
                hideAfter: 1100,
                icon: 'error'
            });
        });


    }

});

app.controller('PendingOrderDetails', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();
    $scope.Order = {};

    NProgress.configure({ parent: '.box' });


    $scope.loadOrderDetails = function () {
        //$scope.OrderNumber = '3000013715';
        NProgress.start();

        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/GetPendingOrderDetails?OrderNumber=' + getQueryStringParameter("OrderNumber"),
            headers: { 'Content-Type': 'application/json' },
        }).then(function (data, status, header, config) {

            console.log("Order Response");
            console.log(data.data.Data);
            $scope.Order = data.data.Data;

            $scope.showLoader = false;
            NProgress.done();
        }, function (data, status, header, config) {
            $scope.showLoader = false;
            NProgress.done();
        });

    }

    $scope.loadOrderDetails();

    $scope.cancelOrder = function () {
        NProgress.start();

        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/Cancel?OrderNumber=' + getQueryStringParameter("OrderNumber") + '&Remarks=' + $scope.Order.Remarks,
            headers: { 'Content-Type': 'application/json' },
        }).then(function (data, status, header, config) {

            if (data.data.Status === "Success") {
                $scope.Order = data.data.Data;
                $.toast({
                    heading: 'Success!',
                    text: 'Order has been cancelled successfully!',
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 2000,
                    icon: 'success'
                });
            } else {
                $.toast({
                    heading: 'Opps!',
                    text: 'There was an error, kindly try after a while.',
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 2000,
                    icon: 'error'
                });
            }

            $scope.showLoader = false;
            NProgress.done();
        }, function (data, status, header, config) {
            $.toast({
                heading: 'Error',
                text: 'There was an error, kindly try after a while.',
                position: {
                    right: 1,
                    top: 70
                },
                stack: true,
                hideAfter: 2000,
                icon: 'error'
            });
            $scope.showLoader = false;
            NProgress.done();
        });
    };

    $scope.approveOrder = function () {
        NProgress.start();

        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/Approve?OrderNumber=' + getQueryStringParameter("OrderNumber") + '&Remarks=' + $scope.Order.Remarks,
            headers: { 'Content-Type': 'application/json' },
        }).then(function (data, status, header, config) {

            $scope.Order = data.data.Data;

            if (data.data.Status === "Success") {

                $.toast({
                    heading: 'Success!',
                    text: 'Order has been approved successfully!',
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 2000,
                    icon: 'success'
                });
            } else {
                $.toast({
                    heading: 'Opps!',
                    text: data.data.ErrorMessage,
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 2000,
                    icon: 'error'
                });
            }

            $scope.showLoader = false;
            NProgress.done();
        }, function (data, status, header, config) {
            $.toast({
                heading: 'Error',
                text: 'There was an error, kindly try after a while.',
                position: {
                    right: 1,
                    top: 70
                },
                stack: true,
                hideAfter: 2000,
                icon: 'error'
            });
            $scope.showLoader = false;
            NProgress.done();
        });
    };

    $scope.ConvertForecastToOrder = function () {

        $scope.hasAnyError = false;

        $.each($scope.Order.Items, function (index, item) {
            if (item.BalanceQuantity == 0 || (item.BalanceQuantity <= (item.Quantity - item.BilledQuantity) && item.BalanceQuantity >= 1)) {
                item.ErrorMessage = "";
            }
            else {
                item.ErrorMessage = "Invalid Quantity";
                $scope.hasAnyError = true;
            }
        });

        $scope.TotalQuantity = 0;

        $.each($scope.Order.Items, function (index, item) {
            $scope.TotalQuantity = $scope.TotalQuantity + item.BalanceQuantity;
        });


        if ($scope.hasAnyError == false) {

            if ($scope.TotalQuantity > 0) {

                NProgress.start();

                $http({
                    method: 'POST',
                    url: $scope._baseUrl + 'api/Order/ConvertForecastToOrder?OrderNumber=' + getQueryStringParameter("OrderNumber") + '&Remarks=',
                    headers: { 'Content-Type': 'application/json' },
                    data: $scope.Order
                }).then(function (data, status, header, config) {

                    $scope.Order = data.data.Data;

                    if (data.data.Status == "Success") {
                        window.location = $scope._baseUrl + "Order/OrderStatus?Status=Success";
                    } else {
                        window.location = $scope._baseUrl + "Order/OrderStatus?Status=Failed";
                    }

                    $scope.showLoader = false;
                    NProgress.done();
                }, function (data, status, header, config) {
                    $.toast({
                        heading: 'Error',
                        text: 'There was an error, kindly try after a while.',
                        position: {
                            right: 1,
                            top: 70
                        },
                        stack: true,
                        hideAfter: 2000,
                        icon: 'error'
                    });
                    $scope.showLoader = false;
                    NProgress.done();
                });
            }
        }
        else {
            $.toast({
                heading: 'Error',
                text: 'Please enter atleast 1 quantity to place an order.',
                position: {
                    right: 1,
                    top: 70
                },
                stack: true,
                hideAfter: 2000,
                icon: 'error'
            });
            $scope.showLoader = false;
            NProgress.done();
        }

    };

    $scope.updateItemCode = function (item) {
        //alert(item.NewItemCode);

        $scope.request = {};
        $scope.request.VendorCode = item.VendorCode;
        $scope.request.OptionCode = item.CTOOptionCode;
        $scope.request.SAPItemCode = item.NewItemCode;

        NProgress.start();

        $http({
            method: 'POST',
            url: $scope._baseUrl + 'api/Product/UpdateCTOSAPItemCode',
            headers: { 'Content-Type': 'application/json' },
            data: $scope.request
        }).then(function (data, status, header, config) {

            console.log("UpdateCTOSAPItemCode Response");
            console.log(data.data.Data);

            if (data.data.Status == "Success") {
                $.toast({
                    heading: 'Success',
                    text: 'SAP Item Code updated successfully!',
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 2000,
                    icon: 'success'
                });
                $scope.loadOrderDetails();
            } else {
                $.toast({
                    heading: 'Error',
                    text: data.data.ErrorMessage,
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 2000,
                    icon: 'error'
                });
            }

            $scope.showLoader = false;
            NProgress.done();
        }, function (data, status, header, config) {
            $scope.showLoader = false;
            NProgress.done();
        });

    };

    $scope.formatNumber = function (x) {
        if (x != null || x != undefined) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        } else {
            return "0";
        }
    }


});

app.controller('ForecastOrdersList', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();
    $scope.Order = {};

    NProgress.configure({ parent: '.box' });

    $scope.downloadOrders = function () {
        NProgress.start();

        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/DownloadForecastOrders?DateFrom=' + $(".dateFrom").val() + '&DateTill=' + $(".dateTill").val(),
            headers: { 'Content-Type': 'application/json' }
        }).then(function (data, status, header, config) {

            $scope.isDownloading = false;

            if (data.data.Status == "Success") {

                console.log(data.data.Data);
                window.location.href = data.data.Data;

                $.toast({
                    heading: 'File Download',
                    text: 'File Downloaded successfully!',
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 1100,
                    icon: 'success'
                });
            }
            else {
                $.toast({
                    heading: 'Sorry!',
                    text: data.data.ErrorMessage,
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 1100,
                    icon: 'success'
                });
            }

            NProgress.done();
        }, function (data, status, header, config) {
            NProgress.done();
            $scope.isDownloading = false;
            $.toast({
                heading: 'Opps!',
                text: 'Something went wrong, please try after a while.',
                position: {
                    right: 1,
                    top: 70
                },
                stack: true,
                hideAfter: 1100,
                icon: 'error'
            });
        });


    }

});


app.directive('uploadFiles', function () {
    return {
        scope: true,        //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                console.log(event);
                var files = event.target.files;
                //iterate files since 'multiple' may be specified on the element

                scope.uploadCartFile(event);
                //emit event upward
                //scope.$emit("seletedFile", { file: files[0], fileFor: attrs.filefor });
            });
        }
    };
});

app.controller('UploadBulkOrder', function ($scope, ShoppingCart, $http, $location, $window, $sce) {

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();

    $scope.btnDownloadExcel = function () {

        $scope.isDownloading = true;
        //$scope.request = {};
        //$scope.request.CustomerCode = "";
        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/DownloadBulkOrderExcel',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (data, status, header, config) {

            $scope.isDownloading = false;

            if (data.data.Status == "Success") {

                console.log(data.data.Data);
                window.location.href = data.data.Data;

                $.toast({
                    heading: 'File Download',
                    text: 'File Downloaded successfully!',
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 1100,
                    icon: 'success'
                });
            }
            else {
                $.toast({
                    heading: 'Sorry!',
                    text: data.data.ErrorMessage,
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 1100,
                    icon: 'success'
                });
            }


        }, function (data, status, header, config) {
            $scope.isDownloading = false;
            $.toast({
                heading: 'Opps!',
                text: 'Something went wrong, please try after a while.',
                position: {
                    right: 1,
                    top: 70
                },
                stack: true,
                hideAfter: 1100,
                icon: 'error'
            });
        });

    }

    $scope.POType = "Demand";


    $scope.showFileUploadLoader = false;
    $scope.uploadCartFile = function (event) {

        $scope.showFileUploadLoader = true;
        $scope._CartFile = {};
        $scope._CartFile = event.target.files[0];

        $scope.request = {};
        $scope.request.CustomerCode = $scope.CustomerCode;
        $scope.request.POType = $scope.POType;

        $http({
            method: 'POST',
            url: $scope._baseUrl + 'api/Order/UploadBulkOrderFile',
            headers: { 'Content-Type': undefined },
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("CartFile", data.files);
                return formData;
            },
            data: { model: $scope.request, files: $scope._CartFile }

        }).then(function (data, status, header, config) {
            console.log("success");


            if (data.data.Status == "Success") {

                $('#uploadCart').val('');

                $.toast({
                    heading: 'Success',
                    text: 'Cart uploaded successfully!',
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 2000,
                    icon: 'success'
                });
                window.location = data.data.Data;
            }

            $scope.showFileUploadLoader = false;

        }, function (data, status, header, config) {
            console.log(data);
            console.log(status);
            console.log(header);
            console.log(config);
            console.log("File upload failed");
            $scope.showFileUploadLoader = false;
        });

        console.log(event);
        //alert("Test");

    }

});


app.controller('Partner_OrderHistory', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();

    $scope.ShoppingCart = ShoppingCart;

    $scope.initShoppingCart = function (ShoppingCart) {
        $scope.ShoppingCart.ItemsCount = ShoppingCart.CartItemCount;
        $scope.ShoppingCart.Cart = ShoppingCart.Items;
        $scope.ShoppingCart.CartTotal = ShoppingCart.CartTotal;
        $scope.ShoppingCart.CartSubTotal = ShoppingCart.CartSubTotal;
        $scope.ShoppingCart.CartDiscountTotal = ShoppingCart.CartDiscountTotal;
        $scope.ShoppingCart.TaxTotal = ShoppingCart.TaxTotal;
    }

    NProgress.configure({ parent: '.page-header' });

    $scope.downloadSerialNumbers = function () {

        NProgress.start();

        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/DownloadSerialNumbersForCustomer?DateFrom=' +
                $(".dateFrom").val() + '&DateTill=' + $(".dateTill").val() + '&CustomerCode=' + $scope.CustomerCode,
            headers: { 'Content-Type': 'application/json' },
            data: $scope.request
        }).then(function (data, status, header, config) {

            if (data.data.Status == "Success") {

                console.log(data.data.Data);
                window.location.href = data.data.Data;

                $.toast({
                    heading: 'File Download',
                    text: 'File Downloaded successfully!',
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 1100,
                    icon: 'success'
                });
            }
            else {
                $.toast({
                    heading: 'Sorry!',
                    text: data.data.ErrorMessage,
                    position: {
                        right: 1,
                        top: 70
                    },
                    stack: true,
                    hideAfter: 1100,
                    icon: 'success'
                });
            }

            NProgress.done();
        }, function (data, status, header, config) {
            NProgress.done();
            $.toast({
                heading: 'Opps!',
                text: 'Something went wrong, please try after a while',
                position: { right: 1, top: 70 },
                stack: true,
                hideAfter: 2000,
                icon: 'error'
            });
        });

    };

    $scope.formatNumber = function (x) {
        if (x != null || x != undefined) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        } else {
            return "0";
        }
    }
});

app.controller('Partner_AddressList', function ($scope, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();

    NProgress.configure({ parent: '.page-header' });

    $scope.getAddressDetails = function () {

        NProgress.start();

        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/GetAddressSequences?CustomerCode=' + $scope.CustomerCode,
            headers: { 'Content-Type': 'application/json' },
        }).then(function (data, status, header, config) {

            console.log("Address Response");
            console.log(data.data.Data);

            $scope.AddressDetails = JSON.parse(JSON.stringify(data.data.Data));
            $scope.AddressDetails.splice(1, 1);


            NProgress.done();
        }, function (data, status, header, config) {
            NProgress.done();
        });

    }


    $scope.getAddressDetails();

});



app.controller('Partner_Profile', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');
    //$scope._baseUrl = "http://localhost:59061/";
    $scope.CustomerCode = $("#hfCustomerCode").val();

    $scope.ShoppingCart = ShoppingCart;

    $scope.initShoppingCart = function (ShoppingCart) {
        $scope.ShoppingCart.ItemsCount = ShoppingCart.CartItemCount;
        $scope.ShoppingCart.Cart = ShoppingCart.Items;
        $scope.ShoppingCart.CartTotal = ShoppingCart.CartTotal;
        $scope.ShoppingCart.CartSubTotal = ShoppingCart.CartSubTotal;
        $scope.ShoppingCart.CartDiscountTotal = ShoppingCart.CartDiscountTotal;
        $scope.ShoppingCart.TaxTotal = ShoppingCart.TaxTotal;
    }

    NProgress.configure({ parent: '.page-header' });


    $scope.getProfile = function () {

        NProgress.start();

        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/GetProfile?CustomerCode=' + $scope.CustomerCode,
            //url: 'https://ril-edi.redington.co.in/WhatsUP_V7/api/Partner/PartnerProfile?CustomerCode=' + $scope.CustomerCode,
            headers: { 'Content-Type': 'application/json' },
        }).then(function (data, status, header, config) {

            console.log("Address Response");
            console.log(data.data.Data);

            $scope.Profile = JSON.parse(JSON.stringify(data.data.Data));

            NProgress.done();
        }, function (data, status, header, config) {
            NProgress.done();
        });

    }


    $scope.getProfile();


    $scope.formatNumber = function (x) {
        if (x != null || x != undefined) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        } else {
            return "0";
        }
    }
});

app.controller('Partner_PaymentDue', function ($scope, ShoppingCart, $http, $location, $window) {

    $scope._baseUrl = $("base").attr('href');
    $scope.CustomerCode = $("#hfCustomerCode").val();



    //NProgress.configure({ parent: '.box' });

    $scope.getPaymentDue = function () {

        //NProgress.start();

        $http({
            method: 'GET',
            url: $scope._baseUrl + 'api/Order/GetPaymentDues?CustomerCode=' + $scope.CustomerCode,
            headers: { 'Content-Type': 'application/json' },
        }).then(function (data, status, header, config) {

            console.log("Payment Due Response");
            console.log(data.data.Data);

            $scope.PaymentDue = data.data.Data;

            //NProgress.done();
        }, function (data, status, header, config) {
            //NProgress.done();
        });

    }


    $scope.getPaymentDue();


    $scope.formatNumber = function (x) {
        if (x != null || x != undefined) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        } else {
            return "0";
        }
    }
});


function getQueryStringParameter(key) {
    key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
    var match = location.search.match(new RegExp("[?&]" + key + "=([^&]+)(&|$)"));
    return match && decodeURIComponent(match[1].replace(/\+/g, " "));
}


function LoadDatatable_DeAlign(custcode) {



    myTable = $('#example').DataTable({
        "processing": true, stateSave: true,
        "bDestroy": true,
        "serverSide": false, "paging": true,
        "ordering": true,
        "info": true,
        "pagingType": "full_numbers",
        "paging": true,
        "lengthMenu": [10, 25, 50, 75, 100],
        "ajax": {
            "url": "api/Order/GetPaymentDuesByWeek?CustomerCode=" + custcode,
            "type": 'GET',
            "dataType": 'json'
        },



        "columns": [
        { "data": null, defaultContent: '' },
        { data: "InvoiceNumber" },
        { data: "InvoiceDate" },
        { data: "DueDate" },
        { data: "OverDueDays" },
        { data: "InvoiceValue" },
        { data: "BalanceDueAmount" },
        { data: "PendingDays" },
        ],
        order: [[1, "asc"]],
        columnDefs: [
        {
            orderable: false,
            className: 'select-checkbox',
            targets: 0
        },
        ],
        retrieve: true,
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }, buttons: [
        'selectAll',
        'selectNone'
        ]
        //'columnDefs': [{
        // 'targets': 0,
        // 'searchable': false,
        // 'orderable': false,
        // 'width': '1%',
        // 'className': 'dt-body-center',
        // 'render': function (data, type, full, meta) {
        // return '<input type="checkbox">';
        // }
        //}],
    });



    myTable.on('select deselect draw', function () {
        var all = myTable.rows({ search: 'applied' }).count(); // get total count of rows
        var selectedRows = myTable.rows({ selected: true, search: 'applied' }).count(); // get total count of selected rows



        if (selectedRows < all) {
            $('#MyTableCheckAllButton i').attr('class', 'fa fa-square-o');
        } else {
            $('#MyTableCheckAllButton i').attr('class', 'fa fa-check-square-o');
        }
    });



    $('#MyTableCheckAllButton').click(function () {
        var all = myTable.rows({ search: 'applied' }).count(); // get total count of rows
        var selectedRows = myTable.rows({ selected: true, search: 'applied' }).count(); // get total count of selected rows




        if (selectedRows < all) {
            //Added search applied in case user wants the search items will be selected
            myTable.rows({ search: 'applied' }).deselect();
            myTable.rows({ search: 'applied' }).select();
        } else {
            myTable.rows({ search: 'applied' }).deselect();
        }



        if (selectedRows < all) {
            $('#MyTableCheckAllButton i').attr('class', 'fa fa-check-square-o');



        } else {
            $('#MyTableCheckAllButton i').attr('class', 'fa fa-square-o');
        }
    });
    NProgress.done();
}






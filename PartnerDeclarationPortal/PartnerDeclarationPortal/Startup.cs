﻿using Microsoft.Owin;
using Owin;


[assembly: OwinStartupAttribute(typeof(PartnerDeclarationPortal.Startup))]
namespace PartnerDeclarationPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartnerDeclarationPortal.Areas.api.Models
{
    public class User
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public string PartnerName { get; set; }
        public string PartnerCode { get; set; }

        public bool IsActive { get; set; }
        public string CreatedOn { get; set; }
        public string DisplayName { get; set; }
        public string Role { get; set; }
        public string EmailId { get; set; }
        public string OTP { get; set; }
        public string IPAddress { get; set; }

        public string MobileNo { get; set; }
        public bool AccountStatus { get; set; }
        public DateTime LastPasswordChanged { get; set; }
        public string Remarks { get; set; }
    }
}
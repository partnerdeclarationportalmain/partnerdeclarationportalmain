﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartnerDeclarationPortal.Areas.api.Models
{
    public class PendingCheques
    {
        public string BusinessCode { get; set; }
        public string BusinessDESC { get; set; }
        public string CustomerCode { get; set; }
        public string InvoiceNumber { get; set; }
        public double InvoiceValue { get; set; }
        public string BalanceDueAmount { get; set; }
        public string InvoiceDate { get; set; }
        public string PendingDays { get; set; }
        public string DueDate { get; set; }
        public string OverDueDays { get; set; }
        public string Page { get; set; }
        public string TotalPage { get; set; }
        public bool active { get; set; }
        public string OutstandingAmount { get; set; }
    }
}
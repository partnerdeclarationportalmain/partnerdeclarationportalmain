﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PartnerDeclarationPortal.Areas.api.Models
{
    public class MyListModel
    {
        public string SelectedItemId { get; set; }
        public IEnumerable<SelectListItem> Items { get; set; }
    }
    public class AddressSequence
    {
        public AddressSeq DeliverySequence { get; set; }
        public AddressSeq BillingSequence { get; set; }
    }
    public class AddressSeq
    {
        public string Customer_Code { get; set; }
        public string Delivery_Sequence { get; set; }
        public string Customer_Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Address5 { get; set; }
        public string PIN_Code { get; set; }
        public string Payment_Days { get; set; }
        public string Payment_Method { get; set; }
        public string Location_Code { get; set; }
    }
}
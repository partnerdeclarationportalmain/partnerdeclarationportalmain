﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartnerDeclarationPortal.Areas.api.Models
{
    public class Detail
    {
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string Week { get; set; }
        public string InvoiceNumber { get; set; }
        public string OutstandingAmount { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoiceValue { get; set; }
        public string DueAmount { get; set; }
        public string ODDays { get; set; }
        public string DueDate { get; set; }
        public DateTime newDuedate { get; set; }
        
    }

    public class Header
    {
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string TotalInvoiceValue { get; set; }
        public string TotalOutstanding { get; set; }
        public string TotalOverDue { get; set; }
        public string Week1 { get; set; }
        public string Week2 { get; set; }
        public string Week3 { get; set; }
        public string Week4 { get; set; }
        public string WeekGreater4 { get; set; }
        public string WEEK { get; set; }
    }

    public class PaymentDueData
    {
        public List<Detail> Detail { get; set; }
        public List<Header> Header { get; set; }
    }

    public class PaymentDue
    {
        public PaymentDueData Data { get; set; }
    }

    //public class ParterEnquiryHistory
    //{
    //    public List<Datum> ParterList { get; set; }
    //    public List<Datum> EnquiryList { get; set; }
    //}

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class Datum
    {
        public string MANDT { get; set; }
        public string MaterialGroup { get; set; }
        public string InvoiceNo { get; set; }
        public string MaterialGroupDesc { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public string SBUCode { get; set; }
        public string SalesOffice { get; set; }
        public string SalesOfficeDescription { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string Profile { get; set; }
        public string Description { get; set; }
        public string SalesDocumentType { get; set; }
        public string Incoterms { get; set; }
        public string IncotermsDescription { get; set; }
        public string QuotationNumber { get; set; }
        public string QuotationDate { get; set; }
        public string QuotationTime { get; set; }
        public string SalesOrderNo { get; set; }
        public string SalesOrderDate { get; set; }
        public string SalesOrderTime { get; set; }
        public string DeliveryNote { get; set; }
        public string DeliveryNoteDate { get; set; }
        public string DeliveryNoteTime { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoiceTime { get; set; }
        public string BilledQuantity { get; set; }
        public string NetValue { get; set; }
        public string DispatchDate { get; set; }
        public string DispatchTime { get; set; }
        public string DeliveryDate { get; set; }
        public string DeliveryTime { get; set; }
        public string BizHoldReleasedUser { get; set; }
        public string BizHoldReleasedDATE { get; set; }
        public string BizHoldReleasedTIME { get; set; }
        public string BranchHoldReleasedUser { get; set; }
        public string BranchHoldReleasedDate { get; set; }
        public string BranchHoldReleasedTime { get; set; }
        public string OEMHoldReleasedUser { get; set; }
        public string OEMHoldReleasedDate { get; set; }
        public string OEMHoldReleasedTime { get; set; }
        public string CreditHoldReleasedUser { get; set; }
        public string CreditHoldReleasedDate { get; set; }
        public string CreditHoldReleasedTime { get; set; }
        public string TAXHoldReleasedDate { get; set; }
        public string TAXHoldReleasedTime { get; set; }
        public string PODCreatedDate { get; set; }
        public string PODCreatedTime { get; set; }
        public string PODUploadDate { get; set; }
        public string PODUploadTime { get; set; }
        public string CustomerReference { get; set; }
        public string ScanningStatus { get; set; }
        public string SBUDescription { get; set; }
        public string TaxHoldReleasedUser { get; set; }
        public string RecordNumber { get; set; }
        public string PageNumber { get; set; }
        public string TotalPage { get; set; }
        public string ShipToCity { get; set; }
    }

    public class PartEnquiery
    {
        public List<Datum> Data { get; set; }
    }



    public class ParterEnquiry
        {

        public string InvoiceNo { get; set; }
        public string InvoiceDate { get; set; }
        public string MaterialGroupDesc { get; set; }
        public double BilledQuantity { get; set; }
        public double NetValue { get; set; }
            public string CustomerReference { get; set; }
        public string ShipToCity { get; set; }

    }

}
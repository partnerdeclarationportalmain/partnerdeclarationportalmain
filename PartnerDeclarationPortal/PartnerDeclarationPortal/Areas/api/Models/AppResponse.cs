﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartnerDeclarationPortal.Areas.api.Models
{
    public class AppResponse
    {
        public string StatusCode { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public string ErrorMessage { get; set; }

        public dynamic Data { get; set; }
        public dynamic MasterData1 { get; set; }
        public dynamic ExtraInfo1 { get; set; }
        public dynamic ExtraInfo2 { get; set; }

        //public ShoppingCart ShoppingCart { get; set; }

        //public AppResponse()
        //{
        //    this.ShoppingCart = new ShoppingCart();
        //}
    }

    public class AppResponseAPI
    {
        public string StatusCode { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public string ErrorMessage { get; set; }

        public dynamic Data { get; set; }

        //public ShoppingCart ShoppingCart { get; set; }

        public AppResponseAPI()
        {
            //this.ShoppingCart = new ShoppingCart();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartnerDeclarationPortal.Areas.api.Models.SAP
{
    public class SAPResponse
    {
        public object Status_Message { get; set; }
        public object Status_Code { get; set; }
        public object Description { get; set; }
        public string SOSStatus { get; set; }
        public string SOSDescription { get; set; }
        public dynamic Data { get; set; }
    }
}
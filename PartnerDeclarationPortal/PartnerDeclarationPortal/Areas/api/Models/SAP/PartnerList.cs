﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartnerDeclarationPortal.Areas.api.Models.SAP
{
    public class PartEnquierHistory
    {
        public List<PartnerDetails> Data { get; set; }
        public List<PartnerDetails> Value { get; set; }
    }
    public class PartnerDetails
    {
        public string MANDT { get; set; }
        public string MaterialGroup { get; set; }
        public string InvoiceNo { get; set; }
        public string MaterialGroupDesc { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public string SBUCode { get; set; }
        public string SalesOffice { get; set; }
        public string SalesOfficeDescription { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string Profile { get; set; }
        public string Description { get; set; }
        public string SalesDocumentType { get; set; }
        public string Incoterms { get; set; }
        public string IncotermsDescription { get; set; }
        public string QuotationNumber { get; set; }
        public string QuotationDate { get; set; }
        public string QuotationTime { get; set; }
        public string SalesOrderNo { get; set; }
        public string SalesOrderDate { get; set; }
        public string SalesOrderTime { get; set; }
        public string DeliveryNote { get; set; }
        public string DeliveryNoteDate { get; set; }
        public string DeliveryNoteTime { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoiceTime { get; set; }
        public string BilledQuantity { get; set; }
        public string NetValue { get; set; }
        public string DispatchDate { get; set; }
        public string DispatchTime { get; set; }
        public string DeliveryDate { get; set; }
        public string DeliveryTime { get; set; }
        public string BizHoldReleasedUser { get; set; }
        public string BizHoldReleasedDATE { get; set; }
        public string BizHoldReleasedTIME { get; set; }
        public string BranchHoldReleasedUser { get; set; }
        public string BranchHoldReleasedDate { get; set; }
        public string BranchHoldReleasedTime { get; set; }
        public string OEMHoldReleasedUser { get; set; }
        public string OEMHoldReleasedDate { get; set; }
        public string OEMHoldReleasedTime { get; set; }
        public string CreditHoldReleasedUser { get; set; }
        public string CreditHoldReleasedDate { get; set; }
        public string CreditHoldReleasedTime { get; set; }
        public string TAXHoldReleasedDate { get; set; }
        public string TAXHoldReleasedTime { get; set; }
        public string PODCreatedDate { get; set; }
        public string PODCreatedTime { get; set; }
        public string PODUploadDate { get; set; }
        public string PODUploadTime { get; set; }
        public string CustomerReference { get; set; }
        public string ScanningStatus { get; set; }
        public string SBUDescription { get; set; }
        public string TaxHoldReleasedUser { get; set; }
        public string RecordNumber { get; set; }
        public string PageNumber { get; set; }
        public string TotalPage { get; set; }
        public string ShipToCity { get; set; }
    }

    
}
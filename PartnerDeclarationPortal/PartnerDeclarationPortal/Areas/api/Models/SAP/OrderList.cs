﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartnerDeclarationPortal.Areas.api.Models.SAP
{
    public class OrderHistory
    {
        public List<OrderDetails> OrderList { get; set; }
        public List<OrderDetails> ShippingList { get; set; }
    }

    public class OrderDetails
    {
        public string Customer_Code { get; set; }
        public string Customer_Name { get; set; }
        public string WREF99 { get; set; }
        public string Order_Number { get; set; }
        public string Order_Date { get; set; }
        public string Invoice_Number { get; set; }
        public string Invoice_Date { get; set; }
        public string Order_Status { get; set; }
        public string Shipping_Date { get; set; }
        public string Shipping_Time { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Payment_Method { get; set; }
        public string Order_Value { get; set; }
        public string Page { get; set; }
        public string TotalPage { get; set; }
        public string POD_Date { get; set; }
        public string POD_Available { get; set; }
        
    }
}
﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

//namespace PartnerDeclarationPortal.Areas.api.Models.SAP
//{
//    public class SAPOrder
//    {
//        public string sale_ordr { get; set; }
//        public string prod_code { get; set; }
//        public string mpn_num { get; set; }
//        public string ord_qty { get; set; }
//        public string unit_price { get; set; }
//        public string tax_price { get; set; }
//        public string tcs_price { get; set; }
//        public string frgt_price { get; set; }
//        public string total_price { get; set; }
//        public string ship_from { get; set; }
//        public string order_status { get; set; }
//        public string inv_num { get; set; }
//        public string inv_url { get; set; }
//        public string inv_qty { get; set; }

//    }

//    public class OrderInfo
//    {
//        public List<SAPOrder> Details { get; set; }

//        public string status_code { get; set; }
//        public string status_message { get; set; }

//        public string sld_cus { get; set; }
//        public string sld_name { get; set; }
//        public string sld_addr1 { get; set; }
//        public string sld_addr2 { get; set; }
//        public string sld_addr3 { get; set; }
//        public string sld_addr4 { get; set; }
//        public string sld_addr5 { get; set; }
//        public string sld_city { get; set; }
//        public string sld_state { get; set; }
//        public string sld_pin { get; set; }

//        public string dlv_cus { get; set; }
//        public string dlv_name { get; set; }
//        public string dlv_addr1 { get; set; }
//        public string dlv_addr2 { get; set; }
//        public string dlv_addr3 { get; set; }
//        public string dlv_addr4 { get; set; }
//        public string dlv_addr5 { get; set; }
//        public string dlv_city { get; set; }
//        public string dlv_state { get; set; }
//        public string dlv_pin { get; set; }

//        public string sale_value { get; set; }
//        public string tax_value { get; set; }
//        public string tcs_value { get; set; }
//        public string frgt_value { get; set; }
//        public string grnd_value { get; set; }
//        public string frgt_taxvalue { get; set; }

//    }
//}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RedApple.Areas.api.Models.SAP
{
    public class SAPOrder
    {
        public string OrderNumber { get; set; }
        public string WREF12 { get; set; }
        public string Product_Name { get; set; }
        public string Item_Code { get; set; }
        public string Item_Description { get; set; }
        public string Item_VendorCode { get; set; }
        public string Unit_Price { get; set; }
        public string Quantity { get; set; }
        public string Total_Linevalue { get; set; }
        public string LVAT12 { get; set; }
        public string SO_Date { get; set; }
        public string Invoice_Number { get; set; }
        public string Invoice_Date { get; set; }
        public string Status { get; set; }
        public string DITM12 { get; set; }
        public string DIDT12 { get; set; }
        public string DLTM12 { get; set; }
        public string DLDT12 { get; set; }
        public string User_ID { get; set; }
        public string Name { get; set; }
        public string Payment_Days { get; set; }
        public string payment_Method { get; set; }
        public string DeliveryAddress1 { get; set; }
        public string DeliveryAddress2 { get; set; }
        public string DeliveryAddress3 { get; set; }
        public string DeliveryAddress4 { get; set; }
        public string DeliveryAddress5 { get; set; }
        public string DeliveryAddress6 { get; set; }
        public string DeliveryAddress7 { get; set; }
        public string BillingAddress1 { get; set; }
        public string BillingAddress2 { get; set; }
        public string BillingAddress3 { get; set; }
        public string BillingAddress4 { get; set; }
        public string BillingAddress5 { get; set; }
        public string BillingAddress6 { get; set; }
        public string BillingAddress7 { get; set; }
    }

    public class OrderInfo
    {
        public List<SAPOrder> Data { get; set; }

        public string OrderNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceDate { get; set; }
        public string OrderTotal { get; set; }
        public string OrderDate { get; set; }

        public string DeliveryAddress1 { get; set; }
        public string DeliveryAddress2 { get; set; }
        public string DeliveryAddress3 { get; set; }
        public string DeliveryAddress4 { get; set; }
        public string DeliveryAddress5 { get; set; }
        public string DeliveryAddress6 { get; set; }
        public string DeliveryAddress7 { get; set; }

        public string BillingAddress1 { get; set; }
        public string BillingAddress2 { get; set; }
        public string BillingAddress3 { get; set; }
        public string BillingAddress4 { get; set; }
        public string BillingAddress5 { get; set; }
        public string BillingAddress6 { get; set; }
        public string BillingAddress7 { get; set; }

    }




}


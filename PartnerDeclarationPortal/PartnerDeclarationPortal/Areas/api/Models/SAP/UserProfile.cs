﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartnerDeclarationPortal.Areas.api.Models.SAP
{
    public class UserProfile
    {
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string ADDRESS_1 { get; set; }
        public string ADDRESS_2 { get; set; }
        public string ADDRESS_3 { get; set; }
        public string ADDRESS_4 { get; set; }
        public string ADDRESS_5 { get; set; }
        public string PHONE_NUMBER1 { get; set; }
        public string PHONE_NUMBER2 { get; set; }
        public string PHONE_NUMBER3 { get; set; }
        public string CONTACT_PERSON1 { get; set; }
        public string CONTACT_PERSON2 { get; set; }
        public string EmailAddress1 { get; set; }
        public string EmailAddress2 { get; set; }
        public string EmailAddress3 { get; set; }
        public string SALUTATION { get; set; }
        public string CompanyRegisterNumber { get; set; }
        public string ALPHA_SEQUENCE { get; set; }
        public string DATE_OPENED { get; set; }
        public string CRD_LMT_CHANGED_DATE { get; set; }
        public string HIERARCHY_TYPE { get; set; }
        public string HIER_STATEMENT { get; set; }
        public string POST_CODE { get; set; }
        public string FAX_NUMBER { get; set; }
        public string FAX_NUMBER_2 { get; set; }
        public string LANGUAGE { get; set; }
        public string SALESMAN { get; set; }
        public string REGION { get; set; }
        public string BAL_BF { get; set; }
        public string CATEGORY { get; set; }
        public string CREDITLIMITDAYS { get; set; }
        public string PAYMENTDAYS { get; set; }
        public string OVERDUE { get; set; }
        public string CREDIT_LIMIT { get; set; }
        public string LAST_INVOICE { get; set; }
        public string LAST_INVOICE_DATE { get; set; }
        public string LAST_INVOICE_VALUE { get; set; }
        public string HIGHEST_EXPOSURE { get; set; }
        public string HIGHST_EXPOSURE_DATE { get; set; }
        public string LAST_PAYMENT { get; set; }
        public string LAST_PAYMENT_DATE { get; set; }
        public string ImageURL { get; set; }
        public string NoOfCoversChq { get; set; }
    }

    public class Data
    {
        public string overall_orders { get; set; }
        public string awaiting_for_invoice { get; set; }
        public string awaiting_for_scanning { get; set; }
        public string awaiting_for_despatch { get; set; }
        public string shipment_in_transit { get; set; }
        public string shipment_delivered { get; set; }
        public string pod_available { get; set; }
    }

    public class Dashboarddetails
    {
        public Data Data { get; set; }
    }

}
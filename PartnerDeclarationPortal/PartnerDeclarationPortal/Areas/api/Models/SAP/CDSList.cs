﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartnerDeclarationPortal.Areas.api.Models.SAP
{
    public class CDSHistory
    {
        public List<CDSDetails> CDSList { get; set; }
    }

    public class CDSDetails
    {
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string DOCUMENT_NO { get; set; }
        public string DOCUMENT_TYPE { get; set; }
        public string DOCUMENT_DATE { get; set; }

        public string REFERENCE { get; set; }
        public string DUE_DATE { get; set; }
        public string DOCUMENT_BASE_AMOUNT { get; set; }
        public string TAX_AMOUNT { get; set; }
        public string DOCUMENT_AMOUNT { get; set; }
        public string OS_AMOUNT { get; set; }
        public string CUSTOMER_PROFILE { get; set; }
        
        public string Page { get; set; }
        public string TotalPage { get; set; }
    }
}
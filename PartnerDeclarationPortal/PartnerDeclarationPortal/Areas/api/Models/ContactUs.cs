﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartnerDeclarationPortal.Areas.api.Models
{
    public class ContactUs
    {
        public string name { get; set; }
        public string emailid { get; set; }
        public string phone { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public int pobox { get; set; }
        public string website { get; set; }
        public string type_of_query { get; set; }
        public string remarks { get; set; }
    }
}
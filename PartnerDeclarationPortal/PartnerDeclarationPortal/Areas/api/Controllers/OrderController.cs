﻿using Newtonsoft.Json;
using PartnerDeclarationPortal.Areas.api.Models;
using PartnerDeclarationPortal.Areas.api.Models.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PartnerDeclarationPortal.App_Code;
using System.Web;
using System.Globalization;
using System.Data;
using RedApple.Areas.api.Models.SAP;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Net.Mail;
using System.Configuration;
using System.Reflection;
using System.IO;
using OfficeOpenXml;

namespace PartnerDeclarationPortal.Areas.api.Controllers
{
    public class OrderController : ApiController
    {

        [HttpGet]
        [ActionName("GetProfile")]
        public AppResponse GetProfile(string CustomerCode)
        {
            var _Response = new AppResponse();
            var _SAPServices = new SAPServices();

            var _data = SAPServices.GET_JSON_RESPONSE("UserProfile?UserID=" + CustomerCode);
            var result = JsonConvert.DeserializeObject<UserProfile>(_data.Data[0].ToString());

            _Response.Data = result;

            return _Response;
        }

        [HttpGet]
        [ActionName("GetPendingCheques")]
        public JQGridResponse GetPendingCheques(string sidx, string sord, int page, int rows, string CustomerCode)
        {
            var _Response = new JQGridResponse();
            var _SAPServices = new SAPServices();

            var _data = SAPServices.GET_JSON_RESPONSE("GetChequePending?UserID=" + CustomerCode + "&RecordNo=" + rows + "&PageNo=" + page);
            List<PendingCheques> result = JsonConvert.DeserializeObject<List<PendingCheques>>(_data.Data.ToString());

            _Response.Data = result;
            _Response.PageNumber = page;

            //int ii = Convert.ToInt32(result.FirstOrDefault().TotalPage);
            _Response.TotalPages = result.Count > 0 ? Convert.ToInt32(result.FirstOrDefault().TotalPage) : 1;
            _Response.TotalRecords = result.Count > 0 ? rows * Convert.ToInt32(result.FirstOrDefault().TotalPage) : rows * 1;
          
            return _Response;
        }

        [HttpGet]
        public JQGridResponse DownloadPendingCheques(string CustomerCode)
        {
            var _Response = new JQGridResponse();
            var _SAPServices = new SAPServices();
            int rows = 2000;
            int page = 1;

            var _data = SAPServices.GET_JSON_RESPONSE("GetChequePending?UserID=" + CustomerCode + "&RecordNo=" + rows + "&PageNo=" + page);
            List<PendingCheques> result = JsonConvert.DeserializeObject<List<PendingCheques>>(_data.Data.ToString());

            _Response.Data = result;
            _Response.PageNumber = page;
            ListtoDataTable lsttodt = new ListtoDataTable();
            DataTable dt = lsttodt.ToDataTable(result);
            var rcount = dt.Rows.Count;
            var dtDownloadList = new DataTable();
            //Start
            if (rcount != 0)
            {
                dtDownloadList = dt;
                var _FilePath = HttpContext.Current.Server.MapPath("~/DOCS/PendingCheques_" + DateTime.Now.ToString("dd_MM_yyyy_hhmmss") + ".xlsx");
                string AppLocation = HttpContext.Current.Server.MapPath("~/DOCS/");


                foreach (string file in Directory.GetFiles(AppLocation, "*.xlsx").Where(item => item.EndsWith(".xlsx")))
                {
                    File.Delete(file);
                }

                if (File.Exists(_FilePath))
                {
                    File.Delete(_FilePath);
                }

                using (FileStream fs = new FileStream(_FilePath, FileMode.OpenOrCreate))
                {
                    using (ExcelPackage pck = new ExcelPackage(fs))
                    {
                        ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Pending Cheques");
                        ws.Cells["A1"].LoadFromDataTable(dtDownloadList, true);

                        ws.Row(1).Style.Font.Bold = true;


                        pck.Save();
                    }
                }

                _Response.Data = Url.Content("~/DOCS/" + Path.GetFileName(_FilePath));

            }

            return _Response;
        }
        
        [HttpGet]
        [ActionName("GetPaymentDues")]
        public AppResponse GetPaymentDues(string CustomerCode)
        {
            var _Response = new AppResponse();

            var _JSON = SAPServices.GET_JSON("PaymentDue?CustomerCode=" + CustomerCode + "&Week=");

            PaymentDue _PaymentDue = JsonConvert.DeserializeObject<PaymentDue>(_JSON);

            _Response.Data = _PaymentDue.Data.Header.FirstOrDefault();

            return _Response;
        }

        [HttpGet]
        [ActionName("GetPaymentDuesByWeek")]
        public JQGridResponse GetPaymentDuesByWeek(string sidx, string sord, int page, int rows, string CustomerCode, string Week)
        {
            var _Response = new JQGridResponse();
            var _SAPServices = new SAPServices();

            var _JSON = SAPServices.GET_JSON("PaymentDue?CustomerCode=" + CustomerCode + "&Week=");
            PaymentDue _PaymentDue = JsonConvert.DeserializeObject<PaymentDue>(_JSON);

            string week = (from p in _PaymentDue.Data.Detail select p.Week).FirstOrDefault();
            int weekvalue = week.Length>0?Convert.ToInt32(week.Substring(0, 1)):0;
            List<PendingCheques> result;

            if (Convert.ToInt32(Week) <= 4)
            {
                result = (from p in _PaymentDue.Data.Detail
                          where weekvalue == weekvalue
                          select new PendingCheques()
                          {
                              InvoiceNumber = p.InvoiceNumber,
                              InvoiceDate = p.InvoiceDate,
                              OutstandingAmount=p.OutstandingAmount,
                              InvoiceValue =Math.Round(double.Parse(p.InvoiceValue)),
                              DueDate = p.DueDate,
                              BalanceDueAmount = p.DueAmount.EndsWith("-")?"-"+Math.Round(double.Parse(p.DueAmount.Replace("-", ""))).ToString(): Math.Round(double.Parse(p.DueAmount)).ToString(),
                              OverDueDays = p.ODDays.Replace("-", ""),
                              PendingDays = p.ODDays.Replace("-", "")
                          }).ToList();
            }
            else
            {
                result = (from p in _PaymentDue.Data.Detail
                          where Convert.ToInt32(p.Week) >= Convert.ToInt32(Week)
                          select new PendingCheques()
                          {
                              InvoiceNumber = p.InvoiceNumber,
                              InvoiceDate = p.InvoiceDate,
                              InvoiceValue = Math.Round(double.Parse(p.InvoiceValue)),
                              DueDate = p.DueDate,
                              BalanceDueAmount = p.DueAmount.EndsWith("-") ? "-" + Math.Round(double.Parse(p.DueAmount.Replace("-", ""))).ToString() : Math.Round(double.Parse(p.DueAmount)).ToString(),
                              OverDueDays = p.ODDays.Replace("-", ""),
                              PendingDays = p.ODDays.Replace("-", "")
                          }).ToList();
            }

            _Response.Data = result;
            _Response.PageNumber = page;
            if (result.Count > 0)
            {
                _Response.TotalPages = Convert.ToInt32(result.FirstOrDefault().TotalPage);
                _Response.TotalRecords = rows * Convert.ToInt32(result.FirstOrDefault().TotalPage);
            }
            else
            {
                _Response.TotalPages = 0;
                _Response.TotalRecords = 0;
            }

            return _Response;
        }

        [HttpGet]
        public JQGridResponse DownloadPaymentDuesWeekNew(string CustomerCode)
        {
            var _Response = new JQGridResponse();
            var _SAPServices = new SAPServices();
            int rows = 2000;
            int page = 1;

            var _JSON = SAPServices.GET_JSON("PaymentDue?CustomerCode=" + CustomerCode + "&Week=");
            PaymentDue _PaymentDue = JsonConvert.DeserializeObject<PaymentDue>(_JSON);

            _Response.Data = _PaymentDue.Data.Detail;
            _Response.PageNumber = page;
            ListtoDataTable lsttodt = new ListtoDataTable();
            DataTable dt = lsttodt.ToDataTable(_PaymentDue.Data.Detail);
            var rcount = dt.Rows.Count;
            var dtDownloadList = new DataTable();
            //Start
            if (rcount != 0)
            {
                dtDownloadList = dt;
                var _FilePath = HttpContext.Current.Server.MapPath("~/DOCS/PaymentDueOutstand_" + DateTime.Now.ToString("dd_MM_yyyy_hhmmss") + ".xlsx");
                string AppLocation = HttpContext.Current.Server.MapPath("~/DOCS/");


                foreach (string file in Directory.GetFiles(AppLocation, "*.xlsx").Where(item => item.EndsWith(".xlsx")))
                {
                    File.Delete(file);
                }

                if (File.Exists(_FilePath))
                {
                    File.Delete(_FilePath);
                }

                using (FileStream fs = new FileStream(_FilePath, FileMode.OpenOrCreate))
                {
                    using (ExcelPackage pck = new ExcelPackage(fs))
                    {
                        ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Payment Due Outstanding");
                        ws.Cells["A1"].LoadFromDataTable(dtDownloadList, true);

                        ws.Row(1).Style.Font.Bold = true;


                        pck.Save();
                    }
                }

                _Response.Data = Url.Content("~/DOCS/" + Path.GetFileName(_FilePath));

            }

            return _Response;
        }

        [HttpGet]
        [ActionName("GetPaymentDuesByWeekNew")]
        public JQGridResponse GetPaymentDuesByWeekNew(string sidx, string sord, int page, int rows, string CustomerCode, string Week)
        {
            var _Response = new JQGridResponse();
            var _SAPServices = new SAPServices();
            //CustomerCode = "E10027";
            var _JSON = SAPServices.GET_JSON("PaymentDue?CustomerCode=" + CustomerCode + "&Week=");
            PaymentDue _PaymentDue = JsonConvert.DeserializeObject<PaymentDue>(_JSON);
            string[] date;

            DateTime newdate;

            foreach (var item in _PaymentDue.Data.Detail)
            {
                //date = item.DueDate.Split('/');
                //string date1 = date[1] + "/" + date[0] + "/" + date[2];
                newdate = Convert.ToDateTime(item.DueDate);

                item.newDuedate = newdate;
            }
            List<PendingCheques> result;
            //if (Convert.ToInt32(Week) <= 4)
            //{
            result = (from p in _PaymentDue.Data.Detail
                      where Convert.ToDateTime(p.newDuedate) <= DateTime.Today
                      select new PendingCheques()
                      {
                          InvoiceNumber = p.InvoiceNumber,
                          InvoiceDate = p.InvoiceDate,
                          InvoiceValue = Math.Round(double.Parse(p.InvoiceValue)),
                          DueDate = p.DueDate,
                          BalanceDueAmount = p.DueAmount,
                          PendingDays = p.ODDays.Replace("-",""),
                          OverDueDays = p.ODDays.Replace("-", ""),                       
                          active = false
                      }).ToList();
            

            _Response.Data = result;
            _Response.PageNumber = page;
            if (result.Count > 0)
            {
                _Response.TotalPages = Convert.ToInt32(result.FirstOrDefault().TotalPage);
                _Response.TotalRecords = rows * Convert.ToInt32(result.FirstOrDefault().TotalPage);
            }
            else
            {
                _Response.TotalPages = 0;
                _Response.TotalRecords = 0;
            }

            return _Response;
        }

        [HttpGet]
        public JQGridResponse DownloadPaymentDuesWeekOutstanding(string CustomerCode)
        {
            var _Response = new JQGridResponse();
            var _SAPServices = new SAPServices();
            int rows = 2000;
            int page = 1;

            var _JSON = SAPServices.GET_JSON("PaymentDue?CustomerCode=" + CustomerCode + "&Week=");
            PaymentDue _PaymentDue = JsonConvert.DeserializeObject<PaymentDue>(_JSON);

            _Response.Data = _PaymentDue.Data.Detail;
            _Response.PageNumber = page;
            ListtoDataTable lsttodt = new ListtoDataTable();
            DataTable dt = lsttodt.ToDataTable(_PaymentDue.Data.Detail);
            var rcount = dt.Rows.Count;
            var dtDownloadList = new DataTable();
            //Start
            if (rcount != 0)
            {
                dtDownloadList = dt;
                var _FilePath = HttpContext.Current.Server.MapPath("~/DOCS/PaymentDueOutstand_" + DateTime.Now.ToString("dd_MM_yyyy_hhmmss") + ".xlsx");
                string AppLocation = HttpContext.Current.Server.MapPath("~/DOCS/");


                foreach (string file in Directory.GetFiles(AppLocation, "*.xlsx").Where(item => item.EndsWith(".xlsx")))
                {
                    File.Delete(file);
                }

                if (File.Exists(_FilePath))
                {
                    File.Delete(_FilePath);
                }

                using (FileStream fs = new FileStream(_FilePath, FileMode.OpenOrCreate))
                {
                    using (ExcelPackage pck = new ExcelPackage(fs))
                    {
                        ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Payment Due Outstanding");
                        ws.Cells["A1"].LoadFromDataTable(dtDownloadList, true);

                        ws.Row(1).Style.Font.Bold = true;


                        pck.Save();
                    }
                }

                _Response.Data = Url.Content("~/DOCS/" + Path.GetFileName(_FilePath));

            }

            return _Response;
        }


        [ActionName("GetOrderHistory")]
        [HttpGet]
        public JQGridResponse GetOrderHistory(string sidx, string sord, int page, int rows, string dateFrom, string dateTill, string CustomerCode)
        {
            var _Response = new JQGridResponse();

            var _DateFrom = "";
            var _DateTill = "";

            if (!string.IsNullOrEmpty(dateFrom) && TryParseDate(dateFrom))
            {
                _DateFrom = DateTime.ParseExact(dateFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
            }
            else
            {
                _DateFrom = DateTime.Now.AddDays(-7).ToString("yyyyMMdd");
            }

            if (!string.IsNullOrEmpty(dateTill) && TryParseDate(dateTill))
            {
                _DateTill = DateTime.ParseExact(dateTill, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
            }
            else
            {
                _DateTill = DateTime.Now.ToString("yyyyMMdd");
            }

            var _Request = new
            {
                User_ID = CustomerCode,
                Order_Number = "",
                FromDate = _DateFrom,
                ToDate = _DateTill,
                RecordNo = rows,
                PageNo = page
            };

            var _data = SAPServices.POST_JSON("GetOrdersHistory", JsonConvert.SerializeObject(_Request),"");
            OrderHistory result = new OrderHistory();

            if (!string.IsNullOrEmpty(_data))
            {
                result = JsonConvert.DeserializeObject<OrderHistory>(JsonConvert.DeserializeObject(_data).Data.ToString());
            }

            var _orderList = new List<OrderDetails>();
            if (result.ShippingList != null && result.ShippingList.Count > 0)
            {
                _orderList.AddRange(result.ShippingList);
            }
            if (result.OrderList != null && result.OrderList.Count > 0)
            {
                _orderList.AddRange(result.OrderList);
            }

            #region Check Physical Invoice existance

            var _InvoiceNumbers = new List<string>();

            foreach (var _order in _orderList.ToList())
            {
                if (!string.IsNullOrEmpty(_order.Invoice_Number))
                {
                    _InvoiceNumbers.Add(_order.Invoice_Number);
                }
            }

            var _SQL = "SELECT VBELN,PRINT_INV,RECEIVED FROM SAPSR3.ZSD_INV_PRINT WHERE PRINT_INV='X' AND RECEIVED='X' " +
                "AND VBELN IN('" + string.Join("','", _InvoiceNumbers.ToArray()) + "')";
            SAPServices _SAPService = new SAPServices();
            _SAPService.CommandText = _SQL;
            _SAPService.CommandType = SAPServices.SAPQueryType.PlainSQL;
            DataTable dtInvoiceStatus = SAPServices.HanaService(_SQL);//_SAPService.ExecuteDataTable();

            foreach (DataRow dr in dtInvoiceStatus.Rows)
            {
                _orderList.ToList().Find(o => o.Invoice_Number == dr["VBELN"].ToString()).Invoice_Number = dr["VBELN"].ToString() + "*";
            }

            #endregion

            _Response.Data = _orderList;
            _Response.PageNumber = page;
            _Response.TotalRecords = _orderList.Count > 0 ? Convert.ToInt32(_orderList.FirstOrDefault().TotalPage) * rows : 0;
            _Response.TotalPages = _orderList.Count > 0 ? Convert.ToInt32(_orderList.FirstOrDefault().TotalPage) : 0;

            return _Response;
        }

         [HttpGet]
        public JQGridResponse DownloadSerialNumbersForCustomer(string dateFrom, string dateTill, string CustomerCode)
        {
            var _Response = new JQGridResponse();

            var _DateFrom = "";
            var _DateTill = "";

            if (!string.IsNullOrEmpty(dateFrom) && TryParseDate(dateFrom))
            {
                _DateFrom = DateTime.ParseExact(dateFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
            }
            else
            {
                _DateFrom = DateTime.Now.AddDays(-30).ToString("yyyyMMdd");
            }

            if (!string.IsNullOrEmpty(dateTill) && TryParseDate(dateTill))
            {
                _DateTill = DateTime.ParseExact(dateTill, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
            }
            else
            {
                _DateTill = DateTime.Now.ToString("yyyyMMdd");
            }

            var _Request = new
            {
                User_ID = CustomerCode,
                Order_Number = "",
                FromDate = _DateFrom,
                ToDate = _DateTill,
                RecordNo =  5000,
                PageNo = 1
            };

            var _data = SAPServices.POST_JSON("GetOrdersHistory", JsonConvert.SerializeObject(_Request), "");
            OrderHistory result = new OrderHistory();

            if (!string.IsNullOrEmpty(_data))
            {
                result = JsonConvert.DeserializeObject<OrderHistory>(JsonConvert.DeserializeObject(_data).Data.ToString());
            }

            var _orderList = new List<OrderDetails>();
            if (result.ShippingList != null && result.ShippingList.Count > 0)
            {
                _orderList.AddRange(result.ShippingList);
            }
            if (result.OrderList != null && result.OrderList.Count > 0)
            {
                _orderList.AddRange(result.OrderList);
            }

            #region Check Physical Invoice existance

            var _InvoiceNumbers = new List<string>();

            foreach (var _order in _orderList.ToList())
            {
                if (!string.IsNullOrEmpty(_order.Invoice_Number))
                {
                    _InvoiceNumbers.Add(_order.Invoice_Number);
                }
            }

            var _SQL = "SELECT VBELN,PRINT_INV,RECEIVED FROM SAPSR3.ZSD_INV_PRINT WHERE PRINT_INV='X' AND RECEIVED='X' " +
                "AND VBELN IN('" + string.Join("','", _InvoiceNumbers.ToArray()) + "')";
            SAPServices _SAPService = new SAPServices();
            _SAPService.CommandText = _SQL;
            _SAPService.CommandType = SAPServices.SAPQueryType.PlainSQL;
            DataTable dtInvoiceStatus = SAPServices.HanaService(_SQL);//_SAPService.ExecuteDataTable();

            foreach (DataRow dr in dtInvoiceStatus.Rows)
            {
                _orderList.ToList().Find(o => o.Invoice_Number == dr["VBELN"].ToString()).Invoice_Number = dr["VBELN"].ToString() + "*";
            }
            var dtDownloadList = new DataTable();
            #endregion
            ListtoDataTable lsttodt = new ListtoDataTable();
            DataTable dt = lsttodt.ToDataTable(_orderList);
            _Response.Data = _orderList;

          var rcount = dt.Rows.Count;
            //Start
            if (rcount != 0)
            {
                dtDownloadList = dt;
                var _FilePath = HttpContext.Current.Server.MapPath("~/DOCS/OrderHistory_" + DateTime.Now.ToString("dd_MM_yyyy_hhmmss") + ".xlsx");
                string AppLocation = HttpContext.Current.Server.MapPath("~/DOCS/");


                foreach (string file in Directory.GetFiles(AppLocation, "*.xlsx").Where(item => item.EndsWith(".xlsx")))
                {
                    File.Delete(file);
                }
                
                if (File.Exists(_FilePath))
                {
                    File.Delete(_FilePath);
                }

                using (FileStream fs = new FileStream(_FilePath, FileMode.OpenOrCreate))
                {
                    using (ExcelPackage pck = new ExcelPackage(fs))
                    {
                        ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Order Details");
                        ws.Cells["A1"].LoadFromDataTable(dtDownloadList, true);

                        ws.Row(1).Style.Font.Bold = true;


                        pck.Save();
                    }
                }

                _Response.Data = Url.Content("~/DOCS/" + Path.GetFileName(_FilePath));

            }
                    
            return _Response;
        }
        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }
    
    [HttpGet]
        [ActionName("GetCDS")]
        public AppResponse GetCDS(string CustomerCode)
        {
            var _Response = new AppResponse();
            var _SAPServices = new SAPServices();

            var dt = SAPServices.HanaService("select * from SAPSR3.ZCDS_ZFI_TDS('S10003','20190101','20220101')");

            //var _data = SAPServices.GET_JSON_RESPONSE("GetCDS?UserID=" + CustomerCode);
            //var result = JsonConvert.DeserializeObject<CDSHistory>(_data.Data[0].ToString());

            _Response.Data = dt.ToString();

            return _Response;
        }

        [ActionName("GetOrderDetails")]
        [HttpGet]
        public AppResponseAPI GetOrderDetails(string OrderNumber)
        {
            var _Response = new AppResponseAPI();

            var _Request = new
            {
                User_ID = "",
                Order_Number = OrderNumber,
                FromDate = "",
                ToDate = "",
                RecordNo = "",
                PageNo = ""
            };

            var _data = SAPServices.POST_JSON("GetOrders", JsonConvert.SerializeObject(_Request));
            OrderInfo result = JsonConvert.DeserializeObject<OrderInfo>(_data);

            if (result.Data.Count > 0)
            {
                result.BillingAddress1 = result.Data.FirstOrDefault().BillingAddress1;
                result.BillingAddress2 = result.Data.FirstOrDefault().BillingAddress2;
                result.BillingAddress3 = result.Data.FirstOrDefault().BillingAddress3;
                result.BillingAddress4 = result.Data.FirstOrDefault().BillingAddress4;
                result.BillingAddress5 = result.Data.FirstOrDefault().BillingAddress5;
                result.BillingAddress6 = result.Data.FirstOrDefault().BillingAddress6;
                result.BillingAddress7 = result.Data.FirstOrDefault().BillingAddress7;

                result.DeliveryAddress1 = result.Data.FirstOrDefault().DeliveryAddress1;
                result.DeliveryAddress2 = result.Data.FirstOrDefault().DeliveryAddress2;
                result.DeliveryAddress3 = result.Data.FirstOrDefault().DeliveryAddress3;
                result.DeliveryAddress4 = result.Data.FirstOrDefault().DeliveryAddress4;
                result.DeliveryAddress5 = result.Data.FirstOrDefault().DeliveryAddress5;
                result.DeliveryAddress6 = result.Data.FirstOrDefault().DeliveryAddress6;
                result.DeliveryAddress7 = result.Data.FirstOrDefault().DeliveryAddress7;

                result.OrderNumber = result.Data.FirstOrDefault().OrderNumber;
                result.OrderDate = result.Data.FirstOrDefault().SO_Date;
                result.InvoiceNumber = result.Data.FirstOrDefault().Invoice_Number;
                result.InvoiceDate = result.Data.FirstOrDefault().Invoice_Date;

                result.OrderTotal = (result.Data.Sum(o => Convert.ToSingle(o.Total_Linevalue)) + result.Data.Sum(o => Convert.ToSingle(o.LVAT12))).ToString();
            }

            _Response.Data = result;

            return _Response;
        }

        [HttpGet]
        [ActionName("GetAddressSequences")]
        public AppResponse GetAddressSequences(string CustomerCode)
        {
            var _Response = new AppResponse();

            try
            {
                //if (HttpContext.Current.Session["AddressList"] == null)
                //{
                    var result = SAPServices.GET_ADDRESS("GetDeliverySequence?UserID=" + CustomerCode);

                    var listAddress = (from DataRow dr in result.Rows
                                       select new AddressSeq()
                                       {
                                           Address1 = dr["Address1"].ToString(),
                                           Address2 = dr["Address2"].ToString(),
                                           Address3 = dr["Address3"].ToString(),
                                           Address4 = dr["Address4"].ToString(),
                                           Address5 = dr["Address5"].ToString(),
                                           Customer_Code = dr["Customer_Code"].ToString(),
                                           Customer_Name = dr["Customer_Name"].ToString(),
                                           Delivery_Sequence = dr["Delivery_Sequence"].ToString(),
                                           Location_Code = dr["Location_Code"].ToString(),
                                           Payment_Days = dr["Payment_Days"].ToString(),
                                           Payment_Method = dr["Payment_Method"].ToString(),
                                           PIN_Code = dr["PIN_Code"].ToString(),
                                       }).ToList();

                    listAddress.Find(a => a.Delivery_Sequence == "AAA").Delivery_Sequence = "EX-Warehouse";
                    HttpContext.Current.Session["AddressList"] = listAddress;
                    _Response.Data = listAddress;
                //}
                //else
                //{
                //    _Response.Data = (List<AddressSeq>)HttpContext.Current.Session["AddressList"];
                //}
                _Response.Status = "Success";
                _Response.Message = "Item added to cart successfully!";
            }
            catch (Exception ex)
            {
                _Response.Status = "Failed";
                _Response.ErrorMessage = ex.Message;
            }

            return _Response;
        }

        private bool TryParseDate(string _Date)
        {
            try
            {
                var _DateVal = DateTime.ParseExact(_Date, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        [HttpPost]
        [ActionName("SendOTP")]
        public AppResponse SendOTP(string arr)
        {
            var _Response = new AppResponse();
            var res = "";
            LOG.Info("SendOTP - START -" + arr);
            var _dbContext = new DBContext();

            try
            {
                var sts = CheckMail(arr);
                if (sts.Status == "Success")
                {
                    var _otp = GenerateOTP();
                    if (SendMailToPartner(arr, _otp, ""))
                    {
                        var _PartnerOTP = new User();
                        _PartnerOTP = _dbContext.InsertOTP(arr, _otp);
                        _Response.Status = "Success";
                        LOG.Info("SendOTP - SUCCESS -" + arr);
                    }
                    else
                    {
                        res = "Error";
                        LOG.Info("SendOTP - ERROR -" + arr);
                    }
                }
                else
                {
                    _Response = sts;
                }


            }
            catch (Exception ex)
            {
                LOG.Info("SendOTP - ERROR -" + ex.Message);
                res = ex.Message.ToString();
            }
            return _Response;
        }

        public AppResponse CheckMail(string arr)
        {
            var _Response = new AppResponse();
            var _dbContext = new DBContext();
            var res = "";
            LOG.Info("CheckMail - START -" + arr);
            try
            {
                var auth_users = ConfigurationManager.AppSettings["LOCAL_USERS"].ToString();
                int pos = Array.IndexOf(auth_users.Split(','), arr.Trim().ToUpper());
                if (pos > -1)
                {

                    _Response.Status = "success";
                    _Response.Data = Url.Content("~/Partner/Profile");
                }



                var dt = new DataTable();
                dt = _dbContext.CheckEmailSAP(arr.Trim().ToUpper());//CheckEmail(arr);
                if (dt.Rows.Count > 0)
                {
                    if (arr.Trim().ToUpper() == dt.Rows[0]["MAILID_TO"].ToString().ToUpper())
                    {
                        res = "success";
                        LOG.Info("SendOTP - SUCCESS -" + arr);
                    }
                    else
                    {
                        res = "Only allowed with an email id which got this communication.";
                        LOG.Info("CheckMail - ERROR -" + arr + res);
                    }

                }
                else
                {
                    res = "Only allowed with an email id which got this communication.";
                    LOG.Info("CheckMail - ERROR -" + arr + res);
                }
            }
            catch (Exception ex)
            {
                //_Response = ex.Message.ToString();
                LOG.Exception(ex);

            }
            return _Response;
        }

        [HttpPost]
        [ActionName("SendOTP")]
        public static string ValidateOTP(string email, string OTP)
        {
            var res = "";
            LOG.Info("ValidateOTP - START -" + email);

            try
            {
                if (!checkForSQLInjection(OTP))
                {
                    var dt = new DataTable();
                    dt = GetOTPData(email);
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["Otp"].ToString() == OTP)
                        {
                            if (checkOTPTime(dt.Rows[0]["Created_On"].ToString()) == true)
                            {
                                res = "success";

                                //foreach (DataRow dr in dt.Rows)// 
                                //{
                                //    if (!string.IsNullOrEmpty(dr["ApprovedBy"].ToString()) && dr["ApprovedBy"].ToString() != "")
                                //    {
                                //        res = "signedcopyupload";
                                //    }
                                //}
                                LOG.Info("ValidateOTP - SUCCESS -" + email);
                                HttpContext.Current.Session["IP"] = dt.Rows[0]["IP"].ToString();
                                HttpContext.Current.Session["Email"] = dt.Rows[0]["Email"].ToString();
                            }
                            else
                            {
                                res = "OTP expierd, try again.";
                            }
                        }
                        else
                        {
                            res = "Invalid OTP.";
                        }
                    }
                    else
                    {
                        res = "Please try again.";
                    }
                }
                else
                {
                    res = "Error";
                    LOG.Info("ValidateOTP - ERROR -" + email);
                }
            }
            catch (Exception ex)
            {
                LOG.Exception(ex);
                res = ex.Message.ToString();
            }
            return res;
        }
        public static string GenerateOTP()
        {

            char[] chars = new char[10];
            chars = "1234567890".ToCharArray();
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            data = new byte[6];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(8);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }


            return result.ToString();

        }

        public static bool SendMailToPartner(string MailID, string OTP, string Partner)
        {
            var _result = true;

            try
            {

                var _filePath = @"C:\Users\ramkumar\Desktop\PartnerDeclarationPortal\PartnerDeclarationPortal\Assets\mail-templates\OTP.html";//System.Configuration.ConfigurationManager.AppSettings["dbCon"];
                //var _filePath = @"C:\inetpub\wwwroot\PartnerDeclaration\mail-templates\OTP.html";//System.Configuration.ConfigurationManager.AppSettings["dbCon"];
                var _MailContent = System.IO.File.ReadAllText(_filePath);

                _MailContent = _MailContent.Replace("{OTP}", OTP);


                using (SmtpClient smtp = new SmtpClient(ConfigurationManager.AppSettings["SMTP_SERVER"]))
                {
                    smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTP_USER"], ConfigurationManager.AppSettings["SMTP_PASS"]);

                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress("Redington India <" + ConfigurationManager.AppSettings["FROM_EMAIL"] + ">");

                        mail.To.Add(MailID);

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailCC"]))
                        {
                            mail.CC.Add(ConfigurationManager.AppSettings["EmailCC"].ToString());
                        }

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailBCC"]))
                        {
                            mail.Bcc.Add(ConfigurationManager.AppSettings["EmailBCC"].ToString());
                        }

                        mail.Subject = "Re: Redington Credit Automation - OTP";

                        mail.Body = _MailContent;
                        mail.IsBodyHtml = true;

                        System.Net.Mime.ContentType mimeType = new System.Net.Mime.ContentType("text/html");
                        AlternateView alternate = AlternateView.CreateAlternateViewFromString(_MailContent, mimeType);
                        mail.AlternateViews.Add(alternate);

                        smtp.Send(mail);
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Exception(ex);
                _result = false;
            }

            return _result;

        }

        public static DataTable GetOTPData(string email)
        {
            LOG.Info("GetOTPData - START -" + email);
            var res = "";
            SqlConnection sCon = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["dbCon"]);
            SqlCommand sCmd;
            DataTable ds = new DataTable();
            SqlDataAdapter sDA = new SqlDataAdapter();
            sCmd = new SqlCommand("SP_GetOTPData", sCon);
            sCmd.CommandType = CommandType.StoredProcedure;
            sCmd.Parameters.Add(new SqlParameter("@Email",email));
            try
            {
                sCon.Open();
                sDA = new SqlDataAdapter(sCmd);
                sDA.Fill(ds);
                LOG.Info("GetOTPData - Success -" + email);
            }
            catch (Exception ee)
            {
                res = ee.Message.ToString();
                LOG.Info("GetOTPData - Error -" + email);
            }
            finally
            {
                sCon.Close();
            }
            return ds;
        }
        public static bool checkOTPTime(string OTPCreated)
        {

            var start = DateTime.Now;
            var oldDate = DateTime.Parse(OTPCreated);

            if ((start - oldDate).TotalMinutes >= 3)
            {
                return false;
            }
            else
            {
                return true;

            }
        }

        [HttpPost]
        [ActionName("GET_Dashboard")]
        public AppResponseAPI GET_Dashboard(string CustomerCode,string _Fromdate,string _todate, string tag)
        { 
            string FromDate = _Fromdate; string Todate= _todate;
          
            var _DateFrom = "";
            var _DateTill = "";
            var _Response = new AppResponseAPI();
            if (!string.IsNullOrEmpty(FromDate) && TryParseDate(FromDate))
            {
                _DateFrom = DateTime.ParseExact(FromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
            }
            else
            {
                _DateFrom = DateTime.Now.AddDays(-7).ToString("yyyyMMdd");
            }

            if (!string.IsNullOrEmpty(Todate) && TryParseDate(Todate))
            {
                _DateTill = DateTime.ParseExact(Todate, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
            }
            else
            {
                _DateTill = DateTime.Now.ToString("yyyyMMdd");
            }
            if (tag == "profile")
            {
                var _Request = new
                {
                    CustomerCode = CustomerCode,
                    SO_Created_Date_From = _DateFrom,
                    SO_Created_Date_To = _DateTill
                };
                var _JSON = SAPServices.PostWhatsUP_JSON("PartnerSummary", JsonConvert.SerializeObject(_Request));
                Dashboarddetails Dashboarlist = JsonConvert.DeserializeObject<Dashboarddetails>(_JSON);
                _Response.Data = Dashboarlist.Data;

                return _Response;
            }
            else
            {
                var _Request = new
                {
                    CustomerCode = "S10003",
                    SO_Created_Date_From = "20210801",
                    SO_Created_Date_To = "20220805",
                    Option = "overall_orders",
                    RecordNo = "1",
                    PageNo = "20",
                    Sort = "1"
                };
                var _JSON = SAPServices.PostWhatsUP_JSON("PartnerSummary", JsonConvert.SerializeObject(_Request));
                Dashboarddetails Dashboarlist = JsonConvert.DeserializeObject<Dashboarddetails>(_JSON);
                _Response.Data = Dashboarlist.Data;

                return _Response;
            }

          
     
       
        }

        [HttpGet]
        [ActionName("GetDashboardDetails")]
        public JQGridResponse GetDashboardDetails(string CustomerCode, string _Fromdate, string _todate, string tag)
        {
            string FromDate = _Fromdate; string Todate = _todate;

            var _DateFrom = "";
            var _DateTill = "";
            if (!string.IsNullOrEmpty(FromDate) && TryParseDate(FromDate))
            {
                _DateFrom = DateTime.ParseExact(FromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
            }
            else
            {
                _DateFrom = DateTime.Now.AddDays(-7).ToString("yyyyMMdd");
            }

            if (!string.IsNullOrEmpty(Todate) && TryParseDate(Todate))
            {
                _DateTill = DateTime.ParseExact(Todate, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
            }
            else
            {
                _DateTill = DateTime.Now.ToString("yyyyMMdd");
            }
            var _Response = new JQGridResponse();
            var _SAPServices = new SAPServices();
            var _Request = new
            {
                CustomerCode = CustomerCode,
                SO_Created_Date_From = _DateFrom,
                SO_Created_Date_To = _DateTill,
                Option = tag,
                RecordNo = "180",
                PageNo = "20",
                Sort = "1"
            };
            try
            {
                var _JSON = SAPServices.PostWhatsUP_JSON("PartnerEnquiry", JsonConvert.SerializeObject(_Request));
                /* var _JSON = SAPServices.GET_JSON_Whatsup("PartnerSummary?CustomerCode=" + CustomerCode + "&SO_Created_Date_From="+ _DateFrom + "&SO_Created_Date_To="+ _DateTill + "&Tag="+ tag);
                 PartEnquiery _PaymentDue = JsonConvert.DeserializeObject<PartEnquiery>(_JSON);*/

                PartEnquiery _PaymentDue = JsonConvert.DeserializeObject<PartEnquiery>(_JSON);
                List<ParterEnquiry> result;

                result = (from p in _PaymentDue.Data

                          select new ParterEnquiry()
                          {
                              InvoiceNo = p.InvoiceNo,
                              InvoiceDate = (p.InvoiceDate.Substring(0, 4) + '/' + p.InvoiceDate.Substring(4, 2) + '/' + p.InvoiceDate.Substring(6, 2)),
                              MaterialGroupDesc = p.MaterialGroupDesc,
                              BilledQuantity =Math.Round(double.Parse(p.BilledQuantity)),
                              NetValue = Math.Round(double.Parse(p.NetValue)),
                              
                                 CustomerReference =p.CustomerReference,
                              ShipToCity= p.ShipToCity


                          }).ToList();


                _Response.Data = result;




                
            }
            catch (Exception e)
            {
                _Response.TotalPages =0;
            }
            return _Response;
        }

        [HttpGet]
        public JQGridResponse DownloadSerialNumberForParter(string dateFrom, string dateTill, string CustomerCode, string tag)
        {
            string FromDate = dateFrom; string Todate = dateTill;

            var _DateFrom = "";
            var _DateTill = "";
            if (!string.IsNullOrEmpty(FromDate) && TryParseDate(FromDate))
            {
                _DateFrom = DateTime.ParseExact(FromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
            }
            else
            {
                _DateFrom = DateTime.Now.AddDays(-7).ToString("yyyyMMdd");
            }

            if (!string.IsNullOrEmpty(Todate) && TryParseDate(Todate))
            {
                _DateTill = DateTime.ParseExact(Todate, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
            }
            else
            {
                _DateTill = DateTime.Now.ToString("yyyyMMdd");
            }
            var _Response = new JQGridResponse();
            var _SAPServices = new SAPServices();
            var _Request = new
            {
                CustomerCode = CustomerCode,
                SO_Created_Date_From = _DateFrom,
                SO_Created_Date_To = _DateTill,
                Option = tag,
                RecordNo = "180",
                PageNo = "20",
                Sort = "1"
            };

            var _data = SAPServices.PostWhatsUP_JSON("PartnerEnquiry", JsonConvert.SerializeObject(_Request));

            PartEnquierHistory result = new PartEnquierHistory();
            

            if (!string.IsNullOrEmpty(_data))
            {
                result = JsonConvert.DeserializeObject<PartEnquierHistory>(_data);
                
            }

            var _datumList = new List<PartnerDetails>();
            if (result != null && result.Data.Count > 0)
            {
                
                _datumList.AddRange(result.Data);
            }

            

            var _orderList = new List<OrderDetails>();
            #region Check Physical Invoice existance

            var _InvoiceNumbers = new List<string>();

            foreach (var _order in _datumList.ToList())
            {
                _InvoiceNumbers.Add(_order.InvoiceNo);
            }

            var _SQL = "SELECT VBELN,PRINT_INV,RECEIVED FROM SAPSR3.ZSD_INV_PRINT WHERE PRINT_INV='X' AND RECEIVED='X' " +
                "AND VBELN IN('" + string.Join("','", _InvoiceNumbers.ToArray()) + "')";
            SAPServices _SAPService = new SAPServices();
            _SAPService.CommandText = _SQL;
            _SAPService.CommandType = SAPServices.SAPQueryType.PlainSQL;
            DataTable dtInvoiceStatus = SAPServices.HanaService(_SQL);//_SAPService.ExecuteDataTable();

            foreach (DataRow dr in dtInvoiceStatus.Rows)
            {
                _orderList.ToList().Find(o => o.Invoice_Number == dr["VBELN"].ToString()).Invoice_Number = dr["VBELN"].ToString() + "*";
            }
            var dtDownloadList = new DataTable();
            #endregion

            ListtoDataTable lsttodt = new ListtoDataTable();
            DataTable dt = lsttodt.ToDataTable(_orderList);
            _Response.Data = _orderList;
            var rcount = dt.Rows.Count;
            //Start
            if (rcount != 0)
            {
                dtDownloadList = dt;
                var _FilePath = HttpContext.Current.Server.MapPath("~/DOCS/PartnerEnquiry_" + DateTime.Now.ToString("dd_MM_yyyy_hhmmss") + ".xlsx");
                string AppLocation = HttpContext.Current.Server.MapPath("~/DOCS/");


                foreach (string file in Directory.GetFiles(AppLocation, "*.xlsx").Where(item => item.EndsWith(".xlsx")))
                {
                    File.Delete(file);
                }

                if (File.Exists(_FilePath))
                {
                    File.Delete(_FilePath);
                }

                using (FileStream fs = new FileStream(_FilePath, FileMode.OpenOrCreate))
                {
                    using (ExcelPackage pck = new ExcelPackage(fs))
                    {
                        ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Partner Enquiry Details");
                        ws.Cells["A1"].LoadFromDataTable(dtDownloadList, true);

                        ws.Row(1).Style.Font.Bold = true;


                        pck.Save();
                    }
                }

                _Response.Data = Url.Content("~/DOCS/" + Path.GetFileName(_FilePath));

            }

            return _Response;
        }

        public static bool checkForSQLInjection(string userInput)
        {

            bool isSQLInjection = false;

            string[] sqlCheckList = { "--", ";--", ";",  "/*", "*/", "@@","char","nchar","varchar", "nvarchar", "alter", "begin","cast",

                                       "create", "cursor","declare", "delete", "drop","end","exec","execute", "fetch","insert", "kill",

                                             "select", "sys","sysobjects", "syscolumns","table","update"

                                       };

            string CheckString = userInput.Replace("'", "''");

            for (int i = 0; i <= sqlCheckList.Length - 1; i++)
            {

                if ((CheckString.IndexOf(sqlCheckList[i],

    StringComparison.OrdinalIgnoreCase) >= 0))

                {
                    isSQLInjection = true;
                }
            }
            return isSQLInjection;
        }
    }

    public class Datum
    {
        public string InvoiceNumber { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoiceValue { get; set; }
        public string DueDate { get; set; }
        public string BalanceDueAmount { get; set; }
        public string OverDueDays { get; set; }
        public string PendingDays { get; set; }



    }


    public class CFRequest
    {
        public Authentication authentication { get; set; }
        public List<Invoice> invoices { get; set; }
        public string callback { get; set; }
    }

    public class Authentication
    {
        public string emailId { get; set; }
        public string dealerCode { get; set; }
    }

    public class Invoice
    {
        public string invoiceNumber { get; set; }
        public int amount { get; set; }
        public long issueDate { get; set; }
        public long dueDate { get; set; }
    }

    public class InvoiceReq
    {
        public string invoiceNumber { get; set; }
        public string amount { get; set; }
        public long issueDate { get; set; }
        public long dueDate { get; set; }
    }
    
    public class Root
    {
        public List<Datum> data { get; set; }
    }

    public class Resp
    {
        public string status { get; set; }
        public string msg { get; set; }
        public string url { get; set; }
        public string txnId { get; set; }
    }

   

}

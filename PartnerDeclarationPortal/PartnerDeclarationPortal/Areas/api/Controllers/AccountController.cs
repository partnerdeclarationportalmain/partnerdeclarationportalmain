﻿using PartnerDeclarationPortal.App_Code;
using PartnerDeclarationPortal.Areas.api.Models;
using PartnerDeclarationPortal.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Http;


namespace PartnerDeclarationPortal.Areas.api.Controllers
{
    public class AccountController : ApiController
    {
        [HttpPost]
        [ActionName("Validate")]
        public AppResponse Validate(User _UserAccount)
        {
            var _Response = new AppResponse();
            var _dbContext = new DBContext();

            var _User = new User();
            _User = _dbContext.GetLoginUser(_UserAccount.Username);

            return null;
        }

        [HttpPost]
        [ActionName("SendOTP")]
        public AppResponse SendOTP(string arr)
        {

            var _Response = new AppResponse();
            var res = "";
            LOG.Info("SendOTP - START -" + arr);


            try
            {
                var _dbContext = new DBContext();
                var sts = CheckMail(arr);
                if (sts.Status == "success")
                {
                    var _otp = GenerateOTP();
                    if (SendMailToPartner(arr, _otp, ""))
                    {
                        var _PartnerOTP = new User();
                        _PartnerOTP = _dbContext.InsertOTP(arr, _otp);
                        _Response.Status = "Success";
                        LOG.Info("SendOTP - SUCCESS -" + arr);
                    }
                    else
                    {
                        res = "Error";
                        LOG.Info("SendOTP - ERROR -" + arr);
                    }
                }
                else
                {
                    _Response = sts;
                }


            }
            catch (Exception ex)
            {
                LOG.Info("SendOTP - ERROR -" + ex.Message);
                res = ex.Message.ToString();
            }
            return _Response;
        }

        public AppResponse CheckMail(string arr)
        {
            string partnercode = "";
            var _Response = new AppResponse();
            var _dbContext = new DBContext();
            var res = "";
            LOG.Info("CheckMail - START -" + arr);
            try
            {
                var auth_users = ConfigurationManager.AppSettings["LOCAL_USERS"].ToString();
                int pos = Array.IndexOf(auth_users.Split(','), arr.Trim().ToUpper());
                if (pos > -1)
                {
                    System.Web.HttpContext.Current.Session["Email"] = arr;
                    System.Web.HttpContext.Current.Session["CustomerCode"] = "S10003";
                    System.Web.HttpContext.Current.Session["partnercode"] = "S10003";
                    _Response.Status = "success";
                   
                    return _Response;
                }
                else
                {

                 


                    var dt = new DataTable();
                    dt = _dbContext.CheckEmailSAP(arr.Trim().ToUpper());//CheckEmail(arr);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            partnercode += row["KUNNR"].ToString() + ",";
                        }
                        System.Web.HttpContext.Current.Session["CustomerCode"] = dt.Rows[0]["KUNNR"].ToString();
                        System.Web.HttpContext.Current.Session["Email"] = dt.Rows[0]["MAILID_TO"].ToString();
                        System.Web.HttpContext.Current.Session["partnercode"] = partnercode;
                        if (arr.Trim().ToUpper() == dt.Rows[0]["MAILID_TO"].ToString().ToUpper())
                        {
                            _Response.Status = "success";
                            LOG.Info("SendOTP - SUCCESS -" + arr);
                          
                    }
                
                        else
                        {
                            _Response.Message = "Only allowed with an email id which got this communication.";
                            LOG.Info("CheckMail - ERROR -" + arr + res);
                        }

                    }
                    else
                    {
                        _Response.Message = "Only allowed with an email id which got this communication.";
                        LOG.Info("CheckMail - ERROR -" + arr + res);
                    }
                }
            }
            catch (Exception ex)
            {
                //_Response = ex.Message.ToString();
                LOG.Exception(ex);

            }
            return _Response;
        }


        [HttpPost]
        [ActionName("ValidateOTP")]
        public AppResponse ValidateOTP(string email, string OTP)
        {
            var res = "";
            var _Response = new AppResponse();
            var _dbContext = new DBContext();
            LOG.Info("ValidateOTP - START -" + email);

            try
            {
                if (!checkForSQLInjection(OTP))
                {
                    var dt = new DataTable();
                    dt = _dbContext.GetOTPData(email);
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["Otp"].ToString() == OTP)
                        {
                            if (checkOTPTime(dt.Rows[0]["Created_On"].ToString()) == true)
                            {
                                _Response.Status = "success";

                              /*  foreach (DataRow dr in dt.Rows)
                                {
                                    if (!string.IsNullOrEmpty(dr["ApprovedBy"].ToString()) && dr["ApprovedBy"].ToString() != "")
                                    {
                                        _Response.Status = "signedcopyupload";
                                    }
                                }*/
                                LOG.Info("ValidateOTP - SUCCESS -" + email);
                                HttpContext.Current.Session["IP"] = dt.Rows[0]["IP"].ToString();
                                HttpContext.Current.Session["Email"] = dt.Rows[0]["Email"].ToString();
                                _Response.Data = dt.Rows[0]["Email"].ToString();
                            }
                            else
                            {
                                _Response.Status = "OTP expired, try again.";
                            }
                        }
                        else
                        {
                            _Response.Status = "Invalid OTP.";
                        }
                    }
                    else
                    {
                        _Response.Status = "Please try again.";
                    }
                }
                else
                {
                    _Response.Status = "Error";
                    LOG.Info("ValidateOTP - ERROR -" + email);
                }
            }
            catch (Exception ex)
            {
                LOG.Exception(ex);
                res = ex.Message.ToString();
            }
            return _Response;
        }
        public static string GenerateOTP()
        {

            char[] chars = new char[10];
            chars = "1234567890".ToCharArray();
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            data = new byte[6];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(8);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }


            return result.ToString();

        }

        public static bool SendMailToPartner(string MailID, string OTP, string Partner)
        {
            var _result = true;

            try
            {
                var _filePath = @"C:\Developmnt\PartnerDeclarationPortal\PartnerDeclarationPortal\Assets\mail-templates\OTP.html";//System.Configuration.ConfigurationManager.AppSettings["dbCon"];
                //var _filePath = @"C:\Users\254114\Documents\Project\PartnerDeclarationPortal\PartnerDeclarationPortal\Assets\mail-templates\OTP.html";//System.Configuration.ConfigurationManager.AppSettings["dbCon"];
                //var _filePath = @"C:\inetpub\wwwroot\PartnerDeclarationPortal\PartnerDeclarationPortal\Assets\mail-templates\OTP.html";//System.Configuration.ConfigurationManager.AppSettings["dbCon"];
                var _MailContent = System.IO.File.ReadAllText(_filePath);

                _MailContent = _MailContent.Replace("{OTP}", OTP);


                using (SmtpClient smtp = new SmtpClient(ConfigurationManager.AppSettings["SMTP_SERVER"]))
                {
                    smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTP_USER"], ConfigurationManager.AppSettings["SMTP_PASS"]);

                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress("Redington India <" + ConfigurationManager.AppSettings["FROM_EMAIL"] + ">");

                        mail.To.Add(MailID);

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailCC"]))
                        {
                            mail.CC.Add(ConfigurationManager.AppSettings["EmailCC"].ToString());
                        }

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailBCC"]))
                        {
                            mail.Bcc.Add(ConfigurationManager.AppSettings["EmailBCC"].ToString());
                        }

                        mail.Subject = "Re: Redington Credit Automation - OTP";

                        mail.Body = _MailContent;
                        mail.IsBodyHtml = true;

                        System.Net.Mime.ContentType mimeType = new System.Net.Mime.ContentType("text/html");
                        AlternateView alternate = AlternateView.CreateAlternateViewFromString(_MailContent, mimeType);
                        mail.AlternateViews.Add(alternate);

                       smtp.Send(mail);
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Exception(ex);
                _result = false;
            }

            return _result;

        }

       
        public static bool checkOTPTime(string OTPCreated)
        {

            var start = DateTime.Now;
            var oldDate = DateTime.Parse(OTPCreated);

            if ((start - oldDate).TotalMinutes >= 5)
            {
                return false;
            }
            else
            {
                return true;

            }
        }

        
        public static bool checkForSQLInjection(string userInput)
        {

            bool isSQLInjection = false;

            string[] sqlCheckList = { "--", ";--", ";",  "/*", "*/", "@@","char","nchar","varchar", "nvarchar", "alter", "begin","cast",

                                       "create", "cursor","declare", "delete", "drop","end","exec","execute", "fetch","insert", "kill",

                                             "select", "sys","sysobjects", "syscolumns","table","update"

                                       };

            string CheckString = userInput.Replace("'", "''");

            for (int i = 0; i <= sqlCheckList.Length - 1; i++)
            {

                if ((CheckString.IndexOf(sqlCheckList[i],

    StringComparison.OrdinalIgnoreCase) >= 0))

                {
                    isSQLInjection = true;
                }
            }
            return isSQLInjection;
        }

        public bool ContactInset(ContactUs contact)
        {
            var _dbContext = new DBContext();
            bool value = false;
            try
            {
                value = _dbContext.Contactus(contact.name, contact.emailid, contact.phone, contact.country, contact.city, contact.pobox, contact.website, contact.type_of_query, contact.remarks);
                return value;
            }
            catch (Exception)
            {
                return value;
            }
        }

    }
}

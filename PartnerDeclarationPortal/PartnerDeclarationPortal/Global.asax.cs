﻿using PartnerDeclarationPortal.App_Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;

namespace PartnerDeclarationPortal
{
    public class MvcApplication : System.Web.HttpApplication
    {
        //protected void Application_Start()
        //{
        //    AreaRegistration.RegisterAllAreas();
        //    GlobalConfiguration.Configure(WebApiConfig.Register);
        //    FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        //    RouteConfig.RegisterRoutes(RouteTable.Routes);
        //    BundleConfig.RegisterBundles(BundleTable.Bundles);
        
        
        //}

        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
        
        protected void Application_Error()
        {
            var err = Server.GetLastError();
            //Server.ClearError();
            //Response.Clear();
                Response.Redirect("~/Order/WentWrong");
        }

        #region Methods added for Enable Session in ApiController

        public override void Init()
        {
            this.PostAuthenticateRequest += MvcApplication_PostAuthenticateRequest;
            base.Init();
        }

        void MvcApplication_PostAuthenticateRequest(object sender, EventArgs e)
        {
            System.Web.HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
        }


        void Session_Start(object sender, EventArgs e)
        {
            try
            {
                System.Web.Configuration.SessionStateSection sessionState = (System.Web.Configuration.SessionStateSection)ConfigurationManager.GetSection("system.web/sessionState");
                string sidCookieName = sessionState.CookieName;
                if (Request.Cookies[sidCookieName] != null)
                {
                    HttpCookie sidCookie = Response.Cookies[sidCookieName];
                    sidCookie.Value = Session.SessionID;
                    sidCookie.HttpOnly = true;
                    sidCookie.Secure = true;
                    sidCookie.Path = "/";
                }

            }
            catch (Exception ex)
            {
            }

            try
            {
                if (Request.IsSecureConnection == true)
                {
                    Response.Cookies["ASP.NET_SessionId"].Secure = true;
                }
            }
            catch (Exception ex)
            {
                LOG.Exception(ex);
            }


        }

        #endregion
    }
}
